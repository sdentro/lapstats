================================================================================================================================================
Readme Lapstats v1.2
================================================================================================================================================
# Updates in V1.2
- Total rewrite of the previously existing graphics;
- added a new view, you can now see at which tracks you need to drive with a certain car to complete your handicap;
- you can decide how many car classes there need to be by editing the data.dat file;
- fixed a couple of bugs in the code, most notably the Career loading bug of v1.1.1;
- trashed the .exe that is just not needed;
- adding a total of 34 cars and 111 tracks!

# Updates in v1.1.1
Major bugfix in the filewriter. I discovered that things went terribly wrong when writing multiple users to data.dat. That bug is fixed in this
version. I also added some extra notes to this readme.

# Updates in v1.1
After some beta testing I've decided it's time to release version 1.1. It contains better support for multiple users, handicaps, calculated over tracksets, improved tracksets and optional auto-loading and auto-saving. This means that I've included the functionality that I initially planned for Lapstats. Also included are more cars and more tracks, so more fun! But there are still lots improvements to be done. Next version will probably have a GUI redesign which is needed... 

Thanks for using this app! I'd like to hear from you, so drop me a line at lapstats@gmail.com.

================================================================================================================================================
# Overview
================================================================================================================================================
Thank you for downloading Lapstats for GTR2. This small software package is created so that anyone can go out and try 
to push themselves to improve laptimes. Why Lapstats when we have GTR2 Rank you may ask. Well, GTR2 Rank is a wonderful
website that is very useful. But it contains only the stock GTR2 cars and a small selection of tracks. With the abundance
of addons that are available for GTR2 I felt there should be a software package that is versatile and adaptable (on the data
end at least). 

I have taken the liberty of combining a large amount of cars into one big GT system. Car classes range from GT1 to GT5. It
has taken me many hours to get the basics of this framework down. The plan was to create a measuring stick on how fast 
a car is, when compared to others. Now you can compare cars that aren't in the original GTR2 game. The structure of the
data.dat file is in plain text, so you can add/change whatever you want.

Usage is simple. Start Lapstats, go through the wizard (will only show the first time at startup) and start using. It is possible
to import data from v1.1/v1.1.1, mainly the user data and created tracksets. An existing user or trackset will not be added,
so no changes you made will be overwritten! 

This is probably the last big release of Lapstats. I'm going to start another project (not simracing related), but I will release
updates with bug fixes to make this version even better. You will also notice that the number of tracks starting with the letter
'M' and up in the alphabet is a lot less than the lower letters. That's because I'm just going through all tracks in my main
GTR2 installation to generate data. I will finish that of course.

Although I have not tested this, it is possible that Lapstats will work (more or less) with other Simbin titles. But
there is no data for the tracks and cars. Although one might never know what happens in the future.

I hope you will enjoy Lapstats. If you have any ideas or would like to help out, drop me a line at lapstats@gmail.com.

================================================================================================================================================
# Installation
================================================================================================================================================
1) You will need a Java 1.6 compatible JRE, if not already on your PC => http://www.java.com/en/download/manual.jsp
2) Extract the .7z file to a place where you have write access (the data.dat file needs write access).
3) Upon first usage a wizard will show up asking you for all information needed to start using Lapstats.
4) You can import data from v1.1 and v1.1.1 by using the menu option and selecting the data.dat file from your previous installation. 

================================================================================================================================================
# Known issues
================================================================================================================================================
- The quirkyness of the save and close buttons is designed this way. It's a little awkward when doing just one change, but when doing more than 2 changes it actually saves a lot of clicks;
- FIA 2006 mod contains the same Viper GTS-R (with the same name), but with altered engine. Lapstats cannot see the difference;
- FIA GT3 mod contains a Gallardo with the same name as the standalone Gallardo mod, but with a different engine. Lapstats cannot see the difference;
- the graphical interface doesn't know what to do with a big amount of car classes. Adding a couple more should be OK, but after that the interface might not show all classes when it runs out of space to show info;
- when importing data a user will not be added when it's already in the system. You will see no warning;
- there is no checking of input whatsoever. That means, selecting the wrong file to import or setting a reference time that is not compatible will probably crash Lapstats.

================================================================================================================================================
================================================================================================================================================
