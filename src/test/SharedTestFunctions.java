package test;

/**
 * @author sdentro
 * @version $Id$
 */

import code.backend.DataController;
import code.backend.FileProcessor;
import code.backend.control.Control;
import code.backend.control.GuiControl;
import code.backend.control.TalkToGui;
import code.backend.data.Car;
import code.backend.data.Track;
import code.backend.data.TrackSet;
import code.backend.data.User;

public class SharedTestFunctions {

	protected Control control;
	protected DataController dc;
	protected GuiControl guiControl;
	protected TalkToGui talkToGui;
	
	protected Car ferrariTestCar;
	protected Car porscheTestCar;
	
	protected Track doningtonTestTrack;
	protected Track monzaTestTrack;
	
	protected User user;
	
	
//	public void setupControlAUserAndTwoCarsAndTracks() {
//		setupControllers();
//		user = createUser("TestUser");
//		addUser(user);
//		
//		ferrariTestCar = createCar("Ferrari 550 Maranello", "GT1");
//		porscheTestCar = createCar("Porsche 911-GT3-RSR", "GT2");
//		addCarsToSystem(control, ferrariTestCar, porscheTestCar);
//		
//		doningtonTestTrack = createTrack("Donington");
//		monzaTestTrack = createTrack("Monza");
//		doningtonTestTrack.addReferenceTime("GT1", "1:26.000");
//		monzaTestTrack.addReferenceTime("GT2", "1:40.000");
//		addTracksToSystem(control, doningtonTestTrack, monzaTestTrack);
//	}
	
	public Car createCar(String name, String carClass) {
		Car car = new Car(name, name.toUpperCase());
		car.setToClass(carClass);
		return car;		
	}
	
	public Track createTrack(String name) {
		return new Track(name, name.toUpperCase());
	}

	public User createUser(String name) {
		return new User(name);
	}
	
	public TrackSet createTrackSet(String name, Track...tracks) {
		TrackSet ts = new TrackSet(name, tracks);
		for(Track track : tracks) {
			track.addToSet(name);
		}
		return ts;
	}
	
	public void addUser(User user) {
		control.addUser(user);
	}
	
	public void addCarsToSystem(Control control, Car...cars) {
		for(Car car : cars) {
			control.addCar(car);
		}
	}
	
	public void addTracksToSystem(Control control, Track...tracks) {
		for(Track track : tracks) {
			control.addTrack(track);
		}
	}
	
//	public void addTrackSet(TrackSet ts, Track...tracks) {
//		control.addTrackSet(ts.getName());
//		for(Track t : ts.getTracks()) {
//			trackSet.add(t.getName());
//		}
//	}
	
	public void setupControllers() {
		talkToGui = new TalkToGui();
		dc = new DataController();
		control = new Control(dc);
		guiControl = new GuiControl(control, dc, new FileProcessor(control, talkToGui), talkToGui);
		control.setGuiControl(guiControl);
	}
	
	public void resetControllers() {
		dc = null;
		control = null;
		guiControl = null;
		setupControllers();
	}
	
}
