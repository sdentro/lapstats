package test.backend;

/**
 * @author sdentro
 * @version $Id$
 */

import static org.junit.Assert.*;

import java.io.FileNotFoundException;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import code.backend.DataController;
import code.backend.FileProcessor;
import code.backend.control.Control;
import code.backend.control.GuiControl;
import code.backend.control.TalkToGui;
import code.backend.data.Car;
import code.backend.data.Handicap;
import code.backend.data.Id;
import code.backend.data.Laptime;
import code.backend.data.Track;
import code.backend.data.TrackSet;
import code.backend.data.User;



public class FileProcessorTest {

	private Control control;
	private DataController datacontrol;
	private FileProcessor fileprocessor;
	private TalkToGui talkToGui;
	private GuiControl guicontrol;
	
	@Before 
	public void setUp() {
		setupTestObjects();
	}
	
	private void setupTestObjects() {
		datacontrol = new DataController();
		control = new Control(datacontrol);
		talkToGui = new TalkToGui();
		fileprocessor = new FileProcessor(control, talkToGui);
		guicontrol = new GuiControl(control, datacontrol, fileprocessor, talkToGui);
		control.setGuiControl(guicontrol);
	}
	
	@After 
	public void reset() {
		
	}
	
	@Test
	public void testReadCareer() {
		User user = new User("testuser");
		guicontrol.addUser(user);
		
		String filename = "src/test/backend/CareerTest.blt";
		String userName = "testuser";
		fileprocessor.setDataFile("src/test/backend/data_fileprocessortest.dat");
		fileprocessor.readSavedData();
		fileprocessor.readCareer(filename, userName);
		
		/*
		 * Read file contains three laptimes. All laptimes are checked. 
		 * One of them is its own special trackset that needs checking aswell.
		 */
		// First car
		Id carId = guicontrol.getCarId("Lamborghini Murcielago R-GT");
		Car car = guicontrol.getCar(carId);
		Id trackId = guicontrol.getTrackId("Snetterton 2005");
		Track track = guicontrol.getTrack(trackId);
		
		user = guicontrol.getUser(userName);
		Laptime laptime = user.getLaptime(car, track);
		
		assertTrue("", laptime.getCarName().equals("Lamborghini Murcielago R-GT"));
		assertTrue("", laptime.getTrackName().equals("Snetterton 2005"));
		assertTrue("", laptime.getTime().equals("1:13.275"));
		assertTrue("", laptime.getDifference().equals("11.275"));
		// Check the handicap of a specifically created trackset
		TrackSet trackset = user.getTrackSet("fileprocessortest");
		Handicap handicap = user.getHandicap(car, trackset);
		
		assertTrue("", handicap.getHandicap().equals("11.275"));
		
		// Second car
		carId = guicontrol.getCarId("Ford GT40");
		car = guicontrol.getCar(carId);
		trackId = guicontrol.getTrackId("Denver 2005");
		track = guicontrol.getTrack(trackId);
		
		laptime = user.getLaptime(car, track);
		
		assertTrue("", laptime.getCarName().equals("Ford GT40"));
		assertTrue("", laptime.getTrackName().equals("Denver 2005"));
		assertTrue("", laptime.getTime().equals("1:25.814"));
		assertTrue("", laptime.getDifference().equals("11.814"));
		
		// Third car
		carId = guicontrol.getCarId("Peugeot V12");
		car = guicontrol.getCar(carId);
		trackId = guicontrol.getTrackId("Mid Ohio 2007");
		track = guicontrol.getTrack(trackId);
		
		laptime = user.getLaptime(car, track);
		
		assertTrue("", laptime.getCarName().equals("Peugeot V12"));
		assertTrue("", laptime.getTrackName().equals("Mid Ohio 2007"));
		assertTrue("", laptime.getTime().equals("1:16.746"));
		assertTrue("", laptime.getDifference().equals("-2.254"));
	}
	
	@Test
	public void testReadCareerExceptions() {
		User user = new User("testuser");
		guicontrol.addUser(user);
		
		String filename = "src/test/backend/filedoesnotexist";
		String userName = "testuser";
		fileprocessor.setDataFile("src/test/backend/data_fileprocessortest.dat");
		fileprocessor.readSavedData();
		fileprocessor.readCareer(filename, userName);
		// If the fileReader does not fail, go through
		assertTrue(true);
	}

	@Test
	public void testSplitInput() {
		String stringToSplit = "category=value";
		String answerMustBe = "value";
		
		assertTrue("", fileprocessor.splitInput(stringToSplit).equals(answerMustBe));
		
		// Bordercases
		assertTrue("", fileprocessor.splitInput("category=").equals(""));
		assertTrue("", fileprocessor.splitInput("category").equals(""));
		assertTrue("", fileprocessor.splitInput("").equals(""));
		try {
			fileprocessor.splitInput(null);
		} catch(NullPointerException e) {
			assertTrue(true);
		}
	}

	@Test
	public void testWriteData() {
//		System.out.println("Writetest now");
		fileprocessor.setDataFile("src/test/backend/data_readsaveddatatest.dat");
		fileprocessor.readSavedData();
		
		User user = control.getUser("user");
		assertNotNull(user);
		user.setLoadCareerOnStartup(true);
		user.setSaveDataOnExit(false);
//		System.out.println("Before: " + user.getLoadCareerOnStartup() + " " + user.getSaveDataOnExit());
		user.setCareerLocation("some/where/career");
		
		Track track = control.getTrack(control.getTrackId("Mid Ohio 2007"));
		assertNotNull(track);
		track.setAuthor("Iemand anders");
		track.setName("Mid Ohio Modified");
		
		Car car = control.getCar(control.getCarId("Ford GT40"));
		assertNotNull(car);
		car.setAuthor("Iemand anders");
		car.setName("Ford Modified");
		car.setToClass("GT99");
		
		fileprocessor.writeData();
		
		setupTestObjects();
		
		fileprocessor.setDataFile("src/test/backend/data_readsaveddatatest.dat");
		fileprocessor.readSavedData();
		
		user = control.getUser("user");
		assertNotNull(user);
//		System.out.println("After: " +  user.getLoadCareerOnStartup() + " " + user.getSaveDataOnExit());
		assertTrue("Unexpected load career option", user.getLoadCareerOnStartup() == true);
		assertTrue("Unexpected save data option", user.getSaveDataOnExit() == false);
		
		track = control.getTrack(control.getTrackId("Mid Ohio Modified"));
		assertNotNull(track);
		assertTrue("Unexpected track author", track.getAuthor().equals("Iemand anders"));
		
		car = control.getCar(control.getCarId("Ford Modified"));
		assertNotNull(car);
		assertTrue("Unexpected car author", car.getAuthor().equals("Iemand anders"));
		assertTrue("Unexpected carclass", car.getCarClass().equals("GT99"));
		
		
		// Wrap up by writing
		setupTestObjects();
		fileprocessor.setDataFile("src/test/backend/data_readsaveddatatest_default.dat");
		fileprocessor.readSavedData();
		fileprocessor.setDataFile("src/test/backend/data_readsaveddatatest.dat");
		fileprocessor.writeData();
	}
	
	private void makeAFewChanges() {

	}

	@Test
	public void testReadSavedData() {
		fileprocessor.setDataFile("src/test/backend/data_readsaveddatatest.dat");
		fileprocessor.readSavedData();
		
		assertTrue("Improper number of cars read", control.getAllCarNames().size() == 3);
		assertTrue("Improper number of tracks read", control.getAllTrackNames().size() == 3);
		assertTrue("Improper number of tracksets read", control.getAllTrackSetNames().size() == 3);
		assertTrue("Expected user not added", control.getAllUserNames().contains("user"));
		
		User user = control.getUser("user");
		assertTrue("User load career on startup not set properly", user.getLoadCareerOnStartup() == false);
		assertTrue("User save data on exit not set properly", user.getSaveDataOnExit() == true);
	}
	
	@Test
	public void testReadSavedDataExceptions() {
		//TODO de fileprocessor moet exceptions gaan gooien als data.dat niet klopt!
		
		// Reading a non existant file should not crash the filereader
		fileprocessor.setDataFile("non/existant/file.dat");
		fileprocessor.readSavedData();
		assertTrue(true);
		
		
		// Reading an empty file should not crash the filereader
		setupTestObjects();
		fileprocessor.setDataFile("src/test/backend/empty_file.dat");
		fileprocessor.readSavedData();
		assertTrue(true);
		
		// Reading a file containing no user, only tracks and cars
		setupTestObjects();
		fileprocessor.setDataFile("src/test/backend/no_user_just_tracks_cars.dat");
		fileprocessor.readSavedData();
		assertTrue(true);
		
		setupTestObjects();
		fileprocessor.setDataFile("src/test/backend/no_cars_just_user_tracks.dat");
		fileprocessor.readSavedData();
		assertTrue(true);
		
		setupTestObjects();
		fileprocessor.setDataFile("src/test/backend/no_tracks_just_user_cars.dat");
		fileprocessor.readSavedData();
		assertTrue(true);
	}

	@Test
	public void testExportSettings() {
		fileprocessor.setDataFile("src/test/backend/data_readsaveddatatest.dat");
		fileprocessor.readSavedData();
		
		// change a user
		User user = control.getUser("user");
		user.setCareerLocation("some/where/career.blt");
		assertNotNull(user);
		
		// change a trackset
		TrackSet trackset = control.getTrackSet("exportdatatest");
		assertNotNull(trackset);
		Track track = control.getTrack(control.getTrackId("Denver 2005"));
		assertNotNull(track);
		trackset.addTrack(track);
		
		// export		
		fileprocessor.exportSettings("src/test/backend/export.txt");
		
		// recreate objects
		setupTestObjects();
		fileprocessor.setDataFile("src/test/backend/data_readsaveddatatest.dat");
		fileprocessor.readSavedData();
		
		// import settings
		fileprocessor.importSettings("src/test/backend/export.txt");
		
		// check if it worked
		user = control.getUser("user");
		assertNotNull(user);
		assertTrue("Expected career location of user not set", user.getCareerLocation().equals("some/where/career.blt"));
		trackset = control.getTrackSet("exportdatatest");
		assertNotNull(trackset);
		track = control.getTrack(control.getTrackId("Denver 2005"));
		assertNotNull(track);
		assertTrue("Expected track not in trackset", trackset.contains(track));
	}

	@Test
	public void testImportSettings() {
		fileprocessor.setDataFile("src/test/backend/data_readsaveddatatest.dat");
		fileprocessor.readSavedData();
		
		// import settings
		fileprocessor.importSettings("src/test/backend/export.txt");
		
		// check if it worked
		User user = control.getUser("user");
		assertNotNull(user);
		assertTrue("Expected career location of user not set", user.getCareerLocation().equals("some/where/career.blt"));
		TrackSet trackset = control.getTrackSet("exportdatatest");
		assertNotNull(trackset);
		Track track = control.getTrack(control.getTrackId("Denver 2005"));
		assertNotNull(track);
		assertTrue("Expected track not in trackset", trackset.contains(track));
	}
	
	@Test
	public void testImportExceptions() {
		String filename = "src/test/backend/filedoesnotexist";
		fileprocessor.importSettings(filename);
		
		// If the fileReader does not fail, go through
		assertTrue(true);
	}

}
