package test.backend.data;

/**
 * @author sdentro
 * @version $Id$
 */

import java.util.Vector;

import org.junit.*;

import code.backend.data.Car;
import code.backend.data.Handicap;
import code.backend.data.Laptime;
import code.backend.data.Track;
import code.backend.data.TrackSet;
import code.backend.data.UserCar;
import code.backend.data.UserTrack;
import code.backend.table.CellObject;


import test.SharedTestFunctions;

public class UserTest extends SharedTestFunctions {

	//TODO refactor whole usertest class
	@Before public void setUp() {
		user = createUser("TestUser");
		ferrariTestCar = createCar("Ferrari 550 Maranello", "GT1");
		porscheTestCar = createCar("Porsche 911-GT3-RSR", "GT2");
		doningtonTestTrack = createTrack("Donington");
		monzaTestTrack = createTrack("Monza");
	}
	
	@After public void reset() {
		user = createUser("TestUser");
		ferrariTestCar = createCar("Ferrari 550 Maranello", "GT1");
		porscheTestCar = createCar("Porsche 911-GT3-RSR", "GT2");
		doningtonTestTrack = createTrack("Donington");
		monzaTestTrack = createTrack("Monza");
	}
	
	@Test public void addCar() {
		user.addCar(ferrariTestCar);
		user.getCarLaptimes(ferrariTestCar);
	}
	
	@Test public void addTrack() {
		user.addTrack(doningtonTestTrack);
		user.getTrackLaptimes(doningtonTestTrack);
	}
	
	@Test public void getLaptime() {
		/* addLaptime test tests getLaptime as well */
		addLaptime();
	}
	
	@Test public void addLaptime() {
		user.addCar(ferrariTestCar);
		user.addTrack(doningtonTestTrack);
		String laptime = "1:25.000";
		user.addLaptime(ferrariTestCar, doningtonTestTrack, laptime, "85.000", 8);
		Laptime justAddedTime = user.getLaptime(ferrariTestCar, doningtonTestTrack);
		Assert.assertTrue(justAddedTime.getTime().equals(laptime));
	}

	@Test public void addSet() {
		TrackSet ts = createTrackSet("TestSet", doningtonTestTrack);
		user.addSet(ts);
		user.getTrackSet(ts.getName());
	}
	
	@Test public void getCarLaptimes() {
		
		user.addCar(ferrariTestCar);
		user.addTrack(doningtonTestTrack);
		String laptime = "1:25.000";
		user.addLaptime(ferrariTestCar, doningtonTestTrack, laptime, "85.000", 8);
		
		Assert.assertTrue(userContainsLaptime(ferrariTestCar, doningtonTestTrack, laptime));
	}
	
	private boolean userContainsLaptime(Car car, Track track, String laptime) {
		Vector<CellObject> times = user.getCarLaptimes(ferrariTestCar);
		for(CellObject object : times) {
			if(object instanceof Laptime) {
				Laptime lt = (Laptime) object;
				if(foundLaptimeEqualsWantedLaptime(lt, laptime, car, track)) {
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean foundLaptimeEqualsWantedLaptime(Laptime foundTime, String wantedTime, Car car, Track track) {
		return foundTime.getTime().equals(wantedTime) &&
			foundTime.getUserTrack().getTrack().equals(track) && 
			foundTime.getUserCar().getCar().equals(car);
	}
	
	@Test public void getHandicap() {
		user.addCar(ferrariTestCar);
		user.addTrack(doningtonTestTrack);
		doningtonTestTrack.addReferenceTime("GT1", "1:26.000");
		TrackSet ts = createTrackSet("TestSet", doningtonTestTrack);
		user.addSet(ts);
		user.addLaptime(ferrariTestCar, doningtonTestTrack, "1:25.123", "85.123", 8);
		
		Handicap storedHandicap = (Handicap) user.getHandicap(ferrariTestCar, ts);
		
		Assert.assertTrue(storedHandicap.getHandicap().equals("-0.876"));
	}
	
	@Test public void getUserCar() {
		user.addCar(ferrariTestCar);
		UserCar uCar = user.getUserCar(ferrariTestCar);
		Assert.assertTrue(uCar.getCar().equals(ferrariTestCar));
	}
	
	
	@Test public void getTrackLaptimes() {
		user.addCar(ferrariTestCar);
		user.addCar(porscheTestCar);
		user.addTrack(doningtonTestTrack);
		user.addLaptime(ferrariTestCar, doningtonTestTrack, "1:25.123", "85.123", 8);
		user.addLaptime(porscheTestCar, doningtonTestTrack, "1:27.483", "85.483", 3);
		
		Assert.assertTrue(containsBothLaptimes());
	}
	
	private boolean containsBothLaptimes() {
		boolean containsFerrariTime = false;
		boolean containsPorscheTime = false;
		
		Vector<CellObject> times = user.getTrackLaptimes(doningtonTestTrack);
		for (CellObject object : times) {
			if (object instanceof Laptime) {
				Laptime lt = (Laptime) object;
				if (lt.getCarName().equals(ferrariTestCar.getName()) && lt.getTime().equals("1:25.123")) {
					containsFerrariTime = true;
				}
				if (lt.getCarName().equals(porscheTestCar.getName()) && lt.getTime().equals("1:27.483")) {
					containsPorscheTime = true;
				}
			}
		}
		return containsFerrariTime && containsPorscheTime;
	}
	
	@Test public void getUserTrack() {
		user.addTrack(doningtonTestTrack);
		UserTrack uTrack = user.getUserTrack(doningtonTestTrack);
		Assert.assertTrue(uTrack.getTrack().equals(doningtonTestTrack));
	}
	
	@Test public void recalculateDifferencesCar() {
		user.addCar(ferrariTestCar);
		user.addTrack(doningtonTestTrack);
		doningtonTestTrack.addReferenceTime("GT1", "1:26.000");
		doningtonTestTrack.addReferenceTime("GT2", "1:27.000");
		user.addLaptime(ferrariTestCar, doningtonTestTrack, "1:25.123", "85.123", 8);
		
		String previousDifference = user.getLaptime(ferrariTestCar, doningtonTestTrack).getDifference();
		ferrariTestCar.setToClass("GT2");
		user.recalculateDifferences(ferrariTestCar);
		String newDifference = user.getLaptime(ferrariTestCar, doningtonTestTrack).getDifference();
		
		Assert.assertTrue(previousDifference.equals("-0.876") && newDifference.equals("-1.876"));
	}
	
	@Test public void recalculateDifferencesTrack() {
		user.addCar(ferrariTestCar);
		user.addTrack(doningtonTestTrack);
		doningtonTestTrack.addReferenceTime("GT1", "1:26.000");
		user.addLaptime(ferrariTestCar, doningtonTestTrack, "1:25.123", "85.123", 8);
		
		String previousDifference = user.getLaptime(ferrariTestCar, doningtonTestTrack).getDifference();
		doningtonTestTrack.addReferenceTime("GT1", "1:27.000");
		user.recalculateDifferences(doningtonTestTrack);
		String newDifference = user.getLaptime(ferrariTestCar, doningtonTestTrack).getDifference();
		
		Assert.assertTrue(previousDifference.equals("-0.876") && newDifference.equals("-1.876"));
	}
	
	@Test public void removeSet() {
		boolean testResult = false;
		
		user.addCar(ferrariTestCar);
		user.addTrack(doningtonTestTrack);
		doningtonTestTrack.addReferenceTime("GT1", "1:26.000");
		TrackSet ts = createTrackSet("TestSet", doningtonTestTrack);
		user.addSet(ts);
		user.addLaptime(ferrariTestCar, doningtonTestTrack, "1:25.123", "85.123", 8);
		
		user.removeSet(ts);
		try {
			user.getHandicap(ferrariTestCar, ts);
		}
		catch (IndexOutOfBoundsException e) {
			testResult = true;
		}
		Assert.assertTrue(testResult);
	}
	
//	@Test public void setLaptime() {
//		user.addCar(ferrariTestCar);
//		user.addTrack(doningtonTestTrack);
//		user.addLaptime(ferrariTestCar, doningtonTestTrack, "1:25.123", "85.123", 8);
//		
//		Laptime oldTime = user.getLaptime(ferrariTestCar, doningtonTestTrack);
//		String oldLaptime = oldTime.getTime();
//		
//		Laptime time = new Laptime(user.getUserCar(ferrariTestCar), user.getUserTrack(doningtonTestTrack), "1:26.123", "86.123", 10);
//		user.setLaptime(time);
//		
//		Laptime newTime = user.getLaptime(ferrariTestCar, doningtonTestTrack);
//		String newLaptime = newTime.getTime();
//		
//		Assert.assertTrue(newLaptime.equals("1:26.123") && !oldLaptime.equals(newLaptime));
//	}
	
	@Test public void stripOffExcessDigits() {
		Assert.assertTrue(user.stripOffExcessDigits(0.1).equals("0.100"));
		Assert.assertTrue(user.stripOffExcessDigits(0.22).equals("0.220"));
		Assert.assertTrue(user.stripOffExcessDigits(0.333).equals("0.333"));
		Assert.assertTrue(user.stripOffExcessDigits(0.4444444).equals("0.444"));
	}
}
