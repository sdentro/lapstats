//package test.backend;
//
//import java.util.HashSet;
//import java.util.Vector;
//
//import junit.framework.Assert;
//
//import org.junit.*;
//
//import backend.control.Control;
//import backend.data.Car;
//import backend.data.Track;
//import backend.data.TrackSet;
//import backend.data.User;
//
//public class ControlTest {
//
//	/*
//	 * Feature testing:
//	 *  1) Lees in Career.blt
//	 *  2) Lees in data file
//	 *  3) Sla data op => niet nuttig
//	 *  
//	 *  4) Voeg user toe
//	 *  5) Verwijder user
//	 *  6) Display alle auto's
//	 *  7) Display alle tracks
//	 *  8) Display alle users
//	 *  9) Maak track set
//	 *  10) Verwijder track set
//	 *  11) Voeg een track toe aan een set
//	 *  
//	 *  12) Bereken handicap over track set per car
//	 *  13) Bereken handicap over track set per class
//	 */
//	
//	private Control control;
//	private User user;
//	private Track track1;
//	private Track track2;
//	private TrackSet tSet;
//	private Car car1;
//	private Car car2;
//	
//	@Before public void setUp() {
//		// Control and one user
//		control = new Control();
//		user = new User(control,"TestUser");
//		
//		// Cars
//		car1 = new Car("Ferrari 550 Maranello", "FERRARI 550");
//		car1.setToClass("GT1");
//		car2 = new Car("Porsche 911-GT3-RSR", "PORSCHE 911-GT3-RSR");
//		car2.setToClass("GT1");
//		
////		control.addCar(car1);
////		control.addCar(car2);
////		user.addCar(car1);
////		user.addCar(car2);
//		
//		// Tracks and trackset
//		track1 = new Track("Donington", "4DONINGTON");
//		track2 = new Track("Monza", "3MONZA");
//		track1.addReferenceTime("GT1", "1:26.000");
//		track1.addReferenceTime("GT2", "1:40.000");
//		tSet = new TrackSet("TestSet", track1, track2);
//		track1.addToSet(tSet.getName());
//		track2.addToSet(tSet.getName());
//		
////		control.addTrack(track1);
////		control.addTrack(track2);
////		user.addTrack(track1);
////		user.addTrack(track2);
//		
////		control.addTrackSet("TestSet");
////		HashSet<String> trackSet = new HashSet<String>();
////		for(Track t : tSet.getTracks()) {
////			trackSet.add(t.getName());
////		}
////		control.addTrackToTracksets(track1, trackSet);
//	}
//	
//	@After public void reset() {
//		control = new Control();
//		user = new User(control,"TestUser");
//		
//		control.addCar(car1);
//		control.addCar(car2);
//		user.addCar(car1);
//		user.addCar(car2);
//		
//		control.addTrack(track1);
//		control.addTrack(track2);
//		user.addTrack(track1);
//		user.addTrack(track2);
//		
//		control.addTrackSet("TestSet");
//		HashSet<String> trackSet = new HashSet<String>();
//		for(Track t : tSet.getTracks()) {
//			trackSet.add(t.getName());
//		}
//		control.addTrackToOnlyTheseTracksets(track1, trackSet);
//	}
//
//	@Test public void readCareer() {
//		//TODO deze test moet bekeken worden
//		control.readCareerOfCurrentlyActiveUser();
//		// Test for at least one car, one track and one laptime
//		Assert.assertTrue(control.containsCar("FERRARI 550 MARANELLO"));
//		Assert.assertTrue(control.containsTrack("4DONINGTON"));
//		
//		// Fetch all laptimes by this car and check if 4donington is there
//		Vector<Vector<String>> times = control.getCarsTableData("FERRARI 550 MARANELLO");
//		boolean value = false;
//		for(Vector<String> time : times) {
//			if(time.contains("4DONINGTON")) {
//				value = true;
//			}
//		}
//		Assert.assertTrue(value);
//	}
//
////	@Test public void readData() {
////		control.readDataFromFile();
////		Assert.assertTrue(control.containsCar("FERRARI 550 MARANELLO"));
////		Assert.assertTrue(control.containsTrack("4DONINGTON"));
////		Assert.assertTrue(control.containsUser("user"));
////		
////		// Fetch all laptimes by this car and check if 4donington is not there
////		// This test is this way because the data file does not contain any Laptimes on release
////		Vector<Vector<String>> times = control.getLaptimesByCar("FERRARI 550 MARANELLO");
////		boolean value = false;
////		for(Vector<String> time : times) {
////			if(time.contains("4DONINGTON")) {
////				value = true;
////			}
////		}
////		Assert.assertFalse(value);
////	}
//
//	@Test public void addUser() {
//		control.addUser("test");
//		control.addUser(new User(control, "anotherTest"));
//		
//		Assert.assertTrue(control.containsUser("test"));
//		Assert.assertTrue(control.containsUser("anotherTest"));
//	}
//	
//	@Test public void removeUser() {
//		control.addUser("test");
//		// Remove one user
//		control.removeUser(control.getUser("test"));
//		Assert.assertFalse(control.containsUser("test"));
//		
//		// Cannot remove the last user
//		control.removeUser(control.getUser("user"));
//		Assert.assertTrue(control.containsUser("user"));
//	}
//
//	@Test public void getCars() {
//		control.getAllCarNames();
//	}
//	
//	@Test public void getTracks() {
//		control.getAllTrackNames();
//	}
//	
//	@Test public void getUsers() {
//		control.getAllUserNames();
//	}
//	
//	@Test public void saveTrack() {
//		//TODO saveTrack(String trackName, String lodLocation, HashSet<String> trackSets, Hashtable<String, String> newRefTimes)
//	}
//	
//	@Test public void saveCar() {
//		//TODO saveCar(String carName, String carClass)
//	}
//	
//	@Test public void addTrackSet() {
//		// Add the tracks to the control and the user(s)
//		control.addTrack(track1);
//		control.addTrack(track2);
//		user.addTrack(track1);
//		user.addTrack(track2);
//		
//		
//		control.addTrackSet("TestSet");
//		HashSet<String> trackSet = new HashSet<String>();
//		for(Track t : tSet.getTracks()) {
//			trackSet.add(t.getName());
//		}
//		control.addTrackToOnlyTheseTracksets(track1, trackSet);
//		Assert.assertTrue(control.getAllTrackSetNames().contains("TestSet"));
//	}
//
//	@Test public void removeTrackSet() {
//		// Add the tracks to the system
//		control.addTrack(track1);
//		control.addTrack(track2);
//		
//		// Create a new TrackSet.
//		control.addTrackSet("TestSet");
//		
//		// Create a set with the name of the test set in it and add both tracks
//		HashSet<String> trackSet = new HashSet<String>();
//		trackSet.add("TestSet");
//
//		control.addTrackToOnlyTheseTracksets(track1, trackSet);
//		control.addTrackToOnlyTheseTracksets(track2, trackSet);
//		
//		HashSet<String> setsWithoutTestSet = new HashSet<String>();
//		setsWithoutTestSet.add("2004");
//		setsWithoutTestSet.add("2003");
//		setsWithoutTestSet.add("full");
//		
//		// Remove one track from the set, set should still be in the system
//		control.addTrackToOnlyTheseTracksets(track1, setsWithoutTestSet);
//		Assert.assertTrue(control.getAllTrackSetNames().contains("TestSet"));
//		
//		// Remove the other track from the set, set should now be deleted
//		control.addTrackToOnlyTheseTracksets(track2, setsWithoutTestSet);
//		Assert.assertFalse(control.getAllTrackSetNames().contains("TestSet"));
//	}
//
//	@Test public void addTrackToTrackSet() {
//		// Add the track and the trackset to the system
//		control.addCar(car1);
//		control.addTrack(track1);
//		control.addTrackSet("TestSet");
//
//		// Add the track to the set
//		HashSet<String> trackSet = new HashSet<String>();
//		trackSet.add("TestSet");
//
//		control.addTrackToOnlyTheseTracksets(track1, trackSet);
//		
//		Assert.assertTrue(((TrackSet) control.getTrackSet("TestSet")).getTracks().contains(track1));
//		
//	}
//
//	@Test public void calculateHandicapByCar() {
//		// Add the car, track and the trackset to the system
//		control.addCar(car1);
//		control.addTrack(track1);
//		control.addTrackSet("TestSet");
//
//		// Add the track to the set
//		HashSet<String> trackSet = new HashSet<String>();
//		trackSet.add("TestSet");
//		
//		control.addTrackToOnlyTheseTracksets(track1, trackSet);
//
////		control.setLaptime("user", car1.getId(), track1.getId(), "1:27.000", "87.000", 5);
//		
//		// TODO for some reason this test fails because there is no handicap for this car...
//		
//		Vector<Vector<String>> handicaps = control.getHandicapsTableData("TestSet");
//		String handicap = "";
//		for(Vector<String> h : handicaps) {
//			if(h.contains(car1.getName())) {
//				handicap = h.elementAt(2);
//			}
//		}
//		System.out.println(handicaps.size());
//		System.out.println("Handicap: "+handicap);
//
//		// Difference should be 1.000
//		Assert.assertEquals(handicap, "1.000");
//	}
//}
