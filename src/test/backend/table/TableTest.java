package test.backend.table;

/**
 * @author sdentro
 * @version $Id$
 */

import java.util.Vector;

import org.junit.*;

import code.backend.table.CellObject;
import code.backend.table.Table;


public class TableTest {

	private Table<String, Double, TestObject> tb;
	
	@Before public void setUp() {
		tb = new Table<String, Double, TestObject>();
	}
	
	@After public void reset() {
		tb = new Table<String, Double, TestObject>();
	}
	
	@Test public void addRow() {
		String one = "een";
		
		addRow(one);
		Assert.assertEquals(tb.getRow(one), new Vector<String>());
		
		// Should be no problem
		addRow(null); 
		
		addRow(one);
		Assert.assertEquals(tb.getRow(one), new Vector<String>());
	}
	
	@Test public void removeRow() {
		boolean result = false;
		String one = "een";
		
		addRow(one);
		// Should give no error
		tb.getRow(one);
		tb.removeRow(one);
		
		try {
			tb.getRow(one);
		}
		catch (IndexOutOfBoundsException e) {
			result = true;
		}
		
		Assert.assertTrue(result);
	}
	
	@Test public void addColumn() {
		Double one = new Double(1.0);
		
		addColumn(one);
		Assert.assertEquals(tb.getColumn(one), new Vector<String>());
		
		// Should be fine
		addColumn(null);

		addColumn(one);
		Assert.assertEquals(tb.getColumn(one), new Vector<String>());
	}
	
	@Test public void removeColumn() {
		boolean result = false;
		Double one = new Double(1.0);
		
		addColumn(one);
		tb.getColumn(one);
		tb.removeColumn(one);
		try {
			tb.getColumn(one);
		}
		catch (IndexOutOfBoundsException e) {
			result = true;
		}
		
		Assert.assertTrue(result);
	}
	
	@Test public void setCell() {
		Double colOne = new Double(1.0);
		String rowOne = "een";
		TestObject cellOne = new TestObject("cell one");
		
		addColumn(colOne);
		addRow(rowOne);
		
		tb.setCell(cellOne, rowOne, colOne);
		
		Assert.assertTrue(((TestObject) tb.getCell(rowOne, colOne)).getId().equals(cellOne.getId()));
	}
	
	@Test public void getCell() {
		boolean testOne = false;
		boolean testTwo = false;
		
		Double colOne = new Double(1.0);
		String rowOne = "een";
		
		try {
			tb.getCell(rowOne, colOne);
		}
		catch (IndexOutOfBoundsException e) {
			testOne = true;
		}
		
		
		addColumn(colOne);
		try {
			tb.getCell(rowOne, colOne);
		}
		catch (IndexOutOfBoundsException e) {
			testTwo = true;
		}
		
		setCell();
		
		Assert.assertTrue(testOne && testTwo);
	}
	
	@Test public void clearCell() {
		Double colOne = new Double(1.0);
		String rowOne = "een";
		TestObject cellOne = new TestObject("cell one");
		
		addRow(rowOne);
		addColumn(colOne);
		tb.setCell(cellOne, rowOne, colOne);
		
		Assert.assertFalse(isEmpty(rowOne, colOne));
		
		tb.clearCell(rowOne, colOne);
		Assert.assertTrue(isEmpty(rowOne, colOne));
	}
	
	@Test public void isEmpty() {
		Double colOne = new Double(1.0);
		String rowOne = "een";
		TestObject cellOne = new TestObject("cell one");
		

		Assert.assertTrue(isEmpty(rowOne, colOne));

		
		addRow(rowOne);
		addColumn(colOne);
		Assert.assertTrue(isEmpty(rowOne, colOne));
		
		tb.setCell(cellOne, rowOne, colOne);
		Assert.assertFalse(isEmpty(rowOne, colOne));
	}
	
	private boolean isEmpty(String row, Double column) {
		boolean result = false;
		
		try {
			result = tb.isEmpty(row, column);
		}
		catch (IndexOutOfBoundsException e) {
			result = true;
		}
		
		return result; 
	}

	private void addColumn(Double column) {
		tb.addColumn(column);
	}
	
	private void addRow(String row) {
		tb.addRow(row);
	}
	
}

final class TestObject extends CellObject {
	private String id;
	
	public TestObject(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
}