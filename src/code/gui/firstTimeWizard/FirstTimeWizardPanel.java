package code.gui.firstTimeWizard;

/**
 * @author sdentro
 * @version $Id$
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import code.backend.control.GuiControlInterface;
import code.backend.data.User;

import code.exec.Config;
import code.gui.helperclasses.AbstractGuiPanel;

public class FirstTimeWizardPanel extends AbstractGuiPanel {

	private static final long serialVersionUID = 1923878716219795611L;
	private GuiControlInterface control;
	
	private String text = "Welcome!\n\n" +
			"This is the first time you're running Lapstats. That means a few things need to be" +
			" set up. Think of a user name and set up the Career.blt path.\n\n" +
			"If you'd like to have your latest laptimes loaded automatically at startup, make sure to select the checkbox. Select" +
			" the other checkbox if you want to save all changes when you exit.\n\n" +
			"Thanks for using Lapstats!";
	private JTextArea textTA;
	
	private JLabel image;
	
	private JLabel userNameJL;
	private JTextField userNameValueJT;
	private JLabel careerJL;
	private JTextField careerValueJT;
	
	private JLabel autoLoadCareerJL;
	private JCheckBox autoLoadCareerJCB;
	private JLabel autoSaveDataJL;
	private JCheckBox autoSaveDataJCB;
	
	private JButton saveButtonBT;
	private JButton browseBT;
	
	private ActionListener saveAL = new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			String newName = userNameValueJT.getText();
			String newCareer = careerValueJT.getText();
			
			// Check if important fields are filled in
			if(newName.equals("")) {
				generateErrorPane(null, "User Name cannot be empty.", "Error");
			}
			else {
				// If not, add the user
				control.addUser(newName);
				
				// Set intangibles
				User u = control.getUser(newName);
				u.setCareerLocation(newCareer);
				u.setLoadCareerOnStartup(autoLoadCareerJCB.isSelected());
				u.setSaveDataOnExit(autoSaveDataJCB.isSelected());
				control.setCurrentlyActiveUserName(newName);
				
				control.removeUser(control.getUser("user"));
				
				control.loadCareerOfAllUsers();
				
				getThisAGP().getTopLevelAncestor().setVisible(false);
			}
		}
	};
	
	private ActionListener browseAL = new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			File choosenFile = chooseFile();
			if(choosenFile != null) {
				careerValueJT.setText(choosenFile.getAbsolutePath());
			}
		}
	};
	
	private ActionListener doNothingAL = new ActionListener(){
		public void actionPerformed(ActionEvent e) {}
	};
	
	private KeyListener doNothingKL = new KeyListener() {
		@Override
		public void keyPressed(KeyEvent e) {}
		@Override
		public void keyReleased(KeyEvent e) {}
		@Override
		public void keyTyped(KeyEvent e) {}
	};
	
	
	public FirstTimeWizardPanel(GuiControlInterface control) {
		this.control = control;
		// Empty space
		setupLabelProperties(new JLabel(), "", "h 20px, w 100%, wrap, spanx 3", Config.TEXTFONT);
		
		setupTextAreaProperties(textTA, text, "w 290px:290px:290px, spanx 2", Config.TEXTFONT);
		
		setupImageProperties();
		
		setupSeperatorProperties(new JSeparator(), 0, "w 100%, h 5px, spanx 3, wrap");
		
		setupLabelProperties(userNameJL, "Username", "w 20%, h 1%", Config.TEXTFONT);
		setupTextFieldProperties(userNameValueJT, "w 143px:143px:143px, h 26px:26px:26px, wrap", Config.TEXTFONT, doNothingKL);

		setupLabelProperties(careerJL, "Career location", "w 20%, h 1%", Config.TEXTFONT);
		setupTextFieldProperties(careerValueJT, "w 143px:143px:143px, h 26px", Config.TEXTFONT, doNothingKL);
		setupButtonProperties(browseBT, "Browse", "w 80px:80px:80px, h 25px, wrap", Config.TEXTFONT, browseAL);
		
		setupSeperatorProperties(new JSeparator(), 0, "w 100%, h 5px, spanx 3, wrap");
		
		setupLabelProperties(autoLoadCareerJL, "Load Career.blt on startup", "w 90px, h 20px", Config.TEXTFONT);
		setupCheckBox(autoLoadCareerJCB, "", "wrap", Config.TEXTFONT, false, doNothingAL);
		autoLoadCareerJCB.setSelected(true);
		
		setupLabelProperties(autoSaveDataJL, "Save data on exit", "w 90px, h 20px", Config.TEXTFONT);
		setupCheckBox(autoSaveDataJCB, "", "wrap", Config.TEXTFONT, false, doNothingAL);
		autoSaveDataJCB.setSelected(true);
		
		setupSeperatorProperties(new JSeparator(), 0, "w 100%, h 5px, spanx 3, wrap");
		setupLabelProperties(new JLabel(), "", "h 20px, w 1%", Config.TEXTFONT);
		setupButtonProperties(saveButtonBT, "Save", "w 80px:80px:80px, h 25px, alignx left", Config.TEXTFONT, saveAL);

	}
	
	public void setupImageProperties() {
		ImageIcon icon = new ImageIcon("icon.png","");
		image = new JLabel(icon);
		setupLabelProperties(image, "", "aligny top, alignx center, wrap", Config.TEXTFONT);
	}

	@Override
	protected void initWidgets() {
		textTA = new JTextArea();
		
		image = new JLabel();
		
		careerJL = new JLabel();
		careerValueJT = new JTextField();
		
		userNameJL = new JLabel();
		userNameValueJT = new JTextField();
		
		autoLoadCareerJL = new JLabel();
		autoLoadCareerJCB = new JCheckBox();
		autoSaveDataJL = new JLabel();
		autoSaveDataJCB = new JCheckBox();
		
		saveButtonBT = new JButton();
		browseBT = new JButton();
	}
}
