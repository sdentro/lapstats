package code.gui.main;

/**
 * @author sdentro
 * @version $Id$
 */

import java.awt.Dimension;
import java.awt.Window;
import java.awt.Dialog.ModalityType;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.SwingUtilities;

import code.backend.control.GuiControlInterface;
import code.exec.Config;
import code.gui.firstTimeWizard.FirstTimeWizardPanel;
import code.gui.helperclasses.AbstractGuiFrame;


@SuppressWarnings("serial")
public class MainFrame extends AbstractGuiFrame {
	
	private GuiControlInterface control;
	
	private MainPanel mainPanel;
	
	private WindowListener exitWL = new WindowListener() {

		@Override
		public void windowClosing(WindowEvent arg0) {
			// When the close "X" is pressed, call the exit method
			exit();
		}
		@Override
		public void windowActivated(WindowEvent arg0) {		}
		@Override
		public void windowClosed(WindowEvent arg0) {		}
		@Override
		public void windowDeactivated(WindowEvent arg0) {		}
		@Override
		public void windowDeiconified(WindowEvent arg0) {		}
		@Override
		public void windowIconified(WindowEvent arg0) {		}
		@Override
		public void windowOpened(WindowEvent arg0) {		}
	};
	
	
	public MainFrame(GuiControlInterface control) {
		super(Config.mainFrameTitle, Config.MAIN_FRAME_WIDTH, Config.MAIN_FRAME_HEIGHT);
		this.control = control;
		setIconImage(new ImageIcon(Config.ICON_LOCATION).getImage()); 

		mainPanel = new MainPanel(this, this.control);
		mainPanel.setSize(Config.MAIN_FRAME_WIDTH, Config.MAIN_FRAME_HEIGHT);
		add(mainPanel);
		
		addWindowListener(exitWL);
		setVisible(true);

		if(control.getFirstTime()) {
			firstSession();
			mainPanel.updateVisibleTabPanel();
		}
	}
	
	public void firstSession() {
		this.setEnabled(false);
		
		Window win = SwingUtilities.getWindowAncestor(mainPanel);
		JDialog dialog = new JDialog(win, "First run wizard", ModalityType.APPLICATION_MODAL);
	    dialog.setPreferredSize(new Dimension(405, 425));
	    dialog.add(new FirstTimeWizardPanel(control));
	    dialog.pack();
	    dialog.setLocationRelativeTo(null);
	    dialog.setVisible(true);

	    this.setEnabled(true);
	}
	
	public StatusBarPanel getStatusBar() {
		return mainPanel.getStatusBar();
	}
	
	/**
	 * Method that should be called when closing Lapstats
	 */
	public void exit() {
		// Check if the user wants the data to be saved
		if(control.getUser(control.getCurrentlyActiveUserName()).getSaveDataOnExit()) {
			control.saveDataToFile();
		}
		
		// Now exit
		System.exit(0);
	}
}
