package code.gui.main;

/**
 * @author sdentro
 * @version $Id$
 */

import javax.swing.JLabel;

import code.exec.Config;
import code.gui.helperclasses.AbstractGuiPanel;

@SuppressWarnings("serial")
public class StatusBarPanel extends AbstractGuiPanel {

	private JLabel message;
	
	public StatusBarPanel() {
		super();
		message = new JLabel();
		setupLabelProperties(message, "", "w 100%, h 15px:15px:15px", Config.STATUSBARFONT);
	}
	
	@Override
	protected void initWidgets() {
		message = new JLabel();
	}

	public void setMessage(String message) {
		this.message.setText(message);
		validate();
	}
}
