package code.gui.main;

/**
 * @author sdentro
 * @version $Id$
 */

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTabbedPane;

import net.miginfocom.swing.MigLayout;
import code.backend.control.GuiControlInterface;
import code.exec.Config;
import code.gui.helperclasses.AbstractGuiPanel;
import code.gui.helperclasses.AbstractStandardTabPanel;
import code.gui.menuBar.CarEditPanel;
import code.gui.menuBar.TrackEditPanel;
import code.gui.menuBar.TrackSetEditPanel;
import code.gui.menuBar.UserEditPanel;
import code.gui.tabs.CarTab;
import code.gui.tabs.HandicapTab;
import code.gui.tabs.StatisticsTab;
import code.gui.tabs.TrackSetsTab;
import code.gui.tabs.TrackTab;

public class MainPanel extends AbstractGuiPanel {

	private static final long serialVersionUID = 3490936404039852745L;
	private GuiControlInterface control;
	private MainFrame parent;
	private JTabbedPane tabPane;
	private StatusBarPanel statusPanel;
	
	private CarTab carTab;
	private TrackTab trackTab;
	private HandicapTab handicapTab;
	private TrackSetsTab trackSetsTab;
	private StatisticsTab statisticsTab;
	
	private ActionListener aboutMenuAction = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			generateInformationMessagePane(null, Config.aboutText, "About");
		}
	};
	
	private ActionListener editMenuAction = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			generateDialogPanel(getParent(), new UserEditPanel(control), "Edit user", new Dimension(447, 170));
			updateVisibleTabPanel();
		}
	};
	
	private ActionListener editTrackSetMenuAction = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			generateDialogPanel(getParent(), new TrackSetEditPanel(control), "Edit TrackSet", new Dimension(675, 400));
			updateVisibleTabPanel();
		}
	};
	
	private ActionListener trackEditMenuAction = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			generateDialogPanel(getParent(), new TrackEditPanel(control), "Edit track", new Dimension(500, 400));
			updateVisibleTabPanel();
		}
	};
	
	private ActionListener carEditMenuAction = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			generateDialogPanel(getParent(), new CarEditPanel(control), "Edit car", new Dimension(500, 400));
			updateVisibleTabPanel();
		}
	};
	
	private ActionListener exitMenuAction = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			parent.exit();
		}
	};
	
	private ActionListener importSettingsAction = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			File choosenFile = chooseFile();
			if(choosenFile != null) {
				control.importSettings(choosenFile);
				updateVisibleTabPanel();
			}
		}
	};
	
	private ActionListener exportSettingsAction = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			File choosenFile = chooseFile();
			if(choosenFile != null) {
				control.exportSettings(choosenFile);
				updateVisibleTabPanel();
			}
		}
	};
	
	private ActionListener saveMenuAction = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			control.saveDataToFile();
		}
	};
	
	private ActionListener loadAllCareersMenuAction = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			control.loadCareerOfAllUsers();
			updateVisibleTabPanel();
		}
	};
	
	private ActionListener loadCareerMenuAction = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			if(control.getCurrentlyActiveUserName().equals("") || 
					control.getUser(control.getCurrentlyActiveUserName()).getCareerLocation().equals("")) {
				File choosenFile = chooseFile();
				control.readCareerFromFile(choosenFile);
			}
			else {
				control.readCareerOfCurrentlyActiveUser();
			}
			updateVisibleTabPanel();
		}
	};
	
	public MainPanel(MainFrame parent, GuiControlInterface control) {
		this.parent = parent;
		this.control = control;
		setLayout(new MigLayout("gap 0 0 0 0"));

		createMenuBar();
		
		setupTabbedPaneProperties(tabPane, "width 100%, height 100%, wrap", Config.TEXTFONT);
		
		carTab = new CarTab(this.control, tabPane);
		tabPane.addTab("Cars", carTab);	
		
		trackTab = new TrackTab(this.control, tabPane);
		tabPane.addTab("Tracks", trackTab);

		handicapTab = new HandicapTab(this.control, tabPane);
		tabPane.addTab("Handicaps", handicapTab);

		trackSetsTab = new TrackSetsTab(this.control, tabPane);
		tabPane.add("Track Sets", trackSetsTab);
		
		statisticsTab = new StatisticsTab(this.control, tabPane);
		tabPane.add("Statistics", statisticsTab);

		statusPanel = new StatusBarPanel();
		add(statusPanel, "grow");
		
	}
	
	@Override
	protected void initWidgets() {
		tabPane = new JTabbedPane();
	}
	
	private Component getSelectedTabPane() {
		return tabPane.getSelectedComponent();
	}
	
	protected StatusBarPanel getStatusBar() {
		return statusPanel;
	}
	
	public void updateVisibleTabPanel() {
		Component c = getSelectedTabPane();
		if(c instanceof AbstractStandardTabPanel) {
			((AbstractStandardTabPanel) c).refreshComponentData();
		}
	}
	
	private void createMenuBar() {
		JMenuBar menuBar = new JMenuBar();
		parent.setJMenuBar(menuBar);
		
		JMenu fileMenu = addMenu(menuBar, "File");
		addMenuItem("Load Career.blt", fileMenu, loadCareerMenuAction);
		addMenuItem("Load all Careers", fileMenu, loadAllCareersMenuAction);
		addMenuItem("Save data", fileMenu, saveMenuAction);
		fileMenu.addSeparator();
//		addMenuItem("Import data.dat v1.2.x", fileMenu, importDataMenuAction);
		addMenuItem("Import settings", fileMenu, importSettingsAction);
		addMenuItem("Export settings", fileMenu, exportSettingsAction);
		fileMenu.addSeparator();
		addMenuItem("Exit", fileMenu, exitMenuAction);

		JMenu editMenu = addMenu(menuBar, "Edit");
		addMenuItem("Car", editMenu, carEditMenuAction);
		addMenuItem("Track", editMenu, trackEditMenuAction);
		addMenuItem("Track Set", editMenu, editTrackSetMenuAction);
		addMenuItem("User", editMenu, editMenuAction);

		JMenu helpMenu = addMenu(menuBar, "Help");
		addMenuItem("About", helpMenu, aboutMenuAction);
	}
	
	private void addMenuItem(String itemTitle, JMenu menu, ActionListener action) {
		JMenuItem aboutAction = new JMenuItem(itemTitle);
		menu.add(aboutAction);
		aboutAction.addActionListener(action);
	}
	
	private JMenu addMenu(JMenuBar menuBar, String menuName) {
		JMenu menu = new JMenu(menuName);
		menuBar.add(menu);
		return menu;
	}
}
