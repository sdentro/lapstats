package code.gui.helperclasses;

/**
 * @author sdentro
 * @version $Id$
 */

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;

import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SortOrder;
import javax.swing.RowSorter.SortKey;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableRowSorter;

import code.backend.control.Control;
import code.backend.control.GuiControlInterface;

import code.exec.Config;


public abstract class AbstractTabPanel extends AbstractGuiPanel {

	private static final long serialVersionUID = 7076896532178363407L;
	
	protected JTabbedPane parent;
	protected GuiControlInterface control;
	
	protected LapstatsTable table;
	protected JScrollPane tableScrollPane;
	protected Vector<String> tableHeader;	

	/**
	 * This listener is called upon when a tab is selected to reload the list data.
	 */
	protected ComponentListener reloadComponentCL = new ComponentListener() {
		@Override
		public void componentShown(ComponentEvent e) {
			refreshComponentData();
		}
		
		@Override
		public void componentHidden(ComponentEvent e) {}
		@Override
		public void componentMoved(ComponentEvent e) {}
		@Override
		public void componentResized(ComponentEvent e) {}
	};
	
	/**
	 * KeyListener of a list that updates the table with the newly
	 * selected item.
	 */
	protected ListSelectionListener listLSL = new ListSelectionListener() {
		@Override
		public void valueChanged(ListSelectionEvent e) {
			// Make sure this get's only done at onClick, not at onRelease
			if(e.getValueIsAdjusting()) {
				updateTableWithCurrentSelection();
			}
		}
	};
	
	/**
	 * KeyListener of a list that updates the table with the newly
	 * selected item.
	 */
	protected KeyListener listKL = new KeyListener() {
		@Override
		public void keyPressed(KeyEvent e) {
			 int key = e.getKeyCode();
		     if (key == KeyEvent.VK_ENTER) {  	
		    	 updateTableWithCurrentSelection();
		     }
		}
		@Override
		public void keyReleased(KeyEvent e) {}
		@Override
		public void keyTyped(KeyEvent e) {}
	};
	
	/**
	 * MouseListener that updates the details panel when a row is selected in the table
	 */
	protected MouseListener tableClickAL = new MouseListener() {
		@Override
		public void mouseClicked(MouseEvent e) {
			if (e.getClickCount() == 2){
				tableDoubleClickedAction();
			}
			updateDetails();
		}

		@Override
		public void mouseEntered(MouseEvent e) {}
		@Override
		public void mouseExited(MouseEvent e) {}
		@Override
		public void mousePressed(MouseEvent e) {}
		@Override
		public void mouseReleased(MouseEvent e) {}
	};
	
	/**
	 * This method needs to update the table data according to
	 * the current selection across the input fields.
	 */
	protected abstract void updateTableWithCurrentSelection();
	
	/**
	 * Update the table from AbstractTabPanel with the data given at
	 * the param.
	 * @param tableData
	 */
	protected void updateTable(Vector<Vector<String>> tableData) {
		tableHeader = Control.TRACKSETS_TABLE_HEADERS;
		setupTable(tableData, tableHeader);
		
		updateScrollPaneProperties(tableScrollPane, table);
		
		validate();
	}
	
	/**
	 * This method takes care of the setting up that needs to be done when inserting a table.
	 * @param data The data that needs to go in. The new table is stored in the table field
	 * of AbstractTabPanel for future reference.
	 * @param colNames The column names.
	 */
	protected void setupTable(Vector<Vector<String>> data, Vector<String> colNames) {	
		table = new LapstatsTable(data, colNames);
		setupTableProperties(table, Config.TEXTFONT, tableClickAL);
		table = setupSorting(table);
		
		if(table.getColumnModel().getColumnCount() > 0) { // ie. there actually are columns 
			setupTableColumns(table);
		}
	}
	
	/**
	 * Convenience method that takes care of setting up sorting. It uses the comparator
	 * specified inside the table.
	 */
	protected LapstatsTable setupSorting(LapstatsTable lt) {
		TableRowSorter<LapstatsTableModel> trs = (TableRowSorter) lt.getRowSorter();
		for(int i=0; i<lt.getColumnCount(); i++) {
			trs.setComparator(i, lt.getComparator());
		}
		lt.setRowSorter(trs);
		lt.getRowSorter().toggleSortOrder(getPreferredSortedColumn());
		return lt;
	}
	
	/**
	 * This method returns the number of the column that should be automatically sorted
	 * in a table. The column id's start at 0.
	 * @return
	 */
	protected abstract int getPreferredSortedColumn();
	
	/**
	 * This function basically sets up the columns of the table. This means that cell width, alignment of text
	 * and cell background colors need to be set here.
	 * @param table
	 */
	protected abstract void setupTableColumns(LapstatsTable table);

	/**
	 * This method is called when the table is double clicked.
	 */
	protected abstract void tableDoubleClickedAction();
	
	/**
	 * Update the details according to the recent selections in list and table.
	 */
	protected abstract void updateDetails();
	
	/**
	 * This method takes care of all the setting up that needs to be done when inserting a list into a panel.
	 * Since this is a more Lapstats specific method it is not inserted into the AbstractGUIPanel. The params
	 * are used to set up the list, while the return contains the new list with it's data. That can be used
	 * to store inside a specific class for future reference.
	 * @param list The JList that needs to be setup.
	 * @param data The data that needs to go in the list.
	 * @param scrollPane The ScrollPane that surrounds the list.
	 * @param scrollPaneConstraints The MigLayout constraints for the ScrollPane.
	 * @param selectedElement The index of the element that needs to be selected in the list.
	 * @param listKL The KeyListener.
	 * @param listLSL The ListSelectionListener.
	 * @return The list after all the alterations are done. Can be used to store for future reference.
	 */
	protected JList setupList(JList list, Vector<String> data, JScrollPane scrollPane, String scrollPaneConstraints, int selectedElement, KeyListener listKL, ListSelectionListener listLSL) {
		removeComponentAndMakeInvisible(list);
		
		list = new JList(data);
		setupList(list, "w 100%, h 100%", Config.TEXTFONT, listKL, listLSL);
		setupScrollPaneProperties(scrollPane, list, scrollPaneConstraints);
		list.setSelectedIndex(selectedElement);

		validate();
		
		return list;
	}
	
	/**
	 * This method takes care of all the stuff that needs to be done when updating a list.
	 * Since this is a more Lapstats specific method it is not inserted into the AbstractGUIPanel. The params
	 * are used to update the list, while the return contains the new list with it's data. That can be used
	 * to store inside a specific class for future reference.
	 * @param list The JList that needs to be updated.
	 * @param data The data that needs to go in the list.
	 * @param scrollPane The ScrollPane that surrounds the list.
	 * @param selectedElement The index of the element that needs to be selected in the list.
	 * @param listKL The KeyListener.
	 * @param listLSL The ListSelectionListener.
	 * @return The list after all the alterations are done. Can be used to store for future reference.
	 */
	protected JList updateList(JList list, Vector<String> data, JScrollPane scrollPane, int selectedElement, KeyListener listKL, ListSelectionListener listLSL) {
		removeComponentAndMakeInvisible(list);
		
		list = new JList(data);
		setupList(list, "w 100%, h 100%", Config.TEXTFONT, listKL, listLSL);
		updateScrollPaneProperties(scrollPane, list);
		list.setSelectedIndex(selectedElement);

		validate();
		
		return list;
	}

	@Override
	protected abstract void initWidgets();
	
	/**
	 * This method calls refreshComponentData(listData) with the right data.
	 */
	public abstract void refreshComponentData();
	
	/**
	 * This method can be called when searching for a specific String.
	 * @param searchTerm The word we're looking for.
	 * @param data The set of data we're searching in.
	 * @return A Vector<String> that contains all elements that contain the searchTerm. I.e. 
	 * part of the element has the word searchTerm in it.
	 */
	protected Vector<String> searchFor(String searchTerm, Vector<String> data) {
		Vector<String> returnVector = new Vector<String>();
		for(String setName : data) {
			if(setName.toLowerCase().contains(searchTerm)) {
				returnVector.add(setName);
			}
		}
		return control.sort(returnVector);
	}
	
	/**
	 * Get the control entity of this tab.
	 * @param control
	 */
	protected void setControl(GuiControlInterface control) {
		this.control = control;
	}
	
	/**
	 * Get the parent of this tab.
	 * @param parent
	 */
	public void setParent(JTabbedPane parent) {
		this.parent = parent;
	}
}
