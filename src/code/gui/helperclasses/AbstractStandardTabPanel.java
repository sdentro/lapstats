package code.gui.helperclasses;

/**
 * @author sdentro
 * @version $Id$
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import code.exec.Config;

@SuppressWarnings("serial")
public abstract class AbstractStandardTabPanel extends AbstractTabPanel {

	protected JTextField searchTextField;
	private JButton searchButton;
	protected JList list;
	protected JScrollPane listScrollPane;
	
	protected JComboBox firstCombobox;
	protected JComboBox secondCombobox;
	protected JComboBox thirdCombobox;
	protected JButton resetFiltersButton;
	
	protected Vector<String> listData;
	protected int selected;
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ActionListeners, KeyListeners and WindowListeners	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	private ActionListener searchAL = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) { search(); }
	};
	
	private KeyListener searchKL = new KeyListener() {
		@Override
		public void keyPressed(KeyEvent e) {
			 int key = e.getKeyCode();
		     if (key == KeyEvent.VK_ENTER) {  search(); }
		}
		@Override
		public void keyReleased(KeyEvent e) {}
		@Override
		public void keyTyped(KeyEvent e) {}
	};
	
	private ActionListener resetFiltersAL = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) { 
			updateTable(getTableData());
			firstCombobox.setSelectedIndex(0);
			secondCombobox.setSelectedIndex(0);
			thirdCombobox.setSelectedIndex(0);
		}
	};
	
	/**
	 * This AL takes care of the clicking event on the fist combobox. 
	 */
	private ActionListener filterComboboxAL = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			filterTableData();
		}
	};
	
	/**
	 * Filter the data of the table by the object(s) that is/are selected in the {@link JCombobox}es. 
	 * @param selectedObject The Object that needs to be compared.
	 * @param columnToCompare The id of the column that the selectedObject should be in.
	 */
	protected abstract void filterTableData();

	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Setup window components	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	/**
	 * Set up the main layout of the TabPanel. This method adds the search {@link JTextField}, the search {@link JButton},
	 * the {@link JComboBox}es, the reset {@link JButton}, the table and the details to the screen. When a new TabPanel
	 * is created, just call this method in the constructor and your basic layout is setup.
	 */
	public void paint() {
		this.addComponentListener(reloadComponentCL);

		setupTextFieldProperties(searchTextField, "w 20%, h 26px", Config.TEXTFONT, searchKL);
		setupButtonProperties(searchButton, "Search", "w 8%, h 25px", Config.TEXTFONT, searchAL, searchKL);
		setupComboboxes();
		setupButtonProperties(resetFiltersButton, "Reset", "w 88:88:88px, h 25px, wrap", Config.TEXTFONT, resetFiltersAL);
		setupList(listData);
		setupTable(new Vector<Vector<String>>(), tableHeader);
		setupScrollPaneProperties(tableScrollPane, table, "w 70%, h 65%, spany 1, spanx 6, wrap");
		setupDetails();
	}
	
	public void initWidgets() {
		listData = new Vector<String>();
		selected = 0;
		tableHeader = new Vector<String>();
		
		tableScrollPane = new JScrollPane();
		listScrollPane = new JScrollPane();
		searchButton = new JButton();
		searchTextField = new JTextField();
		list = new JList();
		resetFiltersButton = new JButton();
	}
	
	/**
	 * Set up the three {@link JCombobox}es above the table. This is not included in initWidgets() because
	 * of the functions that need to be called to set them up.
	 */
	private void setupComboboxes() {
		firstCombobox = new JComboBox(getDataForFirstCombobox());
		secondCombobox = new JComboBox(getDataForSecondCombobox());
		thirdCombobox = new JComboBox(getDataForThirdCombobox());
		
		setupComboBox(firstCombobox, "w 93:93:93px, h 25px", Config.TEXTFONT, filterComboboxAL);
		setupComboBox(secondCombobox, "w 189:189:189px, h 25px", Config.TEXTFONT, filterComboboxAL); 
		setupComboBox(thirdCombobox, "w 149:149:149px, h 25px, spanx 3", Config.TEXTFONT, filterComboboxAL);
	}
	
	/**
	 * Update the list in the panel. Use this method when a selection change has been made.
	 * @param data The {@link Vector} that contains the data that needs to be displayed in the list.
	 * @author sdentro
	 */
	protected void setupList(Vector<String> data) {
		list = setupList(list, data, listScrollPane, "w 30%, h 100%, spanx 2, spany 10", selected, listKL, listLSL);
	}
	
	/**
	 * Update the list in the panel. Use this method when a selection change has been made.
	 * @param data The {@link Vector} that contains the data that needs to be displayed in the list.
	 * @author sdentro
	 */
	protected void updateList(Vector<String> data) {
		list = updateList(list, data, listScrollPane, selected, listKL, listLSL);
	}
	
	protected void updateTableWithCurrentSelection() {
		updateTable(getTableData());
	}
	
	/**
	 * This function basically sets up the columns of the table. This means that cell width, alignment of text
	 * and cell background colors need to be set here.
	 * @param table
	 */
	protected abstract void setupTableColumns(LapstatsTable table);
	
	/**
	 * Setup the objects that are needed to display the details of the selections in the list and table.
	 */
	protected abstract void setupDetails();
	
	/**
	 * Get the data that needs to go into the first {@link JCombobox}. This is the Combobox on the left.
	 * @return A {@link Vector} containing {@link String}s that can go into the Combobox.
	 */
	protected abstract Vector<String> getDataForFirstCombobox();
	
	/**
	 * Get the data that needs to go into the second {@link JCombobox}. This is the Combobox at center.
	 * @return A {@link Vector} containing {@link String}s that can go into the Combobox.
	 */
	protected abstract Vector<String> getDataForSecondCombobox();
	
	/**
	 * Get the data that needs to go into the third {@link JCombobox}. This is the Combobox on the right.
	 * @return A {@link Vector} containing {@link String}s that can go into the Combobox.
	 */
	protected abstract Vector<String> getDataForThirdCombobox();
	
	
	/**
	 * This method updates all the data inside the different {@link Component}s inside this tab panel.
	 * @param List data contains the data for the list. This is a param because this is the only difference for
	 * each specific Tab. Putting this method here saves writing code.
	 */
	public void refreshComponentData(Vector<String> listData) {
		int selectedItem = list.getSelectedIndex();
		listData = control.sort(listData);
		updateList(listData);
		list.setSelectedIndex(selectedItem);
		updateTable(getTableData());
		resetComboboxes();
	}
	
	/**
	 * Update the table with the data given at the param.
	 * @param data A {@link Vector} table containing the data for the table.
	 */
	protected abstract void updateTable(Vector<Vector<String>> tableData);

	
	/**
	 * Method that updates the data in the three comboboxes
	 */
	public void resetComboboxes() {
		updateCombobox(firstCombobox, getDataForFirstCombobox());
		updateCombobox(secondCombobox, getDataForSecondCombobox());
		updateCombobox(thirdCombobox, getDataForThirdCombobox());
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Functions	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	/**
	 * Method that can be called when a search needs to be done
	 */
	protected void search() {
		String searchTerm = searchTextField.getText().toLowerCase();
		
		listData = control.sort(searchFor(searchTerm));
		updateList(listData);
		updateDetails();
		
		// update table with new data		
		updateTable(getTableData());
		
		searchTextField.setText("");
	}
	
	/**
	 * This function sets the specific way to search and in what to search. This can be different for every
	 * tab. That's why this method needs to be overridden.
	 * @param searchTerm
	 * @return A {@link Vector} of {@link String}s containing all the values that look like the searchTerm.
	 */
	protected abstract Vector<String> searchFor(String searchTerm);
	
	/**
	 * This method fetches the data for the table to put on display
	 * @param selectedItem The item selected at the GUI
	 * @return A Vector<Vector<String>> table that can be inserted into a JTable
	 */
	protected abstract Vector<Vector<String>> getTableData();
}
