package code.gui.helperclasses;

/**
 * @author sdentro
 * @version $Id$
 */

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JFrame;

/**
 * An abstract {@link JFrame} representing the base frame from which the other frames are built.
 * @author Joey Ezechiels
 */
@SuppressWarnings("serial")
public abstract class AbstractGuiFrame extends JFrame {

	/**
	 * Constructs the Frame.
	 * @param title The title of the frame.
	 */
	public AbstractGuiFrame(String title, int width, int height){
		super(title);
		
		Dimension screenResolution = Toolkit.getDefaultToolkit().getScreenSize();
		Point midOfScreen = new Point(screenResolution.width/2, screenResolution.height/2);
		setBounds(midOfScreen.x-width/2, midOfScreen.y-height/2, width+3, height+28);
//		setVisible(true);
	}
}
