package code.gui.helperclasses;

/**
 * @author sdentro
 * @version $Id$
 */

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.Window;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.EventListener;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionListener;

import code.exec.Config;

import net.miginfocom.swing.MigLayout;

/**
 * An abstract JPanel representing the base Panel from which all other panels are built.
 */
@SuppressWarnings("serial")
public abstract class AbstractGuiPanel extends JPanel {
	private final AbstractGuiPanel agp = this;
	
	/**
	 * The most basic constructor available. Creates an AbstractGuiPanel.
	 * @param control The {@link Control} object used to get the info needed to actually do useful things with an {@link AbstractGuiPanel}.
	 */
	public AbstractGuiPanel() {
		this(new MigLayout());
	}
	
	/**
	 * Creates an AbstractGuiPanel.
	 * @param layout The LayoutManager to use.
	 * @param control The {@link Control} object used to get the info needed to actually do useful things with an {@link AbstractGuiPanel}.
	 */
	public AbstractGuiPanel(LayoutManager layout) {
		setLayout(layout);
		initWidgets(); //initialize the buttons, labels and textfields.
	}
	
	/**
	 * Initializes the widgets specified in classes extending {@link AbstractGuiPanel}. The call to this method is done automatically,
	 * and should not be manually invoked.
	 */
	protected abstract void initWidgets();
	
	/**
	 * @return  The current panel, cast as the supertype {@link AbstractGuiPanel}. Used primarily to give other {@link AbstractGuiPanel}'s access to this panel.
	 */
	public final AbstractGuiPanel getThisAGP() {
		return this.agp;
	}
	
	
	/**
	 * Convenience method to make components invisible and remove them from this panel.
	 */
	public void removeComponentAndMakeInvisible(Component component) {
		component.setVisible(false);
		remove(component);
	}
	
	/**
	 * Convenience method to both create and set the properties of a {@link JButton} that are used most in the GUI code.
	 * Sets button visible by default.
	 * @param button The JButton on which to work.
	 * @param caption The caption of button.
	 * @param constraints The {@link MigLayout} constraints used for button.
	 * @param font The {@link Font} with which to display the caption.
	 * @param eventListeners The {@link EventListener}s to use with button, for now only KeyListeners and ActionListeners are added!.
	 */
	public void setupButtonProperties(JButton button, String caption, String constraints, Font font, EventListener... eventListeners) {
		button.setText(caption);
		if(eventListeners != null) {
			for(EventListener listener : eventListeners){
				if(listener instanceof KeyListener) {				
					button.addKeyListener((KeyListener) listener);
				}
				else if(listener instanceof ActionListener) {
					button.addActionListener((ActionListener) listener);
				}
			}
		}
		button.setFont(font);
		button.setVisible(true);
		add(button, constraints);
	}

	/**
	 * Convenience method to both create and set the properties of a {@link JTextField} that are used most in the GUI code.
	 * Sets textField visible by default.
	 * @param textField  The JTextField on which to work.
	 * @param constraints The {@link MigLayout} constraints used for textField.
	 * @param font The {@link Font} with which to display the text of textField.
	 * @param keyListeners The {@link KeyListener}s to use with textField.
	 */
	public void setupTextFieldProperties(JTextField textField, String constraints, Font font, KeyListener... keyListeners) {
		textField.selectAll();
		for(KeyListener listener : keyListeners){
			textField.addKeyListener(listener);		
		}
		textField.setFont(font);
		textField.setVisible(true);
		add(textField, constraints);
	}
	
	/**
	 * Convenience method to both create and set the properties of a {@link JTextArea} that are used most in the GUI code.
	 * Sets textArea visible by default. The Area will be non-editable, line wrapping and keeping words together by default.
	 * @param textArea  The JTextField on which to work.
	 * @param constraints The {@link MigLayout} constraints used for textArea.
	 * @param font The {@link Font} with which to display the text of textArea.
	 * @param keyListeners The {@link KeyListener}s to use with textArea.
	 */
	public void setupTextAreaProperties(JTextArea textArea, String text, String constraints, Font font){
		textArea.setText(text);
		textArea.setFont(font);
		textArea.setEditable(false);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setVisible(true);
		add(textArea, constraints);
	}
	
	/**
	 * Convenience method to both create and set the properties of a {@link JLabel} that are used most in the GUI code.
	 * Sets label visible by default.
	 * @param label The JTextField on which to work.
	 * @param caption The caption of label.
	 * @param constraints The {@link MigLayout} constraints used for label.
	 * @param font The {@link Font} with which to display the text of label.
	 */
	public void setupLabelProperties(JLabel label, String caption, String constraints, Font font){
		setupLabelProperties(this, label, caption, constraints, font);
	}
	
	/**
	 * Convenience method to both create and set the properties of a {@link JLabel} that are used most in the GUI code.
	 * Sets label visible by default.
	 * @param label The JTextField on which to work.
	 * @param caption The caption of label.
	 * @param constraints The {@link MigLayout} constraints used for label.
	 * @param font The {@link Font} with which to display the text of label.
	 */
	public void setupLabelProperties(Container container, JLabel label, String caption, String constraints, Font font){
		label.setText(caption);
		label.setFont(font);
		label.setVisible(true);
		container.add(label, constraints);
	}

	/**
	 * Convenience method to both create and set the properties of a {@link JTabbedPane} that are used most in the GUI code.
	 * Sets tabbedPane visible by default.
	 * @param tabbedPane The {@link JTabbedPane} on which to work.
	 * @param constraints The {@link MigLayout} constraints used for tabbedPane.
	 * @param font font The {@link Font} with which to display the tabbedPane.
	 */
	public void setupTabbedPaneProperties(JTabbedPane tabbedPane, String constraints, Font font){
		tabbedPane.setFont(font);
		tabbedPane.setVisible(true);
		add(tabbedPane, constraints);
	}

	/**
	 * Convenience method to both create and set the properties of a {@link JTable} that are used most in the GUI code.
	 * @param table The {@link JTable} on which to work.
	 */
	protected  void setupTableProperties(JTable table){
        table.setFillsViewportHeight(true);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}
	
	/**
	 * Convenience method to both create and set the properties of a {@link JTable} that are used most in the GUI code.
	 * @param table The {@link JTable} on which to work.
	 * @param font The {@link Font} for the text.
	 * @param mouseListeners The {@link MouseListener}s that need to listen.
	 */
	protected  void setupTableProperties(JTable table, Font font, MouseListener...mouseListeners){
        setupTableProperties(table);
        for(MouseListener al : mouseListeners) {
        	table.addMouseListener(al);
        }
		table.setFont(font);
	}

	/**
	 * Convenience method to both create and set the properties of a {@link JScrollPane} that are used most in the GUI code.
	 * Sets scrollPane visible by default.
	 * @param scrollPane The {@link JScrollPane} on which to work.
	 * @param component The {@link Component} that scrollPane should wrap.
	 * @param constraints The {@link MigLayout} constraints used for scrollPane.
	 */
	public void setupScrollPaneProperties(JScrollPane scrollPane, Component component, String constraints){
		scrollPane.setViewportView(component);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setVisible(true);
		if(component instanceof JTable) {
			JTable table = (JTable)component;
			setupTableProperties(table);
		}
		add(scrollPane, constraints);	
	}
	
	/**
	 * Update a scrollpane with new data.
	 * @param scrollPane The scrollpane that needs to be updated.
	 * @param component The component that needs to be inserted into the ScrollPane
	 * @author sdentro
	 */
	public void updateScrollPaneProperties(JScrollPane scrollPane, Component component) {
		scrollPane.setViewportView(component);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setFont(Config.TEXTFONT);
	}

	/**
	 * Convenience method to both create and set the properties of a {@link JSeparator} that are used most in the GUI code.
	 * Sets seperator visible by default.
	 * @param seperator The {@link JSeparator} on which to work.
	 * @param orientation The orientation of seperator. Most used values are JSeparator#HORIZONTAL, JSeparator#VERTICAL_SCROLLBAR_ALWAYS,
	 * 								JSeparator#HORIZONTAL_SCROLLBAR_AS_NEEDED and JSeparator#VERTICAL_SCROLLBAR_AS_NEEDED.
	 * @param constraints The {@link MigLayout} constraints used for seperator.
	 */
	public void setupSeperatorProperties(JSeparator seperator, int orientation, String constraints){
		seperator.setOrientation(orientation);
		seperator.setVisible(true);
		add(seperator, constraints);
	}
	
	/**
	 * Convenience method to both create and set the properties of a {@link ButtonGroup} that are used most in the GUI code.
	 * @param buttonGroup The {@link ButtonGroup} on which to work.
	 * @param radioButtons The {@link JRadioButton}s to include in buttonGroup. This number may be 0 or more.
	 */
	public void setupButtonGroupProperties(ButtonGroup buttonGroup, JRadioButton... radioButtons){
		for(JRadioButton radioButton : radioButtons){
			buttonGroup.add(radioButton);
		}
	}
	
	/**
	 * Convenience method to both create and set the properties of a {@link JRadioButton} that are used most in the GUI code.
	 * Sets radioButton visible by default.
	 * @param radioButton The {@link JRadioButton} on which to work.
	 * @param caption The caption of radioButton.
	 * @param constraints The {@link MigLayout} constraints used for radioButton.
	 * @param font The {@link Font} used for radioButton.
	 * @param select If <code>true</code>, this radioButton will be selected.
	 * @param actionListeners The {@link ActionListener}s to use with radioButton.
	 */
	public void setupRadioButton(JRadioButton radioButton, String caption, String constraints, Font font, boolean select, ActionListener... actionListeners){
		radioButton.setText(caption);
		for(ActionListener listener : actionListeners){
			radioButton.addActionListener(listener);			
		}
		radioButton.setFont(font);
		if(select) radioButton.setSelected(select);
		radioButton.setVisible(true);
		add(radioButton, constraints);
	}
	
	/**
	 * Convenience method to both create and set the properties of a {@link JCheckBox} that are used most in the GUI code.
	 * Sets checkBox visible by default.
	 * @param checkBox
	 * @param caption
	 * @param constraints
	 * @param font
	 * @param select
	 * @param actionListeners
	 */
	public void setupCheckBox(JCheckBox checkBox, String caption, String constraints, Font font, boolean select, ActionListener... actionListeners){
		checkBox.setText(caption);
		for(ActionListener listener : actionListeners){
			checkBox.addActionListener(listener);			
		}
		checkBox.setFont(font);
		if(select) checkBox.setSelected(select);
		checkBox.setVisible(true);
		add(checkBox, constraints);
	}
	
	/**
	 * Convenience method to both create and set the properties of a {@link JComboBox} that are used most in the GUI code.
	 * Sets comboBox visible by default.
	 * @param comboBox The {@link JComboBox} on which to work.
	 * @param constraints The {@link MigLayout} constraints used for comboBox.
	 * @param font The {@link Font} used for comboBox.
	 * @param actionListeners The {@link ActionListener}s to use with comboBox.
	 */
	public void setupComboBox(JComboBox comboBox, String constraints, Font font, ActionListener... actionListeners){
		for(ActionListener listener : actionListeners){
			comboBox.addActionListener(listener);
		}
		comboBox.setVisible(true);
		comboBox.setFont(font);
		add(comboBox, constraints);
	}
	
	/**
	 * Method that can update a combobox
	 * @param box the box that needs updating
	 * @param data the data that needs to be inserted
	 */
	protected void updateCombobox(JComboBox box, Vector<String> data) {
		box.removeAllItems();
		for(String element : data) {
			box.addItem(element);
		}
	}
	
	/**
	 * Convenience method to both create and set the properties of a {@link JList} that are used most in the GUI code.
	 * Sets list visible by default.
	 * @param list The {@link JList} on which to work.
	 * @param constraints The {@link MigLayout} constraints used for list.
	 * @param font The {@link Font} used for list.
	 * @param eventListeners All listeners that should be added to the list. Only ListSelectionListeners and KeyListeners are added as of now.
	 */
	public void setupList(JList list, String constraints, Font font, EventListener...eventListeners) {
		list.setFont(font);
		list.setVisible(true);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		for(EventListener listener : eventListeners) {
			
			if(listener instanceof ListSelectionListener) {
				list.addListSelectionListener((ListSelectionListener) listener);
			}
			else if(listener instanceof KeyListener) {
				list.addKeyListener((KeyListener) listener);
			}
		}
		add(list, constraints);
	}

	/**
	 * Draw an error pane on screen. Use this when the user needs to see an error message.
	 * @param parent the Component that generates this screen (can be null).
	 * @param messageBody the body that contains the message for the user;
	 * @param frameTitle the title of the Frame that contains the warning
	 * @author sdentro
	 */
	public void generateErrorPane(Component parent, String messageBody, String frameTitle) {
		generateOptionPane(parent, messageBody, frameTitle, JOptionPane.ERROR_MESSAGE);
	}
	
	/**
	 * Draw an information message pane on screen. Use this when the user needs to see an info message.
	 * @param parent the Component that generates this screen (can be null).
	 * @param messageBody the body that contains the message for the user.
	 * @param frameTitle the title of the Frame that contains the warning.
	 * @author sdentro
	 */
	public void generateInformationMessagePane(Component parent, String messageBody, String frameTitle) {
		generateOptionPane(parent, messageBody, frameTitle, JOptionPane.INFORMATION_MESSAGE);
	}
	
	/**
	 * Draw an Ok/Cancel message pane on screen. Use this when the user needs to see an Ok/Cancel message.
	 * @param parent the Component that generates this screen (can be null).
	 * @param messageBody the body that contains the message for the user.
	 * @param frameTitle the title of the Frame that contains the warning.
	 * @return 0 iff Yes is pressed, 1 iff Cancel is pressed.
	 * @author sdentro
	 */
	public int generateOkCancelMessagePane(Component parent, String messageBody, String frameTitle) {
		return generateConfirmPane(parent, messageBody, frameTitle, JOptionPane.YES_NO_OPTION, JOptionPane.OK_CANCEL_OPTION);
	}

	private void generateOptionPane(Component parent, String messageBody, String frameTitle, int type) {
		JOptionPane.showMessageDialog(
				parent, 
				messageBody, 
				frameTitle, 
				type
		);
	}
	
	private int generateConfirmPane(Component parent, String messageBody, String frameTitle, int selectionType, int type) {
		return JOptionPane.showConfirmDialog(
				parent, 
				messageBody, 
				frameTitle, 
				selectionType,
				type
		);
	}
	
	/**
	 * This method opens the file selector, let's the user select the career.blt file.
	 * @return The selected {@link File}.
	 */
	public File chooseFile() {
		// Create the String, so the file chooser knows where to start
		String filename = File.separator + Config.fileChooserStandardDirectory;
		JFileChooser fc = new JFileChooser(new File(filename));
		
		// Open the dialog
		fc.showOpenDialog(this);
		// Return the selected file
		return fc.getSelectedFile();
	}
	
	
	/**
	 * Method that creates a {@link Dialog} panel to quickly show some information. This is particularly usefull
	 * when a small window is needed to display a few pieces of information. The parent component(s) will
	 * be locked from editing when {@link Dialog} is open.
	 * @param parent The parent container of the dialog. This info is needed to lock the parent.
	 * @param contentPanel The content that you want to be displayed on the {@link Dialog}.
	 * @param dialogTitle The title of the screen.
	 * @param dimension The size of the screen.
	 * @author sdentro
	 */
	public void generateDialogPanel(Component parent, JPanel contentPanel, String dialogTitle, Dimension dimension) {
		parent.setEnabled(false);
		
		Window win = SwingUtilities.getWindowAncestor(parent);
		JDialog dialog = new JDialog(win, dialogTitle, ModalityType.APPLICATION_MODAL);
	    dialog.setPreferredSize(dimension);
	    dialog.add(contentPanel);
	    dialog.pack();
	    dialog.setLocationRelativeTo(null);
	    dialog.setVisible(true);

	    parent.setEnabled(true);
	}
	
}
