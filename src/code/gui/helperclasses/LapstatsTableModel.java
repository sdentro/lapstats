package code.gui.helperclasses;

/**
 * @author sdentro
 * @version $Id$
 */

import javax.swing.table.AbstractTableModel;

public class LapstatsTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 5027801870224358042L;

	private Object[][] rowData;
	private Object[] columnNames;
	
	public LapstatsTableModel(Object[][] rowData, Object[] columnNames) {
		this.rowData = rowData;
		this.columnNames = columnNames;
	}
	
	public String getColumnName(int column) {
		return columnNames[column].toString();
	}

	public int getRowCount() {
		return rowData.length;
	}

	public int getColumnCount() {
		return columnNames.length;
	}

	public Object getValueAt(int row, int col) {
		return rowData[row][col];
	}

	public boolean isCellEditable(int row, int column) {
		return true;
	}

	public void setValueAt(Object value, int row, int col) {
		rowData[row][col] = value;
		fireTableCellUpdated(row, col);
	}

	public Object[][] getRowData() {
		return rowData;
	}
	
	public void setModelData(Object[][] rowData, Object[] columnNames) {
		this.rowData = rowData;
		this.columnNames = columnNames;
	}
}
