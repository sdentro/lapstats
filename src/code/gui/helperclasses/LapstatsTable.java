package code.gui.helperclasses;

/**
 * @author sdentro
 * @version $Id$
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Comparator;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.RowSorter;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class LapstatsTable extends JTable {
	private static final long serialVersionUID = 6782812451147232983L;
	private TableRowSorter<TableModel> sorter;
	private boolean ascending;

	/**
	 * Special sorter for sorting all the different types of data that is 
	 * inserted into the LapstatsTable
	 */
	protected Comparator<String> compareStringContainingNumbers = new Comparator<String>() {
		public int compare(String s1, String s2) {
			int result = 0;
			Integer i1 = new Integer(0);
			Integer i2 = new Integer(0);
			
			if(stringIsLaptime(s1) && stringIsLaptime(s2)) {
				String s1Colon[] = s1.split(":");
				String s2Colon[] = s2.split(":");
				Integer.parseInt(s1Colon[0]);
				String s1Dot[] = s1Colon[1].split("\\."); // \\ escapes the dot
				Integer.parseInt(s2Colon[0]);
				String s2Dot[] = s2Colon[1].split("\\."); // \\ escapes the dot
				
				// Minutes differ
				if(!s1Colon[0].equals(s2Colon[0])) {
					i1 = Integer.parseInt(s1Colon[0]);
					i2 = Integer.parseInt(s2Colon[0]);
				}
				
				// Seconds differ
				else if(!s1Dot[0].equals(s2Dot[0])) {
					i1 = Integer.parseInt(s1Dot[0]);
					i2 = Integer.parseInt(s2Dot[0]);
				}
				
				// Subseconds differ
				else if(!s1Dot[1].equals(s2Dot[1])) {
					i1 = Integer.parseInt(s1Dot[1]);
					i2 = Integer.parseInt(s2Dot[1]);
				}
				result = i1.compareTo(i2);
				return ascending ? result : -result;
			}
			else if(isNumber(s1) && isNumber(s2)) {
				i1 = Integer.parseInt(s1);
				i2 = Integer.parseInt(s2);
				
				result = i1.compareTo(i2);
				return ascending ? result : -result;
			}
			
			result = s1.compareTo(s2);
			return ascending ? result : -result;
		}
		
		private boolean stringIsLaptime(String s) {
			// We know the format should be x*n:xx.xxx
			String sSplitColon[] = s.split(":");
			try {
				Integer.parseInt(sSplitColon[0]);
				String sSplitDot[] = sSplitColon[1].split("\\."); // \\ escapes the dot
				Integer.parseInt(sSplitDot[0]);
				Integer.parseInt(sSplitDot[1]);
			}
			catch(NumberFormatException e) {
				return false;
			}
			catch(IndexOutOfBoundsException e) {
				return false;
			}
			return true;
		}
		
		private boolean isNumber(String s) {
			try {
				Integer.parseInt(s);
			}
			catch(NumberFormatException e) {
				return false;
			}
			return true;
		}
	};
	
	/**
	 * @see JTable
	 */
	public LapstatsTable() {
		super();
		getTableHeader().setReorderingAllowed(false);
		setupDetails();
	}

	/**
	 * @see JTable
	 */
	public LapstatsTable(TableModel dm) {
		super(dm);
		getTableHeader().setReorderingAllowed(false);
		setupDetails();
	}

	/**
	 * @see JTable
	 */
	public LapstatsTable(TableModel dm, TableColumnModel cm) {
		super(dm, cm);
		getTableHeader().setReorderingAllowed(false);
		setupDetails();
	}

	/**
	 * @see JTable
	 */
	public LapstatsTable(int numRows, int numColumns) {
		super(numRows, numColumns);
		setupDetails();
	}

	/**
	 * @see JTable
	 */
	@SuppressWarnings("unchecked") //We really DON'T want to edit this signature.
	public LapstatsTable(Vector rowData, Vector columnNames) {
		super(rowData, columnNames);
		getTableHeader().setResizingAllowed(false);
		setupDetails();
	}

	/**
	 * @see JTable
	 */
	public LapstatsTable(TableModel dm, TableColumnModel cm, ListSelectionModel sm) {
		super(dm, cm, sm);
		setupDetails();
	}
	
	/**
	 * The way this method overrides it's super makes the table selectable and not editable.
	 */
	@Override
	public boolean isCellEditable(int row, int col) {
        return false;
    }
	
	public void setSelectedRow(int row){
		getSelectionModel().setSelectionInterval(row, row);
	}
	
	public LapstatsTableModel getLapstatsModel(){
		if(getModel() instanceof LapstatsTableModel){
			return (LapstatsTableModel)getModel();
		}
		return null;
	}
	
	/*
	 * Method that takes care of setting up the details.
	 * It makes the table:
	 *  1) One row selectable at a time
	 *  2) set reordering to false
	 *  3) creates a tablerowsorter in order to get sorting working.
	 *  4) set ascending to true by default
	 */
    private void setupDetails() {
    	getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		getTableHeader().setReorderingAllowed(false);
    	sorter = new TableRowSorter<TableModel>((TableModel) getModel());
		setRowSorter(sorter);
		ascending = true;
    }
    
    /**
     * Method that sets the comparator of a specific column
     * @param column
     * @param comparator
     */
    public void setComparator(int column, Comparator<?> comparator) {
    	sorter.setComparator(column, comparator);
    }
    
    public Comparator<String> getComparator() {
    	return compareStringContainingNumbers;
    }
    
    @Override
    public RowSorter<? extends TableModel> getRowSorter() {
    	return sorter;
    }
    
    public void setAscending(boolean ascending) {
    	this.ascending = ascending;
    }
    
    public boolean getAscending() {
    	return ascending;
    }
    
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // The following section makes the color of the table rows alternating. 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * Paints empty rows too, after letting the UI delegate do
     * its painting.
     */
    public void paint(Graphics g) {
        super.paint(g);
        paintEmptyRows(g);
    }

    /**
     * Paints the backgrounds of the implied empty rows when the
     * table model is insufficient to fill all the visible area
     * available to us. We don't involve cell renderers, because
     * we have no data.
     */
    protected void paintEmptyRows(Graphics g) {
        final int rowCount = getRowCount();
        final Rectangle clip = g.getClipBounds();
        if (rowCount * rowHeight < clip.height) {
            for (int i = rowCount; i <= clip.height/rowHeight; ++i) {
                g.setColor(colorForRow(i));
                g.fillRect(clip.x, i * rowHeight, clip.width, rowHeight);
            }
        }
    }

    /**
     * Changes the behavior of a table in a JScrollPane to be more like
     * the behavior of JList, which expands to fill the available space.
     * JTable normally restricts its size to just what's needed by its
     * model.
     */
    public boolean getScrollableTracksViewportHeight() {
        if (getParent() instanceof JViewport) {
            JViewport parent = (JViewport) getParent();
            return (parent.getHeight() > getPreferredSize().height);
        }
        return false;
    }

    /**
     * Returns the appropriate background color for the given row.
     */
    protected Color colorForRow(int row) {
        return (row % 2 == 0) ? UIManager.getColor("InternalFrame.borderColor") : getBackground();
    }

    /**
     * Shades alternate rows in different colors.
     */
    public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
        Component c = super.prepareRenderer(renderer, row, column);
        if (isCellSelected(row, column) == false) {
            c.setBackground(colorForRow(row));
            c.setForeground(UIManager.getColor("Table.foreground"));
        } else {
            c.setBackground(UIManager.getColor("Table.selectionBackground"));
            c.setForeground(UIManager.getColor("Table.selectionForeground"));
        }
        return c;
    }
 }
