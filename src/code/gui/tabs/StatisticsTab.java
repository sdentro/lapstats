package code.gui.tabs;

/**
 * @author sdentro
 * @version $Id$
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableRowSorter;

import code.backend.control.Control;
import code.backend.control.GuiControlInterface;

import code.exec.Config;


import code.gui.helperclasses.AbstractTabPanel;
import code.gui.helperclasses.LapstatsTable;
import code.gui.helperclasses.LapstatsTableModel;

public class StatisticsTab extends AbstractTabPanel{
	
	private static final long serialVersionUID = -377361584770778963L;
	
	private final static int TRACK_NAME_COLUMN = 0;
	private final static int TRACK_LAPS_COLUMN = 1;
	private final static int CAR_NAME_COLUMN = 0;
	private final static int CAR_LAPS_COLUMN = 1;
	
	private final static int WIDTH_FIRST_COLUMN = 200;
	private final static int WIDTH_SECOND_COLUMN = 35;
	
	private static final int PREFERRED_SORTED_COLUMN = 1;
	
	private JScrollPane carsTableScrollPane;
	private LapstatsTable carsTable;
	private Vector<Vector<String>> carsTableData;
	private JScrollPane tracksTableScrollPane;
	private LapstatsTable tracksTable;
	private Vector<Vector<String>> tracksTableData;
	
	private JComboBox selectUserJC;
	private String selectedUser;
	private JLabel userNameLabel;
	private JLabel userNameValueLabel;
	private JLabel totalLaptimesLabel;
	private JLabel totalLaptimesValueLabel;
	private String totalLaptimesValue;
	private JLabel totalCarsDrivenLabel;
	private JLabel totalCarsDrivenValueLabel;
	private String totalCarsDrivenValue;
	private JLabel totalLapsLabel;
	private JLabel totalLapsValueLabel;
	private String totalLapsValue;
	
	private ActionListener selectedUserAL = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			selectedUser = (String) selectUserJC.getSelectedItem();
			if(selectedUser != null && !selectedUser.equals("")) {
				updateTableWithCurrentSelection();
			}
		}
	};
	
	public StatisticsTab(GuiControlInterface control, JTabbedPane parent) {
		setControl(control);
		setParent(parent);
		
		this.addComponentListener(reloadComponentCL);
		
		selectedUser = this.control.getCurrentlyActiveUserName();
		
		setupLabelProperties(userNameLabel, "Username", "w 90px, h 20px", Config.TEXTFONT);
		selectUserJC = new JComboBox(control.getAllUserNames());
		setupComboBox(selectUserJC, "w 18%, h 25px, wrap", Config.TEXTFONT, selectedUserAL);
		
		fetchLabelStatisticsAndFormat();
		
		setupLabelProperties(totalLaptimesLabel, "Total tracks driven", "w 90px, h 20px", Config.TEXTFONT);
		setupLabelProperties(totalLaptimesValueLabel, totalLaptimesValue, "w 90px, h 20px, wrap", Config.TEXTFONT);
		
		setupLabelProperties(totalCarsDrivenLabel, "Total cars driven", "w 90px, h 20px", Config.TEXTFONT);
		setupLabelProperties(totalCarsDrivenValueLabel, totalCarsDrivenValue, "w 90px, h 20px, wrap", Config.TEXTFONT);
		
		setupLabelProperties(totalLapsLabel, "Total laps driven", "w 90px, h 20px", Config.TEXTFONT);
		setupLabelProperties(totalLapsValueLabel, totalLapsValue, "w 90px, h 20px, wrap", Config.TEXTFONT);
		
		carsTable = setupTable(carsTable, getCarNamesAndLaps(selectedUser), Control.CARS_LAPS_TABLE_HEADERS, CAR_NAME_COLUMN, CAR_LAPS_COLUMN);
		setupScrollPaneProperties(carsTableScrollPane, carsTable, "w 49%, h 100%, spanx 3");
		
		tracksTable = setupTable(tracksTable, getTrackNamesAndLaps(selectedUser), Control.TRACKS_LAPS_TABLE_HEADERS, TRACK_NAME_COLUMN, TRACK_LAPS_COLUMN);
		setupScrollPaneProperties(tracksTableScrollPane, tracksTable, "w 49%, h 100%, wrap");
	}
	
	/**
	 * Method that fetches the label statisctics and add's formatting and 
	 * spaces to make them look good on screen.
	 */
	private void fetchLabelStatisticsAndFormat() {
		totalLaptimesValue = getTotalLaptimes(selectedUser);
		totalCarsDrivenValue = getTotalCarsDriven(selectedUser);
		totalLapsValue = getTotalLaps(selectedUser);
		// No formatting, it's not working, somehow.
		//TODO fix formatting of labels on statisticstab
	}

	@Override
	protected void initWidgets() {
		selectUserJC = new JComboBox();
		
		userNameLabel = new JLabel();
		userNameValueLabel = new JLabel();
		totalLaptimesLabel = new JLabel();
		totalLaptimesValueLabel = new JLabel();
		totalLaptimesValue = "0";
		totalCarsDrivenLabel = new JLabel();
		totalCarsDrivenValueLabel = new JLabel();
		totalCarsDrivenValue = "0";
		totalLapsLabel = new JLabel();
		totalLapsValueLabel = new JLabel();
		totalLapsValue = "0";
		
		carsTableScrollPane = new JScrollPane();
		tracksTableScrollPane = new JScrollPane();
		
		carsTable = new LapstatsTable();
		tracksTable = new LapstatsTable();
		
		carsTableData = new Vector<Vector<String>>();
		tracksTableData = new Vector<Vector<String>>();
	}

	@Override
	public void refreshComponentData() {
		updateCombobox(selectUserJC, control.getAllUserNames());
		userNameValueLabel.setText(control.getCurrentlyActiveUserName());
		
		fetchLabelStatisticsAndFormat();
		
		totalLaptimesValueLabel.setText(totalLaptimesValue);
		totalCarsDrivenValueLabel.setText(totalCarsDrivenValue);
		totalLapsValueLabel.setText(totalLapsValue);
		
		updateTableWithCurrentSelection();
	}

	@Override
	protected void updateTableWithCurrentSelection() {
		carsTableData = getCarNamesAndLaps(selectedUser);
		tracksTableData = getTrackNamesAndLaps(selectedUser);
		
		updateTable(carsTableScrollPane, carsTable, carsTableData, Control.CARS_LAPS_TABLE_HEADERS, CAR_NAME_COLUMN, CAR_LAPS_COLUMN);
		updateTable(tracksTableScrollPane, tracksTable, tracksTableData, Control.TRACKS_LAPS_TABLE_HEADERS, TRACK_NAME_COLUMN, TRACK_LAPS_COLUMN);
	}
	
	private void updateTable(JScrollPane tableScrollpane, LapstatsTable table, Vector<Vector<String>> tableData, Vector<String> tableHeaders, int namesColumn, int lapsColumn) {
		table = setupTable(table, tableData, tableHeaders, namesColumn, lapsColumn);
		updateScrollPaneProperties(tableScrollpane, table);
		
		validate();
	}
	
	/**
	 * This method takes care of the setting up that needs to be done when inserting a table.
	 * @param data The data that needs to go in. The new table is stored in the table field
	 * of AbstractTabPanel for future reference.
	 * @param colNames The column names.
	 */
	private LapstatsTable setupTable(LapstatsTable table, Vector<Vector<String>> data, Vector<String> colNames, int namesColumn, int lapsColumn) {	
		table = new LapstatsTable(data, colNames);
		table.setAscending(false);
		setupTableProperties(table, Config.TEXTFONT, tableClickAL);
		table = setupSorting(table);
		
		if(table.getColumnModel().getColumnCount() > 0) { // ie. there actually are columns
			setupTableColumns(table, namesColumn, lapsColumn);
		}
		return table;
	}
	
	private void setupTableColumns(LapstatsTable table, int nameColumn, int lapsColumn) {
		table.getColumnModel().getColumn(nameColumn).setPreferredWidth(WIDTH_FIRST_COLUMN);
		table.getColumnModel().getColumn(lapsColumn).setPreferredWidth(WIDTH_SECOND_COLUMN);
		
		DefaultTableCellRenderer r = new DefaultTableCellRenderer();
		r.setHorizontalAlignment(SwingConstants.CENTER);
		
		table.getColumnModel().getColumn(lapsColumn).setCellRenderer(r);
	}

	private String getTotalLaptimes(String userName) {
		return control.getTotalLaptimes(userName);
	}
	
	private String getTotalCarsDriven(String userName) {
		return control.getTotalCarsDriven(userName);
	}
	
	private String getTotalLaps(String userName) {
		return control.getTotalLaps(userName);
	}
	
	private Vector<Vector<String>> getCarNamesAndLaps(String userName) {
		return control.getCarNamesAndLaps(userName);
	}
	
	private Vector<Vector<String>> getTrackNamesAndLaps(String userName) {
		return control.getTrackNamesAndLaps(userName);
	}
	
	@Override
	protected int getPreferredSortedColumn() {
		return PREFERRED_SORTED_COLUMN;
	}

	
	@Override
	protected void setupTableColumns(LapstatsTable table) {
		// Not needed
	}
	
	@Override
	protected void tableDoubleClickedAction() {
		// Not needed
	}

	@Override
	protected void updateDetails() {
		// Not needed
	}

}
