package code.gui.tabs;

/**
 * @author sdentro
 * @version $Id$
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import code.backend.control.Control;
import code.backend.control.GuiControlInterface;

import code.exec.Config;


import code.gui.helperclasses.AbstractTabPanel;
import code.gui.helperclasses.LapstatsTable;

public class TrackSetsTab extends AbstractTabPanel {

	private static final long serialVersionUID = 564186438605536095L;
	
	// Set the column in the tables that needs to be sorted by default
	private static final int PREFERRED_SORTED_COLUMN = 0;
	
	private final static int TRACK_COLUMN = 0;
	private final static int LAPTIME_COLUMN = 1;
	private final static int DIFFERENCE_COLUMN = 2;
	
	private final static int WIDTH_FIRST_COLUMN = 150;
	private final static int WIDTH_SECOND_COLUMN = 50;
	private final static int WIDTH_THIRD_COLUMN = 30;
	
	private JList trackSetsList;
	private JScrollPane trackSetsListScrollPane;
	private JList carsList;
	private JScrollPane carsListScrollPane;
	private JTextField searchTrackSetsTF;
	private JButton searchTrackSetsBT;
	private JTextField searchCarsTF;
	private JButton searchCarsBT;
	private int selectedCar;
	private int selectedTrackSet;
	private JComboBox selectUserJC;
	private String selectedUser;
	
	private Vector<String> trackSetsListData;
	private Vector<String> carsListData;
	
	private ActionListener searchTrackSetsAL = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) { searchTrackSets(); }
	};
	
	private ActionListener searchCarsAL = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) { searchCars(); }
	};
	
	private KeyListener searchTrackSetsKL = new KeyListener() {
		@Override
		public void keyPressed(KeyEvent e) {
			 int key = e.getKeyCode();
		     if (key == KeyEvent.VK_ENTER) {  searchTrackSets(); }
		}
		@Override
		public void keyReleased(KeyEvent e) {}
		@Override
		public void keyTyped(KeyEvent e) {}
	};
	
	private KeyListener searchCarsKL = new KeyListener() {
		@Override
		public void keyPressed(KeyEvent e) {
			 int key = e.getKeyCode();
		     if (key == KeyEvent.VK_ENTER) {  searchCars(); }
		}
		@Override
		public void keyReleased(KeyEvent e) {}
		@Override
		public void keyTyped(KeyEvent e) {}
	};
	
	private ActionListener selectedUserAL = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			selectedUser = (String) selectUserJC.getSelectedItem();
			if(selectedUser != null && !selectedUser.equals("")) {
				updateTableWithCurrentSelection();
			}
		}
	};

	public TrackSetsTab(GuiControlInterface control, JTabbedPane parent) {
		setControl(control);
		setParent(parent);
		
		this.addComponentListener(reloadComponentCL);
		
		tableHeader = Control.TRACKSETS_TABLE_HEADERS;
		selectedUser = this.control.getCurrentlyActiveUserName();

		setupTextFieldProperties(searchTrackSetsTF, "w 15%, h 26px", Config.TEXTFONT, searchTrackSetsKL);
		setupButtonProperties(searchTrackSetsBT, "Search", "w 8%, h 25px", Config.TEXTFONT, searchTrackSetsAL, searchTrackSetsKL);
		
		setupTextFieldProperties(searchCarsTF, "w 18%, h 26px", Config.TEXTFONT, searchCarsKL);
		setupButtonProperties(searchCarsBT, "Search", "w 8%, h 25px", Config.TEXTFONT, searchCarsAL, searchCarsKL);
		selectUserJC = new JComboBox(control.getAllUserNames());
		setupComboBox(selectUserJC, "w 18%, h 25px, wrap", Config.TEXTFONT, selectedUserAL);
		
		trackSetsListData = control.sort(control.getAllTrackSetNames());
		setupTrackSetsList(trackSetsListData);
		carsListData = control.sort(control.getAllCarNames());
		setupCarsList(carsListData);
		
		setupTable(new Vector<Vector<String>>(), tableHeader);
		setupScrollPaneProperties(tableScrollPane, table, "w 47%, h 100%, spany 2, wrap");
	}
	
	/**
	 * Update the list that displays the TrackSets.
	 * @param data The new data for the list.
	 */
	private void setupTrackSetsList(Vector<String> data) {
		trackSetsList = setupList(trackSetsList, data, trackSetsListScrollPane, "w 25%, h 100%, spanx 2, spany 8", selectedTrackSet, listKL, listLSL);
	}
	
	/**
	 * Update the list that displays the cars. 
	 * @param data The new data for the list.
	 */
	private void setupCarsList(Vector<String> data) {
		carsList = setupList(carsList, data, carsListScrollPane, "w 28%, h 100%, spanx 2, spany 8", selectedCar, listKL, listLSL);
	}

	
	protected void setupTableColumns(LapstatsTable table) {
		table.getColumnModel().getColumn(TRACK_COLUMN).setPreferredWidth(WIDTH_FIRST_COLUMN);
		table.getColumnModel().getColumn(LAPTIME_COLUMN).setPreferredWidth(WIDTH_SECOND_COLUMN);
		table.getColumnModel().getColumn(DIFFERENCE_COLUMN).setPreferredWidth(WIDTH_THIRD_COLUMN);
		
		DefaultTableCellRenderer r = new DefaultTableCellRenderer();
		r.setHorizontalAlignment(SwingConstants.CENTER);
		
		table.getColumnModel().getColumn(LAPTIME_COLUMN).setCellRenderer(r);
		table.getColumnModel().getColumn(DIFFERENCE_COLUMN).setCellRenderer(r);
	}
	
	private void searchCars() {
		String searchTerm = searchCarsTF.getText().toLowerCase();
		
		carsListData = searchFor(searchTerm, control.getAllCarNames());
		updateCarsList(carsListData);
		
		updateTableWithCurrentSelection();
		
		searchCarsTF.setText("");
	}
	
	private void searchTrackSets() {
		String searchTerm = searchTrackSetsTF.getText().toLowerCase();
		
		trackSetsListData = searchFor(searchTerm, control.getAllTrackSetNames());
		updateTrackSetsList(trackSetsListData);
		
		updateTableWithCurrentSelection();
		
		searchTrackSetsTF.setText("");
	}
	
	@Override
	protected void updateTableWithCurrentSelection() {
		if(trackSetsList.getModel().getSize() > 0) {
			this.selectedTrackSet = trackSetsList.getSelectedIndex();
		}
		else {
			this.selectedTrackSet = 0;
		}
		
		if(carsList.getModel().getSize() == 0) {
			this.selectedCar = 0;
		}
		
		String selectedTrackSet = (String) trackSetsList.getSelectedValue();
		String selectedCar = (String) carsList.getSelectedValue();
		updateTable(control.getTrackSetsTableData(selectedUser, selectedTrackSet, selectedCar));
	}
	
	private void updateCarsList(Vector<String> data) {
		carsList = updateList(carsList, data, carsListScrollPane, selectedCar, listKL, listLSL);
	}
	
	private void updateTrackSetsList(Vector<String> data) {
		trackSetsList = updateList(trackSetsList, data, trackSetsListScrollPane, selectedTrackSet, listKL, listLSL);
	}
	
	/**
	 * @see AbstractTabPanel
	 */
	protected int getPreferredSortedColumn() {
		return PREFERRED_SORTED_COLUMN;
	}
	
	@Override
	protected void initWidgets() {
		carsListData = new Vector<String>();
		trackSetsListData = new Vector<String>();
		
		trackSetsListScrollPane = new JScrollPane();
		trackSetsList = new JList();
		carsListScrollPane = new JScrollPane();
		carsList = new JList();
		tableScrollPane = new JScrollPane();
		
		searchTrackSetsBT = new JButton();
		searchTrackSetsTF = new JTextField();
		searchCarsBT = new JButton();
		searchCarsTF = new JTextField();
		
		selectUserJC = new JComboBox();
		
		selectedCar = 0;
		selectedTrackSet = 0;
	}

	@Override
	public void refreshComponentData() {
		updateTrackSetsList(control.sort(control.getAllTrackSetNames()));
		updateCombobox(selectUserJC, control.getAllUserNames());
		updateTableWithCurrentSelection();
	}

	@Override
	protected void tableDoubleClickedAction() {
		// Not implemented
	}

	@Override
	protected void updateDetails() {
		// Not needed in this Tab, but needs to be overridden
	}
}
