package code.gui.tabs;

/**
 * @author sdentro
 * @version $Id$
 */

import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import code.backend.control.Control;
import code.backend.control.GuiControlInterface;
import code.backend.data.Car;

import code.exec.Config;

import code.gui.helperclasses.AbstractStandardTabPanel;
import code.gui.helperclasses.LapstatsTable;

@SuppressWarnings("serial")
public class CarTab extends AbstractStandardTabPanel {

	private final static int USER_COLUMN = 0;
	private final static int TRACK_COLUMN = 1;
	private final static int LAPTIME_COLUMN = 2;
	private final static int LAPS_COLUMN = 3;
	private final static int DIFFERENCE_COLUMN = 4;
	
	private final static int WIDTH_FIRST_COLUMN = 50;
	private final static int WIDTH_SECOND_COLUMN = 150;
	private final static int WIDTH_THIRD_COLUMN = 50;
	private final static int WIDTH_FOURTH_COLUMN = 10;
	private final static int WIDTH_FIFTH_COLUMN = 30;
	
	private static final int PREFERRED_SORTED_COLUMN = 1;
	
	private JLabel carNameJL;
	private JLabel carLapsJL;
	private JLabel carClassJL;
	private JLabel authorJL;
	private JLabel carNameValueJL;
	private JLabel carLapsValueJL;
	private JLabel carClassValueJL;
	private JLabel authorValueJL;
	private JLabel propertiesJL;
	
	public CarTab(GuiControlInterface control, JTabbedPane parent) {
		super.setParent(parent);
		super.setControl(control);
		
		this.addComponentListener(reloadComponentCL);

		listData = control.sort(control.getAllCarNames());
		tableHeader = Control.CARS_TABLE_HEADERS;
		paint();
	}

	@Override
	public void updateTable(Vector<Vector<String>> tableData) {
		tableHeader = Control.CARS_TABLE_HEADERS;
		setupTable(tableData, tableHeader);
		
		updateScrollPaneProperties(tableScrollPane, table);
		
		updateDetails();

		validate();
	}

	@Override
	protected Vector<Vector<String>> getTableData() {
		return control.getCarsTableData((String) list.getSelectedValue());
	}

	@Override
	protected void setupDetails() {
		propertiesJL = new JLabel();
		carNameJL = new JLabel();
		carLapsJL = new JLabel();
		carClassJL = new JLabel();
		carNameValueJL = new JLabel();
		carLapsValueJL = new JLabel();
		carClassValueJL = new JLabel();	
		authorJL = new JLabel();
		authorValueJL = new JLabel();

		String selectedCar = (String) list.getSelectedValue();
		
		Car car = control.getCar(selectedCar);
		int numberOfLaps = control.getNumberOfLapsWithCar(selectedCar);
		
		setupLabelProperties(propertiesJL, "Properties", "w 90px, h 1%, wrap", Config.HEADERFONT);
		setupLabelProperties(this, carNameJL, "Car:", "w 90px, h 1%", Config.TEXTFONT);
		setupLabelProperties(this, carNameValueJL, car.getName(), "w 100px, h 1%, spanx 2, wrap", Config.TEXTFONT);
		setupLabelProperties(this, authorJL, "Released by:", "w 100px, h 1%", Config.TEXTFONT);
		setupLabelProperties(this, authorValueJL, car.getAuthor(), "w 100px, h 1%, wrap", Config.TEXTFONT);
		setupLabelProperties(this, carLapsJL, "Laps:", "w 90px, h 1%", Config.TEXTFONT);
		setupLabelProperties(this, carLapsValueJL, ""+numberOfLaps, "w 100px, h 1%, wrap", Config.TEXTFONT);
		setupLabelProperties(this, carClassJL, "Class:", "w 90px, h 1%", Config.TEXTFONT);
		setupLabelProperties(this, carClassValueJL, car.getCarClass(), "w 100px, h 1%, wrap", Config.TEXTFONT);
	}

	/**
	 * @see AbstractStandardTabPanel
	 */
	@Override
	public void refreshComponentData() {
		refreshComponentData(control.getAllCarNames());
	}
	
	@Override
	protected void updateDetails() {
		String selectedCar = (String) list.getSelectedValue();
		Car car = control.getCar(selectedCar);
		if(car != null) {
			int numberOfLaps = control.getNumberOfLapsWithCar(selectedCar);
			
			carNameValueJL.setText(car.getName());
			carLapsValueJL.setText(""+numberOfLaps);
			carClassValueJL.setText(car.getCarClass());
			authorValueJL.setText(car.getAuthor());
			validate();
		}
	}
	
	/**
	 * @see AbstractStandardTabPanel
	 */
	@Override
	protected void setupTableColumns(LapstatsTable table) {
		table.getColumnModel().getColumn(USER_COLUMN).setPreferredWidth(WIDTH_FIRST_COLUMN);
		table.getColumnModel().getColumn(TRACK_COLUMN).setPreferredWidth(WIDTH_SECOND_COLUMN);
		table.getColumnModel().getColumn(LAPTIME_COLUMN).setPreferredWidth(WIDTH_THIRD_COLUMN);
		table.getColumnModel().getColumn(LAPS_COLUMN).setPreferredWidth(WIDTH_FOURTH_COLUMN);
		table.getColumnModel().getColumn(DIFFERENCE_COLUMN).setPreferredWidth(WIDTH_FIFTH_COLUMN);
		
		DefaultTableCellRenderer r = new DefaultTableCellRenderer();
		r.setHorizontalAlignment(SwingConstants.CENTER);
		
		table.getColumnModel().getColumn(LAPTIME_COLUMN).setCellRenderer(r);
		table.getColumnModel().getColumn(LAPS_COLUMN).setCellRenderer(r);
		table.getColumnModel().getColumn(DIFFERENCE_COLUMN).setCellRenderer(r);
	}

	/**
	 * @see AbstractStandardTabPanel
	 */
	@Override
	protected Vector<String> searchFor(String searchTerm) {
		return searchFor(searchTerm, control.getAllCarNames());
	}

	/**
	 * @see AbstractStandardTabPanel
	 */
	@Override
	protected Vector<String> getDataForFirstCombobox() {
		return setupDataForCombobox(control.sort(control.getAllUserNames()));
	}

	/**
	 * @see AbstractStandardTabPanel
	 */
	@Override
	protected Vector<String> getDataForSecondCombobox() {
		return setupDataForCombobox(control.sort(control.getAllTrackNames()));
	}
	
	/**
	 * @see AbstractStandardTabPanel
	 */
	@Override
	protected Vector<String> getDataForThirdCombobox() {
		return setupDataForCombobox(control.sort(control.getAllTrackSetNames()));
	}
	
	/**
	 * Convenience method that sets up the data for the {@link JCombobox}es.
	 * @param data A {@link Vector} containing {@link String}s or empty.
	 * @return A {@link Vector} containing {@link String}s preceded by the String "Filter..." or empty.
	 */
	private Vector<String> setupDataForCombobox(Vector<String> data) {
		Vector<String> returnVector = new Vector<String>();
		
		if(data.size() > 0) {
			returnVector.add("Filter...");
		}
		returnVector.addAll(data);
		
		return returnVector;
	}
	
	/**
	 * @see AbstractStandardTabPanel
	 */
	@Override
	protected void filterTableData() {
		Vector<Vector<String>> data = getTableData();
		
		String userName = "";
		String trackName = "";
		String trackSetName = "";
		
		if(firstCombobox.getSelectedItem() != null && !firstCombobox.getSelectedItem().equals("Filter...")) {
			userName = (String) firstCombobox.getSelectedItem();
			Vector<Vector<String>> temp = new Vector<Vector<String>>();
			for(Vector<String> row : data) {
				if(row.get(0).equals(userName)) {
					temp.add(row);
				}
			}
			data = temp;
		}
		if(secondCombobox.getSelectedItem() != null && !secondCombobox.getSelectedItem().equals("Filter...")) {
			trackName = (String) secondCombobox.getSelectedItem();
			Vector<Vector<String>> temp = new Vector<Vector<String>>();
			for(Vector<String> row : data) {
				if(row.get(1).equals(trackName)) {
					temp.add(row);
				}
			}
			data = temp;
		}
		if(thirdCombobox.getSelectedItem() != null && !thirdCombobox.getSelectedItem().equals("Filter...")) {
			trackSetName = (String) thirdCombobox.getSelectedItem();
			Vector<String> trackNames = control.getTrackSet(trackSetName).getTrackNames();
			Vector<Vector<String>> temp = new Vector<Vector<String>>();
			for(Vector<String> row : data) {
				if(trackNames.contains(row.get(1))) {
					temp.add(row);
				}
			}
			data = temp;
		}
		updateTable(data);
	}

	/**
	 * @see AbstractStandardTabPanel
	 */
	@Override
	protected void tableDoubleClickedAction() {
		System.out.println("Clicked twice!!");
	}
	
	/**
	 * @see AbstractTabPanel
	 */
	@Override
	protected int getPreferredSortedColumn() {
		return PREFERRED_SORTED_COLUMN;
	}
}
