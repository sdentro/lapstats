package code.gui.tabs;

/**
 * @author sdentro
 * @version $Id$
 */

import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import code.backend.control.Control;
import code.backend.control.GuiControlInterface;
import code.backend.data.Track;

import code.exec.Config;


import code.gui.helperclasses.AbstractStandardTabPanel;
import code.gui.helperclasses.LapstatsTable;

public class TrackTab extends AbstractStandardTabPanel {

	private final static int USER_COLUMN = 0;
	private final static int CAR_COLUMN = 1;
	private final static int LAPTIME_COLUMN = 2;
	private final static int LAPS_COLUMN = 3;
	private final static int DIFFERENCE_COLUMN = 4;
	
	private final static int WIDTH_FIRST_COLUMN = 50;
	private final static int WIDTH_SECOND_COLUMN = 150;
	private final static int WIDTH_THIRD_COLUMN = 50;
	private final static int WIDTH_FOURTH_COLUMN = 10;
	private final static int WIDTH_FIFTH_COLUMN = 30;
	
	private static final int PREFERRED_SORTED_COLUMN = 1;
	
	private static final long serialVersionUID = 7734372196361891902L;
	private JLabel properties;
	private JLabel trackName;
	private JLabel trackNameValue;
	private JLabel noOfLaps;
	private JLabel noOfLapsValue;
	private JLabel authorJL;
	private JLabel authorValueJL;
	private Vector<JLabel> refTimes;

	public TrackTab(GuiControlInterface control, JTabbedPane parent) {
		super.setParent(parent);
		super.setControl(control);
		listData = control.sort(control.getAllTrackNames());
		tableHeader = Control.TRACKS_TABLE_HEADERS;
		paint();
	}
	
	@Override
	protected void filterTableData() {
		Vector<Vector<String>> data = getTableData();
		
		String userName = "";
		String carName = "";
		String className = "";
		
		if(firstCombobox.getSelectedItem() != null && !firstCombobox.getSelectedItem().equals("Filter...")) {
			userName = (String) firstCombobox.getSelectedItem();
			Vector<Vector<String>> temp = new Vector<Vector<String>>();
			for(Vector<String> row : data) {
				if(row.get(0).equals(userName)) {
					temp.add(row);
				}
			}
			data = temp;
		}
		if(secondCombobox.getSelectedItem() != null && !secondCombobox.getSelectedItem().equals("Filter...")) {
			carName = (String) secondCombobox.getSelectedItem();
			Vector<Vector<String>> temp = new Vector<Vector<String>>();
			for(Vector<String> row : data) {
				if(row.get(1).equals(carName)) {
					temp.add(row);
				}
			}
			data = temp;
		}
		if(thirdCombobox.getSelectedItem() != null && !thirdCombobox.getSelectedItem().equals("Filter...")) {
			className = (String) thirdCombobox.getSelectedItem();
			Vector<Vector<String>> temp = new Vector<Vector<String>>();
			for(Vector<String> row : data) {
				if(control.getCar(row.get(1)).getCarClass().equals(className)) {
					temp.add(row);
				}
			}
			data = temp;
		}
		updateTable(data);
	}

	@Override
	protected Vector<String> getDataForFirstCombobox() {
		return setupDataForCombobox(control.sort(control.getAllUserNames()));
	}

	@Override
	protected Vector<String> getDataForSecondCombobox() {
		return setupDataForCombobox(control.sort(control.getAllCarNames()));
	}

	@Override
	protected Vector<String> getDataForThirdCombobox() {
		Vector<String> dataContainer = new Vector<String>();
		List<String> data = Arrays.asList(control.getCarClasses());
		dataContainer.addAll(data);
		return setupDataForCombobox(dataContainer);
	}
	
	private Vector<String> setupDataForCombobox(Vector<String> data) {
		Vector<String> returnVector = new Vector<String>();
		
		if(data.size() > 0) {
			returnVector.add("Filter...");
		}
		returnVector.addAll(data);
		
		return returnVector;
	}

	@Override
	public void refreshComponentData() {
		refreshComponentData(control.getAllTrackNames());
	}

	@Override
	protected Vector<String> searchFor(String searchTerm) {
		return searchFor(searchTerm, control.getAllTrackNames());
	}

	@Override
	protected void setupDetails() {
		properties = new JLabel();
		trackName = new JLabel();
		trackNameValue = new JLabel();
		noOfLaps = new JLabel();
		noOfLapsValue = new JLabel();
		authorJL = new JLabel();
		authorValueJL = new JLabel();
		refTimes = new Vector<JLabel>();
		
		String selectedTrack = (String) list.getSelectedValue();
		Track track = control.getTrack(selectedTrack);
		int numberOfLaps = control.getNumberOfLapsAtTrack(selectedTrack);
		
		/*
		 * This is a mess: For the formatting to go right the Objects need to be pushed on in a certain
		 * way. This means that first the properties and trackname need to go on. Then the GT1 time, wrap,
		 * then the numberoflaps followed by GT2 and wrap. Etc Etc. 4 columns would've been a better solution,
		 * but I don't know how to do that..
		 */
		int numberOfPasses = 0;
		Vector<String> refTimesKeys = control.sort(new Vector<String>(track.getReferenceTimes().keySet()));
		for(String refTime : refTimesKeys) {
			if(numberOfPasses == 0) {
				setupLabelProperties(properties, "Properties", "w 90px, h 1%, wrap", Config.HEADERFONT);
				setupLabelProperties(trackName, "Track: ", "w 90px, h 1%", Config.TEXTFONT);
				setupLabelProperties(trackNameValue, track.getName(), "w 90px, h 1%", Config.TEXTFONT);
			}
			else if(numberOfPasses == 1) {
				setupLabelProperties(authorJL, "Released by:", "w 90px, h 1%", Config.TEXTFONT);
				setupLabelProperties(authorValueJL, ""+numberOfLaps, "w 90px, h 1%", Config.TEXTFONT);
			}
			else if(numberOfPasses == 2) {
				setupLabelProperties(noOfLaps, "Laps:", "w 90px, h 1%", Config.TEXTFONT);
				setupLabelProperties(noOfLapsValue, ""+track.getAuthor(), "w 90px, h 1%", Config.TEXTFONT);
			}
			else {
				setupLabelProperties(new JLabel(), "", "w 90px, h 1%", Config.TEXTFONT);
				setupLabelProperties(new JLabel(), "", "w 90px, h 1%", Config.TEXTFONT);
			}
			
			JLabel refTimeName = new JLabel();
			JLabel refTimeValue = new JLabel();
			
			setupLabelProperties(refTimeName, refTime, "w 70px, h 1%", Config.TEXTFONT);
			setupLabelProperties(refTimeValue, track.getReferenceTime(refTime), "w 70px, h 1%, wrap", Config.TEXTFONT);
			
			refTimes.add(refTimeName);
			refTimes.add(refTimeValue);
			numberOfPasses++;
			
		}
	}

	@Override
	protected void setupTableColumns(LapstatsTable table) {
		table.getColumnModel().getColumn(USER_COLUMN).setPreferredWidth(WIDTH_FIRST_COLUMN);
		table.getColumnModel().getColumn(CAR_COLUMN).setPreferredWidth(WIDTH_SECOND_COLUMN);
		table.getColumnModel().getColumn(LAPTIME_COLUMN).setPreferredWidth(WIDTH_THIRD_COLUMN);
		table.getColumnModel().getColumn(LAPS_COLUMN).setPreferredWidth(WIDTH_FOURTH_COLUMN);
		table.getColumnModel().getColumn(DIFFERENCE_COLUMN).setPreferredWidth(WIDTH_FIFTH_COLUMN);
		
		DefaultTableCellRenderer r = new DefaultTableCellRenderer();
		r.setHorizontalAlignment(SwingConstants.CENTER);
		
		table.getColumnModel().getColumn(LAPTIME_COLUMN).setCellRenderer(r);
		table.getColumnModel().getColumn(LAPS_COLUMN).setCellRenderer(r);
		table.getColumnModel().getColumn(DIFFERENCE_COLUMN).setCellRenderer(r);
	}

	@Override
	protected void tableDoubleClickedAction() {
//		System.out.println("[TrackTab] Double clicked!!");
//		int carTabIndex = parent.indexOfTab("Cars");
//		System.out.println(carTabIndex);
//		
//		CarTab carTab = (CarTab) parent.getTabComponentAt(1);
//		parent.getC
//		if(carTab == null) {
//			System.out.println("No cartab!!");
//		}
//		
//		String selectedCar = (String) table.getValueAt(table.getSelectedRow(), 1);
////		selectedCar = control.getCar(selectedCar).getId();
//		System.out.println(selectedCar);
//		System.out.println(control.containsCar(selectedCar));
////		carTab.updateTable(control.getLaptimesByCar(selectedCar));
//		carTab.setCarInListSelected(selectedCar);
//		
//		parent.setSelectedIndex(carTabIndex);
		

	}

	@Override
	protected void updateDetails() {
		String selectedTrack = (String) list.getSelectedValue();
		Track track = control.getTrack(selectedTrack);
		if(track != null) {
			int numberOfLaps = control.getNumberOfLapsAtTrack(selectedTrack);
			
			trackNameValue.setText(track.getName());
			noOfLapsValue.setText(""+numberOfLaps);
			authorValueJL.setText(track.getAuthor());
			
			for(int i=0; i<refTimes.size(); i=i+2) {
				JLabel carClass = refTimes.get(i);
				JLabel refTime = refTimes.get(i+1);
				refTime.setText(track.getReferenceTime(carClass.getText()));
			}
			validate();
		}
	}

	@Override
	public void updateTable(Vector<Vector<String>> tableData) {
		tableHeader = Control.TRACKS_TABLE_HEADERS;
		setupTable(tableData, tableHeader);
		
		updateScrollPaneProperties(tableScrollPane, table);
		
		updateDetails();

		validate();
	}
	
	@Override
	protected Vector<Vector<String>> getTableData() {
		return control.getTracksTableData((String) list.getSelectedValue());
	}
	
	/**
	 * @see AbstractTabPanel
	 */
	@Override
	protected int getPreferredSortedColumn() {
		return PREFERRED_SORTED_COLUMN;
	}
}
