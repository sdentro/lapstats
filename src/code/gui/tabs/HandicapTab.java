package code.gui.tabs;

/**
 * @author sdentro
 * @version $Id$
 */

import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import code.backend.control.Control;
import code.backend.control.GuiControlInterface;

import code.exec.Config;


import code.gui.helperclasses.AbstractStandardTabPanel;
import code.gui.helperclasses.AbstractTabPanel;
import code.gui.helperclasses.LapstatsTable;

public class HandicapTab extends AbstractStandardTabPanel {
	
	// Set the column in the tables that needs to be sorted by default
	private static final int PREFERRED_SORTED_COLUMN = 2;
	
	private static final long serialVersionUID = -6734923772920359060L;
	private final static int USER_COLUMN = 0;
	private final static int CAR_COLUMN = 1;
	private final static int CLASS_COLUMN = 2;
	private final static int DIFFERENCE_COLUMN = 3;
	
	private final static int WIDTH_FIRST_COLUMN = 50;
	private final static int WIDTH_SECOND_COLUMN = 150;
	private final static int WIDTH_THIRD_COLUMN = 50;
	private final static int WIDTH_FOURTH_COLUMN = 10;
	
	private JLabel properties;
	private JLabel noOfTracks;
	private JLabel noOfTracksValue;
	private JLabel noOfCompleted;
	private JLabel noOfCompletedValue;
	private JLabel setName;
	private JLabel setNameValue;
	
	public HandicapTab(GuiControlInterface control, JTabbedPane tabPane) {
		super.setParent(tabPane);
		super.setControl(control);
		listData = control.sort(control.getAllTrackSetNames());
		tableHeader = Control.HANDICAPS_TABLE_HEADERS;
		paint();
	}


	@Override
	protected void filterTableData() {
		Vector<Vector<String>> data = getTableData();
		
		String userName = "";
		String carName = "";
		String className = "";
		
		if(firstCombobox.getSelectedItem() != null && !firstCombobox.getSelectedItem().equals("Filter...")) {
			userName = (String) firstCombobox.getSelectedItem();
			Vector<Vector<String>> temp = new Vector<Vector<String>>();
			for(Vector<String> row : data) {
				if(row.get(0).equals(userName)) {
					temp.add(row);
				}
			}
			data = temp;
		}
		if(secondCombobox.getSelectedItem() != null && !secondCombobox.getSelectedItem().equals("Filter...")) {
			carName = (String) secondCombobox.getSelectedItem();
			Vector<Vector<String>> temp = new Vector<Vector<String>>();
			for(Vector<String> row : data) {
				if(row.get(1).equals(carName)) {
					temp.add(row);
				}
			}
			data = temp;
		}
		if(thirdCombobox.getSelectedItem() != null && !thirdCombobox.getSelectedItem().equals("Filter...")) {
			className = (String) thirdCombobox.getSelectedItem();
			Vector<Vector<String>> temp = new Vector<Vector<String>>();
			for(Vector<String> row : data) {
				if(((String) row.get(2)).equals(className)) {
					temp.add(row);
				}
			}
			data = temp;
		}
		updateTable(data);
	}

	/*
	 *Next code all thesame as in TrackTab...
	 */
	@Override
	protected Vector<String> getDataForFirstCombobox() {
		return setupDataForCombobox(control.sort(control.getAllUserNames()));
	}

	@Override
	protected Vector<String> getDataForSecondCombobox() {
		return setupDataForCombobox(control.sort(control.getAllCarNames()));
	}

	@Override
	protected Vector<String> getDataForThirdCombobox() {
		Vector<String> dataContainer = new Vector<String>();
		List<String> data = Arrays.asList(control.getCarClasses());
		dataContainer.addAll(data);
		return setupDataForCombobox(dataContainer);
	}
	
	private Vector<String> setupDataForCombobox(Vector<String> data) {
		Vector<String> returnVector = new Vector<String>();
		
		if(data.size() > 0) {
			returnVector.add("Filter...");
		}
		returnVector.addAll(data);
		
		return returnVector;
	}
	/*
	 * All above code same as in TrackTab
	 */

	@Override
	public void refreshComponentData() {
		refreshComponentData(control.getAllTrackSetNames());
	}

	@Override
	protected Vector<String> searchFor(String searchTerm) {
		return searchFor(searchTerm, control.getAllTrackSetNames());
	}

	@Override
	protected void setupDetails() {
		properties = new JLabel();
		setName = new JLabel();
		setNameValue = new JLabel();
		noOfCompleted = new JLabel();
		noOfCompletedValue = new JLabel();
		noOfTracks = new JLabel();
		noOfTracksValue = new JLabel();
		
		setupLabelProperties(properties, "Properties", "w 90px, h 1%, wrap", Config.HEADERFONT);
		setupLabelProperties(setName, "Trackset name:", "w 90px, h 1%", Config.TEXTFONT);
		setupLabelProperties(setNameValue, (String) list.getSelectedValue() , "w 90px, h 1%, wrap", Config.TEXTFONT);
		setupLabelProperties(noOfTracks, "Tracks in set:", "w 90px, h 1%", Config.TEXTFONT);
		setupLabelProperties(noOfTracksValue, ""+control.getTrackSet((String) list.getSelectedValue()).getTrackNames().size() , "w 90px, h 1%, wrap", Config.TEXTFONT);
		setupLabelProperties(noOfCompleted, "Completed:", "w 90px, h 1%", Config.TEXTFONT);
		setupLabelProperties(noOfCompletedValue, ""+table.getRowCount(), "w 90px, h 1%, wrap", Config.TEXTFONT);
	}

	@Override
	protected void setupTableColumns(LapstatsTable table) {
		table.getColumnModel().getColumn(USER_COLUMN).setPreferredWidth(WIDTH_FIRST_COLUMN);
		table.getColumnModel().getColumn(CAR_COLUMN).setPreferredWidth(WIDTH_SECOND_COLUMN);
		table.getColumnModel().getColumn(CLASS_COLUMN).setPreferredWidth(WIDTH_THIRD_COLUMN);
		table.getColumnModel().getColumn(DIFFERENCE_COLUMN).setPreferredWidth(WIDTH_FOURTH_COLUMN);
		
		DefaultTableCellRenderer r = new DefaultTableCellRenderer();
		r.setHorizontalAlignment(SwingConstants.CENTER);
		
		table.getColumnModel().getColumn(CLASS_COLUMN).setCellRenderer(r);
		table.getColumnModel().getColumn(DIFFERENCE_COLUMN).setCellRenderer(r);
	}

	@Override
	protected void tableDoubleClickedAction() {
	}

	@Override
	protected void updateDetails() {
		if(list.getModel().getSize() > 0) {
			setNameValue.setText((String) list.getSelectedValue());
			noOfTracksValue.setText(""+control.getTrackSet((String) list.getSelectedValue()).getTrackNames().size());
			noOfCompletedValue.setText(""+table.getRowCount());
		}
	}

	@Override
	public void updateTable(Vector<Vector<String>> tableData) {
		tableHeader = Control.HANDICAPS_TABLE_HEADERS;
		setupTable(tableData, tableHeader);
		
		updateScrollPaneProperties(tableScrollPane, table);
		
		updateDetails();

		validate();
	}
	
	@Override
	protected Vector<Vector<String>> getTableData() {
		return control.getHandicapsTableData((String) list.getSelectedValue());
	}
	
	/**
	 * @see AbstractTabPanel
	 */
	@Override
	protected int getPreferredSortedColumn() {
		return PREFERRED_SORTED_COLUMN;
	}

}
