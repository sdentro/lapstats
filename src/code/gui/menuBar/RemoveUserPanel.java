package code.gui.menuBar;

/**
 * @author sdentro
 * @version $Id$
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import code.backend.control.Control;
import code.backend.control.GuiControlInterface;
import code.exec.Config;
import code.gui.helperclasses.AbstractGuiPanel;



@SuppressWarnings("serial")
public class RemoveUserPanel extends AbstractGuiPanel {

	private GuiControlInterface control;
	private UserEditPanel parent;
	
	private JList userlistJL;
	private JScrollPane userlistSP;

	private JButton removeBT;
	private JButton cancelBT;
	
	// The listener for the cancel button
	private ActionListener removeAL = new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			if((String)userlistJL.getSelectedValue() != null) {

				int deleteUser = generateOkCancelMessagePane(null, "Removing a user will also remove all laptimes by this user.\nAre you sure you want to remove "+(String)userlistJL.getSelectedValue()+"?", "Remove");
				if(deleteUser == 0 && !control.getUser((String)userlistJL.getSelectedValue()).isSelectedUser()) {
					String selectedUser = (String)userlistJL.getSelectedValue();
					
					// Ok is pressed and user is not currently active
					control.removeUser(control.getUser(selectedUser));
					updateUserList();
					parent.removeUserFromList(selectedUser);
				}
				else if(control.getUser((String)userlistJL.getSelectedValue()).isSelectedUser()){
					// Ok is pressed, but user is currently active
					generateErrorPane(null, "You are currently using this account, change to another user in the Preferences menu first.", "Error");
				}
			}
		}
	};
	
	// The listener for the cancel button
	private ActionListener cancelAL = new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			getThisAGP().getTopLevelAncestor().setVisible(false);
		}
	};
	
	/**
	 * Main constructor. It sets up the elements on the right side of the track edit panel
	 * @param control The main {@link Control}
	 */
	public RemoveUserPanel(UserEditPanel parent, GuiControlInterface control) {
		this.parent = parent;
		this.control = control;
		
		userlistJL = new JList(this.control.getAllUserNames());
		userlistJL.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		setupList(userlistJL, "w 100%::100%, h 100%::100%", Config.TEXTFONT);
		
		setupScrollPaneProperties(userlistSP, userlistJL, "w 100%, h 100%, spanx 2, wrap");
		
		// User properties
		setupButtonProperties(removeBT, "Remove", "w 110px:110px:110px, h 20px", Config.TEXTFONT, removeAL);
		setupButtonProperties(cancelBT, "Close/Cancel", "w 110px:110px:110px, h 20px, wrap", Config.TEXTFONT, cancelAL);
	}
	
	private void updateUserList() {
		userlistJL = new JList(control.getAllUserNames());
		userlistJL.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		updateScrollPaneProperties(userlistSP, userlistJL);
	}
	
	@Override
	protected void initWidgets() {
		userlistJL = new JList();
		userlistSP = new JScrollPane();
		
		removeBT = new JButton();
		cancelBT = new JButton();
	}
}
