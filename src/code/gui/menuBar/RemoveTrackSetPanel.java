package code.gui.menuBar;

/**
 * @author sdentro
 * @version $Id$
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import code.backend.control.Control;
import code.backend.control.GuiControlInterface;
import code.exec.Config;
import code.gui.helperclasses.AbstractGuiPanel;



@SuppressWarnings("serial")
public class RemoveTrackSetPanel extends AbstractGuiPanel {

	private GuiControlInterface control;
	private TrackSetEditPanel parent;
	
	private JList trackSetslistJL;
	private JScrollPane trackSetslistSP;

	private JButton removeBT;
	private JButton cancelBT;
	
	// The listener for the cancel button
	private ActionListener removeAL = new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			if((String)trackSetslistJL.getSelectedValue() != null) {

				int deleteUser = generateOkCancelMessagePane(null, "Are you sure you want to remove "+(String)trackSetslistJL.getSelectedValue()+"?", "Remove");
				if(deleteUser == 0) {
					String selectedSet = (String)trackSetslistJL.getSelectedValue();
					
					// Ok is pressed
					control.removeTrackSetAndAllReferences(selectedSet);
					updateTrackSetsList();
					parent.removeTrackSetFromList(selectedSet);
				}
			}
		}
	};
	
	// The listener for the cancel button
	private ActionListener cancelAL = new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			getThisAGP().getTopLevelAncestor().setVisible(false);
		}
	};
	
	/**
	 * Main constructor. It sets up the elements on the right side of the track edit panel
	 * @param control The main {@link Control}
	 */
	public RemoveTrackSetPanel(TrackSetEditPanel parent, GuiControlInterface control) {
		this.parent = parent;
		this.control = control;
		
		trackSetslistJL = new JList(this.control.getAllTrackSetNames());
		trackSetslistJL.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		setupList(trackSetslistJL, "w 100%::100%, h 100%::100%", Config.TEXTFONT);
		
		setupScrollPaneProperties(trackSetslistSP, trackSetslistJL, "w 100%, h 100%, spanx 2, wrap");
		
		// User properties
		setupButtonProperties(removeBT, "Remove", "w 110px:20%:50%, h 20px", Config.TEXTFONT, removeAL);
		setupButtonProperties(cancelBT, "Close/Cancel", "w 110px:20%:50%, h 20px, wrap", Config.TEXTFONT, cancelAL);
	}
	
	private void updateTrackSetsList() {
		trackSetslistJL = new JList(control.getAllTrackSetNames());
		trackSetslistJL.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		updateScrollPaneProperties(trackSetslistSP, trackSetslistJL);
	}
	
	@Override
	protected void initWidgets() {
		trackSetslistJL = new JList();
		trackSetslistSP = new JScrollPane();
		
		removeBT = new JButton();
		cancelBT = new JButton();
	}
}
