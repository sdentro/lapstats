package code.gui.menuBar;

/**
 * @author sdentro
 * @version $Id$
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import code.backend.control.Control;
import code.backend.control.GuiControlInterface;
import code.exec.Config;
import code.gui.helperclasses.AbstractGuiPanel;



@SuppressWarnings("serial")
public class AddTrackSetPanel extends AbstractGuiPanel {

	private GuiControlInterface control;
	private TrackSetEditPanel parent;
	
	private JLabel trackSetNameJL;
	private JTextField trackSetNameValueJT;
	
	private JButton saveButtonBT;
	private JButton cancelButtonBT;
	
	// The listener for the save button
	private ActionListener saveAL = new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			String newName = trackSetNameValueJT.getText();
			// Check if important fields are filled in
			if(newName.equals("")) {
				generateErrorPane(null, "Set name cannot be empty.", "Error");
			}
			// Check if set already in the system
			else if(control.containsTrackSet(newName)) {
				trackSetNameValueJT.setText("");
				// If so, give message	
				generateErrorPane(null, "Set name is already taken.", "Error");
			}
			else {
				// If not, add the set
				control.addTrackSet(newName);
				parent.addTrackSetToList(newName);
			}
		}
	};
	
	// The listener for the cancel button
	private ActionListener cancelAL = new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			getThisAGP().getTopLevelAncestor().setVisible(false);
		}
	};
	
	// Just an empty keylistener so setupTextFieldProperties stops complaining
	private KeyListener textFieldKL = new KeyListener() {
		@Override
		public void keyPressed(KeyEvent e) {}
		@Override
		public void keyReleased(KeyEvent e) {}
		@Override
		public void keyTyped(KeyEvent e) {}
	};
	
	/**
	 * Main constructor. It sets up the elements on the right side of the track edit panel
	 * @param control The main {@link Control}
	 */
	public AddTrackSetPanel(TrackSetEditPanel parent, GuiControlInterface control) {
		this.parent = parent;
		this.control = control;
		
		// Set properties
		setupLabelProperties(trackSetNameJL, "Set name", "w 20%, h 1%", Config.TEXTFONT);
		setupTextFieldProperties(trackSetNameValueJT, "w 75%, h 1%, wrap", Config.TEXTFONT, textFieldKL);
		
		// Save and cancel buttons
		setupButtonProperties(saveButtonBT, "Save", "w 110px:20%:50%, h 20px", Config.TEXTFONT, saveAL);
		setupButtonProperties(cancelButtonBT, "Close/Cancel", "w 110px:20%:50%, h 20px, wrap", Config.TEXTFONT, cancelAL);
	}
	
	@Override
	protected void initWidgets() {
		trackSetNameJL = new JLabel();
		trackSetNameValueJT = new JTextField();
		
		saveButtonBT = new JButton();
		cancelButtonBT = new JButton();
	}
}
