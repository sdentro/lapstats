package code.gui.menuBar;

/**
 * @author sdentro
 * @version $Id$
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import net.miginfocom.swing.MigLayout;
import code.backend.control.GuiControlInterface;
import code.backend.data.Car;
import code.exec.Config;
import code.gui.helperclasses.AbstractGuiPanel;

public class CarEditPanel extends AbstractGuiPanel {

	private static final long serialVersionUID = 7952904318687440368L;

	private GuiControlInterface control;
	
	private JTextField searchTextField;
	private JButton searchButton;
	
	private JList carList;
	private JScrollPane listScrollPane;
	
	private JLabel propertiesJL;
	private JLabel carClassJL;
	private JTextField carClassJT;
	private JLabel carNameJL;
	private JTextField carNameValueJT;
	private JLabel carIdJL;
	private JTextField carIdValueJT;
	private JButton saveButtonBT;
	private JButton cancelButtonBT;
	
	// The listener for the save button
	private ActionListener saveAL = new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			control.saveCar(carIdValueJT.getText(), carNameValueJT.getText(), carClassJT.getText());
			//TODO now the list needs to be updated with the possibly changed name
		}
	};
	
	// The listener for the cancel button
	private ActionListener cancelAL = new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			getThisAGP().getTopLevelAncestor().setVisible(false);
		}
	};
	
	// Just an empty keylistener so setupTextFieldProperties stops complaining
	private KeyListener textFieldKL = new KeyListener() {
		@Override
		public void keyPressed(KeyEvent e) {}
		@Override
		public void keyReleased(KeyEvent e) {}
		@Override
		public void keyTyped(KeyEvent e) {}
	};
	
	private ListSelectionListener listLSL = new ListSelectionListener() {
		@Override
		public void valueChanged(ListSelectionEvent e) {
			// Make sure this get's only done at onClick, not at onRelease
			if(e.getValueIsAdjusting()) {
				String selectedItem = (String) carList.getSelectedValue();
				// update table with new data
				updatePanel(selectedItem);
			}
		}
	};
	
	private ActionListener searchAL = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) { search(); }
	};
	
	private KeyListener searchKL = new KeyListener() {
		@Override
		public void keyPressed(KeyEvent e) {
			 int key = e.getKeyCode();
		     if (key == KeyEvent.VK_ENTER) {  search(); }
		}
		@Override
		public void keyReleased(KeyEvent e) {}
		@Override
		public void keyTyped(KeyEvent e) {}
	};
	
	public CarEditPanel(GuiControlInterface control) {
		super(new MigLayout());
		this.control = control;
		
		setupTextFieldProperties(searchTextField, "w 114px:114px:114px, h 26px", Config.TEXTFONT, searchKL);
		setupButtonProperties(searchButton, "Search", "w 80px:80px:80px, h 25px, wrap", Config.TEXTFONT, searchAL, searchKL);
		
		carList = new JList(control.sort(control.getAllCarNames()));
		setupList(carList, "w 100%, h 100%", Config.TEXTFONT, listLSL);
		setupScrollPaneProperties(listScrollPane, carList, "w 200px:200px:200px, h 100%, spanx 2, spany 7");
		
		setupLabelProperties(propertiesJL, "Properties", "w 20%, h 1%, wrap", Config.HEADERFONT);
		
		setupLabelProperties(carIdJL, "Car id", "w 20%, h 1%", Config.TEXTFONT);
		setupTextFieldProperties(carIdValueJT, "w 75%, h 1%, wrap", Config.TEXTFONT, textFieldKL);
		carIdValueJT.setEditable(false);
		
		setupLabelProperties(carNameJL, "Car name", "w 20%, h 1%", Config.TEXTFONT);
		setupTextFieldProperties(carNameValueJT, "w 75%, h 1%, wrap", Config.TEXTFONT, textFieldKL);

		setupLabelProperties(carClassJL, "Class", "w 20%, h 1%", Config.TEXTFONT);
		setupTextFieldProperties(carClassJT, "w 75%, h 1%, wrap", Config.TEXTFONT, textFieldKL);
		
		// Save and cancel buttons
		setupButtonProperties(saveButtonBT, "Save", "w 110px:110px:110px, h 20px, aligny bottom, alignx center", Config.TEXTFONT, saveAL);
		setupButtonProperties(cancelButtonBT, "Close/Cancel", "w 110px:110px:110px, h 20px, aligny bottom, alignx center", Config.TEXTFONT, cancelAL);
	}
	
	@Override
	protected void initWidgets() {
		searchTextField = new JTextField();
		searchButton = new JButton();
		
		carList = new JList();
		listScrollPane = new JScrollPane();
		
		propertiesJL = new JLabel();
		
		carIdJL = new JLabel();
		carIdValueJT = new JTextField();
		
		carNameJL = new JLabel();
		carNameValueJT = new JTextField();
		
		carClassJL = new JLabel();
		carClassJT = new JTextField();
		
		saveButtonBT = new JButton();
		cancelButtonBT = new JButton();
	}
	
	
	/**
	 * Update the JTextFields on the panel. The method can work out what details need to be updated by
	 * the given param.
	 * @param selectedItem The name of the selected item (a Track). 
	 */
	public void updatePanel(String selectedItem) {
		Car c = control.getCar(selectedItem);
		// If the car returned equals null no action should be taken
		if(c != null) {
			carIdValueJT.setText(c.getIdentifier());
			carNameValueJT.setText(c.getName());
			carClassJT.setText(c.getCarClass());
		}
	}
	
	/**
	 * Update the list in the panel. Use this method when a selection change has been made.
	 * @param data The {@link Vector} that contains the data that needs to be displayed in the list.
	 * @author sdentro
	 */
	public void updateList(Vector<String> data) {
		removeComponentAndMakeInvisible(carList);
		
		carList = new JList(data);
		setupList(carList, "w 100%, h 100%", Config.TEXTFONT, listLSL);
		updateScrollPaneProperties(listScrollPane, carList);

		validate();
	}
	
	/**
	 * Method that can be called when a search needs to be done
	 */
	private void search() {
		String searchTerm = searchTextField.getText().toLowerCase();
		
		updateList(control.sort(searchFor(searchTerm)));
		
		searchTextField.setText("");
	}
	
	private Vector<String> searchFor(String searchTerm) {
		Vector<String> carData = control.getAllCarNames();
		Vector<String> returnVector = new Vector<String>();
		for(String carName : carData) {
			if(carName.toLowerCase().contains(searchTerm)) {
				returnVector.add(carName);
			}
		}
		return returnVector;
	}
}
