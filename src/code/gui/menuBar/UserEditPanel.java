package code.gui.menuBar;

/**
 * @author sdentro
 * @version $Id$
 */

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import net.miginfocom.swing.MigLayout;
import code.backend.control.GuiControlInterface;
import code.backend.data.User;
import code.exec.Config;
import code.gui.helperclasses.AbstractGuiPanel;

@SuppressWarnings("serial")
public class UserEditPanel extends AbstractGuiPanel {

	private GuiControlInterface control;
	
	private JLabel userLabel;
	private JComboBox userJC;
	private JLabel careerLocationJL;
	private JTextField careerLocationJT;
	private JCheckBox autoLoadCareerJCB;
	private JCheckBox autoSaveDataJCB;
	private JButton saveBT;
	private JButton cancelBT;
	private JButton addUserBT;
	private JButton removeUserBT;
	private JButton browseBT;
	
	private ActionListener saveAL = new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			// Deselect the current user as selected (active user)
			control.getUser(control.getCurrentlyActiveUserName()).setSelectedUser(false);
			
			User u = control.getUser((String) userJC.getSelectedItem());
			control.setCurrentlyActiveUserName((String) userJC.getSelectedItem());
			u.setCareerLocation(careerLocationJT.getText());
			u.setLoadCareerOnStartup(autoLoadCareerJCB.isSelected());
			u.setSaveDataOnExit(autoSaveDataJCB.isSelected());
			u.setSelectedUser(true);
		}
	};
	
	private ActionListener cancelAL = new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			getThisAGP().getTopLevelAncestor().setVisible(false);
		}
	};
	
	private ActionListener doNothingAL = new ActionListener(){
		public void actionPerformed(ActionEvent e) {

		}
	};
	
	private ActionListener comboSelectionAL = new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			// UPDATE ALL FIELDS with new user
			String selected = (String) userJC.getSelectedItem();
			User u = control.getUser(selected);
			
			careerLocationJT.setText(u.getCareerLocation());
			autoLoadCareerJCB.setSelected(u.getLoadCareerOnStartup());
			autoSaveDataJCB.setSelected(u.getSaveDataOnExit());
			
		}
	};	
	
	private ActionListener addUserAL = new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			showAddUserDialog();
		}
	};
	
	private ActionListener removeUserAL = new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			showRemoveUserDialog();
		}
	};
	
	private ActionListener browseAL = new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			File choosenFile = chooseFile();
			if(choosenFile != null) {
				careerLocationJT.setText(choosenFile.getAbsolutePath());
			}
		}
	};
	
	public UserEditPanel(GuiControlInterface control) {
		super(new MigLayout());
		this.control = control;
		paint();
	}
	
	private void paint() {
		User u = control.getUser(control.getCurrentlyActiveUserName());
		
		setupButtonProperties(addUserBT, "Add User", "w 110px:110px:110px, h 20px", Config.TEXTFONT, addUserAL);
		setupSeperatorProperties(new JSeparator(), 1, "w 5px, h 100%, spany 4");
		setupLabelProperties(userLabel, "User name", "w 90px, h 20px", Config.TEXTFONT);
		setupUserComboBox();
		
		setupButtonProperties(removeUserBT, "Remove User", "w 110px:110px:110px, h 20px", Config.TEXTFONT, removeUserAL);
		setupLabelProperties(careerLocationJL, "Career.blt location", "w 90px, h 20px", Config.TEXTFONT);
		setupTextFieldProperties(careerLocationJT , "w 170px:170px:170px, h 20px, wrap", Config.TEXTFONT);
		if(control.getCurrentlyActiveUserName() != null && control.getUser(control.getCurrentlyActiveUserName()).getCareerLocation() != null) {
			careerLocationJT.setText(control.getUser(control.getCurrentlyActiveUserName()).getCareerLocation());
		}
		
		setupButtonProperties(saveBT, "Save", "w 110px:110px:110px, h 20px", Config.TEXTFONT, saveAL);
		setupLabelProperties(new JLabel(), "", "w 90px, h 20px", Config.TEXTFONT);
		setupButtonProperties(browseBT, "Browse", "w 110px:110px:110px, h 20px, alignx center, wrap", Config.TEXTFONT, browseAL);
		
		setupButtonProperties(cancelBT, "Close/Cancel", "w 110px:110px:110px, h 20px", Config.TEXTFONT, cancelAL);
		setupCheckBox(autoSaveDataJCB, "Save data on exit", "", Config.TEXTFONT, u.getSaveDataOnExit(), doNothingAL);
		setupCheckBox(autoLoadCareerJCB, "Load Career on startup", "wrap", Config.TEXTFONT, u.getLoadCareerOnStartup(), doNothingAL);
	}
	
 	

	/**
	 * This method creates a new {@link JComboBox} and inserts all usernames before it is printed on screen.
	 * @author sdentro
	 */
	private void setupUserComboBox() {
		// Create a new JComboBox and get the names of all the users
		userJC = new JComboBox(control.getAllUserNames());
		// Select the current user
		userJC.setSelectedItem(control.getCurrentlyActiveUserName());
		// Put it on screen
		setupComboBox(userJC, "w 170px:170px:170px, h 20px, wrap", Config.TEXTFONT, comboSelectionAL);
	}


	private void showAddUserDialog() {
		generateDialogPanel(getParent(), new AddUserPanel(this, control), "Add user", new Dimension(340, 220));
	}
	
	private void showRemoveUserDialog() {
		generateDialogPanel(getParent(), new RemoveUserPanel(this, control), "Remove user", new Dimension(253, 400));
	}

	@Override
	protected void initWidgets() {
		userLabel = new JLabel();
		userJC = new JComboBox();
		
		careerLocationJL = new JLabel();
		careerLocationJT = new JTextField();
		
		autoLoadCareerJCB = new JCheckBox();
		autoSaveDataJCB = new JCheckBox();
		
		saveBT = new JButton();
		cancelBT = new JButton();
		addUserBT = new JButton();
		removeUserBT = new JButton();
		browseBT = new JButton();
	}

	public void removeUserFromList(String userName) {
		userJC.removeItem(userName);
	}
	
	public void addUserToList(String userName) {
		userJC.addItem(userName);
	}
}
