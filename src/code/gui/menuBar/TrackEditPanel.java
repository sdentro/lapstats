package code.gui.menuBar;

/**
 * @author sdentro
 * @version $Id$
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import net.miginfocom.swing.MigLayout;
import code.backend.control.GuiControlInterface;
import code.backend.data.Track;
import code.exec.Config;
import code.gui.helperclasses.AbstractGuiPanel;

public class TrackEditPanel extends AbstractGuiPanel {

	private static final long serialVersionUID = -9210755313040121659L;

	private GuiControlInterface control;
	
	private JTextField searchTextField;
	private JButton searchButton;
	
	private JList trackList;
	private JScrollPane listScrollPane;
	
	private JLabel propertiesJL;
	private JLabel refTimesJL;
	private JLabel trackId;
	private JTextField trackIdValue;
	private JLabel trackName;
	private JTextField trackNameValue;
	private JLabel trackSetJL;
	private JTextField trackSetsJT;
	private JButton saveButton;
	private JButton cancelButton;
	private Hashtable<String, JTextField> refTimesJT;
	
	private ListSelectionListener listLSL = new ListSelectionListener() {
		@Override
		public void valueChanged(ListSelectionEvent e) {
			// Make sure this get's only done at onClick, not at onRelease
			if(e.getValueIsAdjusting()) {
				String selectedItem = (String) trackList.getSelectedValue();
				// update table with new data
				updatePanel(selectedItem);
			}
		}
	};
	
	// The listener for the save button
	private ActionListener saveAL = new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			Hashtable<String, String> newRefTimes = new Hashtable<String, String>();
			for(String s : refTimesJT.keySet()) {
				// TODO check if value entered is a laptime
				newRefTimes.put(s, refTimesJT.get(s).getText());
			}
			control.saveModifiedTrack(trackIdValue.getText(), trackNameValue.getText(), trackSetsJT.getText(), newRefTimes);
			//TODO now the list needs to be updated with the possibly changed name			
		}
	};
	
	// The listener for the cancel button
	private ActionListener cancelAL = new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			getThisAGP().getTopLevelAncestor().setVisible(false);
		}
	};
	
	// Just an empty keylistener so setupTextFieldProperties stops complaining
	private KeyListener textFieldKL = new KeyListener() {
		@Override
		public void keyPressed(KeyEvent e) {}
		@Override
		public void keyReleased(KeyEvent e) {}
		@Override
		public void keyTyped(KeyEvent e) {}
	};
	
	private ActionListener searchAL = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) { search(); }
	};
	
	private KeyListener searchKL = new KeyListener() {
		@Override
		public void keyPressed(KeyEvent e) {
			 int key = e.getKeyCode();
		     if (key == KeyEvent.VK_ENTER) {  search(); }
		}
		@Override
		public void keyReleased(KeyEvent e) {}
		@Override
		public void keyTyped(KeyEvent e) {}
	};
	
	public TrackEditPanel(GuiControlInterface control) {
		super(new MigLayout());
		this.control = control;
		
		setupTextFieldProperties(searchTextField, "w 114px:114px:114px, h 26px", Config.TEXTFONT, searchKL);
		setupButtonProperties(searchButton, "Search", "w 80px:80px:80px, h 25px, wrap", Config.TEXTFONT, searchAL, searchKL);
		
		trackList = new JList(control.sort(control.getAllTrackNames()));
		setupList(trackList, "w 100%, h 100%", Config.TEXTFONT, listLSL);
		setupScrollPaneProperties(listScrollPane, trackList, "w 200px:200px:200px, h 100%, spanx 2, spany 14");
		
		refTimesJT = new Hashtable<String, JTextField>();
		
		setupLabelProperties(propertiesJL, "Properties", "w 20%, h 1%, wrap", Config.HEADERFONT);
		setupLabelProperties(trackId, "Track id", "w 20%, h 1%", Config.TEXTFONT);
		setupTextFieldProperties(trackIdValue, "w 75%, h 1%, wrap", Config.TEXTFONT, textFieldKL);
		trackIdValue.setEditable(false);
		
		setupLabelProperties(trackName, "Name", "w 20%, h 1%", Config.TEXTFONT);
		setupTextFieldProperties(trackNameValue, "w 75%, h 1%, wrap", Config.TEXTFONT, textFieldKL);
		
		setupLabelProperties(trackSetJL, "Set(s)", "w 20%, h 1%", Config.TEXTFONT);
		setupTextFieldProperties(trackSetsJT, "w 75%, h 1%, wrap", Config.TEXTFONT, textFieldKL);
		
		// Dummy spacer that moves the details down a bit
		setupLabelProperties(new JLabel(), " ", "w 20%, h 1%, wrap", Config.TEXTFONT);
		
		setupLabelProperties(refTimesJL, "Reference times", "w 20%, h 1%, wrap", Config.HEADERFONT);
		for(String c : control.getCarClasses()) {
			setupLabelProperties(new JLabel(), c, "w 20%, h 1%", Config.TEXTFONT);
			JTextField temp = new JTextField();
			refTimesJT.put(c, temp);
			setupTextFieldProperties(temp, "w 75%, h 1%, wrap", Config.TEXTFONT, textFieldKL);
		}
		
		// Save and cancel buttons
		setupButtonProperties(saveButton, "Save", "w 110px:20%:50%, h 20px, aligny bottom, alignx center", Config.TEXTFONT, saveAL);
		setupButtonProperties(cancelButton, "Close/Cancel", "w 110px:20%:50%, h 20px, aligny bottom, alignx center, wrap", Config.TEXTFONT, cancelAL);
	}
	
	/**
	 * Update the JTextFields on the panel. The method can work out what details need to be updated by
	 * the given param.
	 * @param selectedItem The name of the selected item (a Track). 
	 */
	public void updatePanel(String selectedItem) {
		Track t = control.getTrack(selectedItem);
		// If the track returned equals null no action should be taken
		if(t != null) {
			// Set the name and class
			trackIdValue.setText(t.getIdentifier());
			trackNameValue.setText(t.getName());
			trackSetsJT.setText(t.getSetsAsString());
			// For every car class, fetch the textfield and update the text
			for(String s : control.getCarClasses()) {
				refTimesJT.get(s).setText(t.getReferenceTime(s));
			}
		}
	}
	
	/**
	 * Update the list in the panel. Use this method when a selection change has been made.
	 * @param data The {@link Vector} that contains the data that needs to be displayed in the list.
	 * @author sdentro
	 */
	public void updateList(Vector<String> data) {
		removeComponentAndMakeInvisible(trackList);
		
		trackList = new JList(data);
		setupList(trackList, "w 100%, h 100%", Config.TEXTFONT, listLSL);
		updateScrollPaneProperties(listScrollPane, trackList);

		validate();
	}
	
	/**
	 * Method that can be called when a search needs to be done
	 */
	private void search() {
		String searchTerm = searchTextField.getText().toLowerCase();
		
		updateList(control.sort(searchFor(searchTerm)));
		
		searchTextField.setText("");
	}
	
	private Vector<String> searchFor(String searchTerm) {
		Vector<String> trackData = control.getAllTrackNames();
		Vector<String> returnVector = new Vector<String>();
		for(String trackName : trackData) {
			if(trackName.toLowerCase().contains(searchTerm)) {
				returnVector.add(trackName);
			}
		}
		return returnVector;
	}
	
	@Override
	protected void initWidgets() {
		searchTextField = new JTextField();
		searchButton = new JButton();
		
		trackList = new JList();
		listScrollPane = new JScrollPane();
		
		propertiesJL = new JLabel();
		
		trackId = new JLabel();
		trackIdValue = new JTextField();
		trackName = new JLabel();
		trackNameValue = new JTextField();
		trackSetJL = new JLabel();
		trackSetsJT = new JTextField();
		
		refTimesJL = new JLabel();
		
		saveButton = new JButton();
		cancelButton = new JButton();
	}

}
