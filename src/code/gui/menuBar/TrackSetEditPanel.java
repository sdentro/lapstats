package code.gui.menuBar;

/**
 * @author sdentro
 * @version $Id$
 */

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.EventListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import code.backend.control.GuiControlInterface;
import code.exec.Config;
import code.gui.helperclasses.AbstractGuiPanel;

public class TrackSetEditPanel extends AbstractGuiPanel {

	private static final long serialVersionUID = -2717162905566128846L;

	private GuiControlInterface control;
	
	private JTextField searchTrackTF;
	private JButton searchTrackBT;
	
	private JButton addNewTrackSetBT;
	private JButton removeTrackSetBT;
	private JButton saveBT;
	private JButton cancelBT;
	private JComboBox trackSetsJC;
	
	private JButton addTrackToSetBT;
	private JButton removeTrackFromSetBT;
	
	private Vector<String> trackSetTracks;
	private Vector<String> availableTracks;
	private Object[] selectedTrackSetTracksItem;
	private Object[] selectedAvailableTracksItem;
	private JList trackSetTracksList;
	private JList availableTracksList;
	private JScrollPane trackSetSP;
	private JScrollPane tracksSP;
	
	private ActionListener saveAL = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			String trackSet = (String) trackSetsJC.getSelectedItem();
			control.saveTrackSet(trackSet, trackSetTracks);
		}
	};
	
	private ActionListener cancelAL = new ActionListener(){
		@Override
		public void actionPerformed(ActionEvent e) {
			getThisAGP().getTopLevelAncestor().setVisible(false);
		}
	};
	
	private KeyListener addTrackSetKL = new KeyListener() {
		@Override
		public void keyPressed(KeyEvent e) {
			 int key = e.getKeyCode();
		     if (key == KeyEvent.VK_ENTER) {  
		    	 showAddTrackSetDialog();
		     }
		}
		
		@Override
		public void keyTyped(KeyEvent e) { }
		@Override
		public void keyReleased(KeyEvent e) { }
	};
	
	private ActionListener addTrackSetAL = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			showAddTrackSetDialog();
		}
	};
	
	private KeyListener removeTrackSetKL = new KeyListener() {
		@Override
		public void keyPressed(KeyEvent e) {
			 int key = e.getKeyCode();
		     if (key == KeyEvent.VK_ENTER) {  
		    	 showRemoveTrackSetDialog();
		     }
		}
		
		@Override
		public void keyTyped(KeyEvent e) { }
		@Override
		public void keyReleased(KeyEvent e) { }
	};
	
	private ActionListener removeTrackSetAL = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			showRemoveTrackSetDialog();
		}
	};
	
	private ActionListener selectionAL = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			updateLists();
		}
	};
	
	private ActionListener addTrackToSetAL = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			if(!((String) trackSetsJC.getSelectedItem()).equals("Select existing set...")) {
				for(Object t : selectedAvailableTracksItem) {
					trackSetTracks.add((String) t);
					availableTracks.remove((String) t);
				}
				
				control.sort(availableTracks);
				control.sort(trackSetTracks);
				
				updateAvailableTrackNamesList(availableTracks);
				updateTrackSetTrackNamesList(trackSetTracks);
			}
		}
	};
	
	private ActionListener removeTrackFromSetAL = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			for(Object t : selectedTrackSetTracksItem) {
				trackSetTracks.remove((String) t);
				availableTracks.add((String) t);
			}
			
			control.sort(availableTracks);
			control.sort(trackSetTracks);
			
			updateAvailableTrackNamesList(availableTracks);
			updateTrackSetTrackNamesList(trackSetTracks);
		}
	};
	
	private KeyListener searchKL = new KeyListener() {
		@Override
		public void keyPressed(KeyEvent e) {
			int key = e.getKeyCode();
		     if (key == KeyEvent.VK_ENTER) {  search(); }
		}
		
		@Override
		public void keyTyped(KeyEvent e) {}
		@Override
		public void keyReleased(KeyEvent e) {}
	};

	private ActionListener searchAL = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) { search(); }
	}; 
	
	private ListSelectionListener trackSetTracksListLSL = new ListSelectionListener() {
		@Override
		public void valueChanged(ListSelectionEvent arg0) {
			selectedTrackSetTracksItem = trackSetTracksList.getSelectedValues();
		}
	};
	
	private ListSelectionListener availableTracksListLSL = new ListSelectionListener() {
		@Override
		public void valueChanged(ListSelectionEvent arg0) {
			selectedAvailableTracksItem = availableTracksList.getSelectedValues();
		}
	};
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Constructor and inherited method(s)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	public TrackSetEditPanel(GuiControlInterface control) {
		this.control = control;
		
		setupButtonProperties(addNewTrackSetBT, "Add set", "w 120px, h 25px", Config.TEXTFONT, addTrackSetAL, addTrackSetKL);
		setupSeperatorProperties(new JSeparator(), 1, "w 5px, h 100%, spany 6");
		setupTextFieldProperties(searchTrackTF, "w 115px:115px:115px, h 26px", Config.TEXTFONT, searchKL);		
		setupButtonProperties(searchTrackBT, "Search", "w 80px:80px:80px, h 25px", Config.TEXTFONT, searchAL, searchKL);
		setupLabelProperties(new JLabel(), "", "", null);
		setupLabelProperties(new JLabel(), "", "", null);
		setupTrackSetsJC();
		setupComboBox(trackSetsJC, "w 200px, h 25px, wrap", Config.TEXTFONT, selectionAL);
		
		
		setupButtonProperties(removeTrackSetBT, "Remove set", "w 120px, h 25px", Config.TEXTFONT, removeTrackSetAL, removeTrackSetKL);
		availableTracks = control.sort(control.getAllTrackNames());
		availableTracksList = new JList(availableTracks);
		setupList(availableTracksList, "w 100%, h 100%", Config.TEXTFONT, availableTracksListLSL);
		setupScrollPaneProperties(tracksSP, availableTracksList, "w 200px:200px:200px, h 100%, spany 5, spanx 2");

		setupLabelProperties(new JLabel(), "", "", null);
		setupLabelProperties(new JLabel(), "", "", null);
		
		trackSetTracks = new Vector<String>();
		trackSetTracksList = new JList(trackSetTracks);
		setupList(trackSetTracksList, "w 100%, h 100%", Config.TEXTFONT, trackSetTracksListLSL);
		setupScrollPaneProperties(trackSetSP, trackSetTracksList, "w 200px:200px:200px, h 100%, spany 5, wrap");
		
		setupButtonProperties(saveBT, "Save", "w 120px, h 25px, wrap", Config.TEXTFONT, saveAL);
		setupButtonProperties(cancelBT, "Close/Cancel", "w 120px, h 25px, wrap", Config.TEXTFONT, cancelAL);
		setupLabelProperties(new JLabel(), "", "h 50px:50px:50px", null);
		setupLabelProperties(new JLabel(), "", "h 50px:50px:50px, wrap", null);
		setupLabelProperties(new JLabel(), "", "", null);
		setupButtonProperties(removeTrackFromSetBT, "<", "w 50px:50px:50px, h 25px, alignx center, aligny top", Config.TEXTFONT, removeTrackFromSetAL);
		setupButtonProperties(addTrackToSetBT, ">", "w 50px:50px:50px, h 25px, alignx center, aligny top", Config.TEXTFONT, addTrackToSetAL);
	}
	
	@Override
	protected void initWidgets() {
		searchTrackBT = new JButton();
		searchTrackTF = new JTextField(); 
		addNewTrackSetBT = new JButton();
		removeTrackSetBT = new JButton();
		saveBT = new JButton();
		cancelBT = new JButton();
		trackSetsJC = new JComboBox();
		
		addTrackToSetBT = new JButton();
		removeTrackFromSetBT = new JButton();
		
		trackSetTracksList = new JList();
		availableTracksList = new JList();
		trackSetSP = new JScrollPane();
		tracksSP = new JScrollPane();
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// GUI methods
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	private void showAddTrackSetDialog() {
		generateDialogPanel(getParent(), new AddTrackSetPanel(this, control), "Add Track Set", new Dimension(255, 103));
	}
	
	private void showRemoveTrackSetDialog() {
		generateDialogPanel(getParent(), new RemoveTrackSetPanel(this, control), "Remove Track Set", new Dimension(255, 302));
	}
	
	/**
	 * This method configures the list that goes into the trackSets Combobox. It add's the "Select..." option basically.
	 */
	private void setupTrackSetsJC() {
		Vector<String> trackSetNames = new Vector<String>();
		trackSetNames.add("Select existing set...");
		trackSetNames.addAll(control.sort(control.getAllTrackSetNames()));

		trackSetsJC = new JComboBox(trackSetNames);
	}
	
	private void updateLists() {
		String selection = (String) trackSetsJC.getSelectedItem();
		if(!selection.equals("Select existing set...")) {
			Vector<String> trackListOfSelection = control.sort(control.getTrackSet(selection).getTrackNames());
			Vector<String> allTracks = control.sort(control.getAllTrackNames());
			
			allTracks.removeAll(trackListOfSelection);
			
			availableTracks.removeAllElements();
			availableTracks.addAll(allTracks);
			trackSetTracks.removeAllElements();
			trackSetTracks.addAll(trackListOfSelection);
		}
		else {
			availableTracks = control.sort(control.getAllTrackNames());
			trackSetTracks = new Vector<String>();
		}
		updateAvailableTrackNamesList(availableTracks);
		updateTrackSetTrackNamesList(trackSetTracks);
	}
	
	/**
	 * Set up the track names
	 * @param data
	 */
	private void updateAvailableTrackNamesList(Vector<String> data) {
		removeComponentAndMakeInvisible(availableTracksList);
		
		availableTracksList = new JList(data);
		setupList(availableTracksList, "w 100%, h 100%", Config.TEXTFONT, availableTracksListLSL);
		updateScrollPaneProperties(tracksSP, availableTracksList);

		validate();
	}
	
	/**
	 * Set up the track sets list
	 * @param data
	 */
	private void updateTrackSetTrackNamesList(Vector<String> data) {
		removeComponentAndMakeInvisible(trackSetTracksList);
		
		trackSetTracksList = new JList(data);
		setupList(trackSetTracksList, "w 100%, h 100%", Config.TEXTFONT, trackSetTracksListLSL);
		updateScrollPaneProperties(trackSetSP, trackSetTracksList);

		validate();
	}
	
	@Override
	public void setupList(JList list, String constraints, Font font, EventListener...eventListeners) {
		list.setFont(font);
		list.setVisible(true);
		for(EventListener listener : eventListeners) {
			
			if(listener instanceof ListSelectionListener) {
				list.addListSelectionListener((ListSelectionListener) listener);
			}
			else if(listener instanceof KeyListener) {
				list.addKeyListener((KeyListener) listener);
			}
		}
		add(list, constraints);
	}
	
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Other methods
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	/**
	 * Method that can be called when a search needs to be done
	 */
	private void search() {
		String searchTerm = searchTrackTF.getText().toLowerCase();
		availableTracks = control.sort(searchFor(searchTerm));
		/*
		 *  Remove all tracks that are in the trackSetTracks Vector. This is usefull when searching, adding a track
		 *  then resetting the search results. Without this removeAll() the just added track will be shown as available. 
		 */
		availableTracks.removeAll(trackSetTracks); 
		updateAvailableTrackNamesList(availableTracks);
		
		searchTrackTF.setText("");
	}
	
	private Vector<String> searchFor(String searchTerm) {
		String selection = (String) trackSetsJC.getSelectedItem();
		Vector<String> trackListOfSelection;
		if(selection.equals("Select existing set...")) {
			trackListOfSelection = new Vector<String>();
		}
		else {
			trackListOfSelection = control.sort(control.getTrackSet(selection).getTrackNames());
		}
		Vector<String> allTracks = control.sort(control.getAllTrackNames());
		
		allTracks.removeAll(trackListOfSelection);
		Vector<String> returnVector = new Vector<String>();
		for(String trackName : allTracks) {
			if(trackName.toLowerCase().contains(searchTerm)) {
				returnVector.add(trackName);
			}
		}
		return returnVector;
	}
	
	public void addTrackSetToList(String setName) {
		trackSetsJC.addItem(setName);
	}
	
	public void removeTrackSetFromList(String setName) {
		trackSetsJC.removeItem(setName);
	}

}
