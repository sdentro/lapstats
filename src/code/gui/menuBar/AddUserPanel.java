package code.gui.menuBar;

/**
 * @author sdentro
 * @version $Id$
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import code.backend.control.Control;
import code.backend.control.GuiControlInterface;
import code.backend.data.User;
import code.exec.Config;
import code.gui.helperclasses.AbstractGuiPanel;



@SuppressWarnings("serial")
public class AddUserPanel extends AbstractGuiPanel {

	private GuiControlInterface control;
	private UserEditPanel parent;
	
	private JLabel userNameJL;
	private JTextField userNameValueJT;
	private JLabel careerJL;
	private JTextField careerValueJT;
	
	private JButton browseBT;
	
	private JLabel autoLoadCareerJL;
	private JCheckBox autoLoadCareerJCB;
	private JLabel autoSaveDataJL;
	private JCheckBox autoSaveDataJCB;
	
	private JButton saveButtonBT;
	private JButton cancelButtonBT;
	
	// The listener for the save button
	private ActionListener saveAL = new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			String newName = userNameValueJT.getText();
			String newCareer = careerValueJT.getText();
			
			// Check if important fields are filled in
			if(newName.equals("")) {
				generateErrorPane(null, "User Name cannot be empty.", "Error");
			}
			// Check if user already in the system
			else if(control.containsUser(newName)) {
				userNameValueJT.setText("");
				
				// If so, give message	
				generateErrorPane(null, "User Name is already taken.", "Error");
			}
			else {
				// If not, add the user
				control.addUser(newName);
				
				// Set intangibles
				User u = control.getUser(newName);
				u.setCareerLocation(newCareer);
				u.setLoadCareerOnStartup(autoLoadCareerJCB.isSelected());
				u.setSaveDataOnExit(autoSaveDataJCB.isSelected());
				
				parent.addUserToList(newName);
			}
		}
	};
	
	// The listener for the cancel button
	private ActionListener cancelAL = new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			getThisAGP().getTopLevelAncestor().setVisible(false);
		}
	};
	
	private ActionListener doNothingAL = new ActionListener(){
		public void actionPerformed(ActionEvent e) {

		}
	};
	
	// Just an empty keylistener so setupTextFieldProperties stops complaining
	private KeyListener textFieldKL = new KeyListener() {
		@Override
		public void keyPressed(KeyEvent e) {}
		@Override
		public void keyReleased(KeyEvent e) {}
		@Override
		public void keyTyped(KeyEvent e) {}
	};
	
	private ActionListener browseAL = new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			File choosenFile = chooseFile();
			if(choosenFile != null) {
				careerValueJT.setText(choosenFile.getAbsolutePath());
			}
		}
	};
	
	/**
	 * Main constructor. It sets up the elements on the right side of the track edit panel
	 * @param control The main {@link Control}
	 */
	public AddUserPanel(UserEditPanel parent, GuiControlInterface control) {
		this.parent = parent;
		this.control = control;
		
		// User properties
		setupLabelProperties(userNameJL, "Username", "w 20%, h 1%", Config.TEXTFONT);
		setupTextFieldProperties(userNameValueJT, "w 75%, h 1%, wrap", Config.TEXTFONT, textFieldKL);

		setupLabelProperties(careerJL, "Career location", "w 20%, h 1%", Config.TEXTFONT);
		setupTextFieldProperties(careerValueJT, "w 75%, h 1%, wrap", Config.TEXTFONT, textFieldKL);
		
		setupLabelProperties(new JLabel(), "", "w 1%, h 1%", Config.TEXTFONT);
		setupButtonProperties(browseBT, "Browse", "w 110px:110px:110px, h 20px, alignx center, wrap", Config.TEXTFONT, browseAL);
		
		setupSeperatorProperties(new JSeparator(), 0, "w 100%, h 5px, spanx 3, wrap");
		
		setupLabelProperties(autoLoadCareerJL, "Load Career.blt on startup", "w 90px, h 20px", Config.TEXTFONT);
		setupCheckBox(autoLoadCareerJCB, "", "wrap", Config.TEXTFONT, false, doNothingAL);
		
		setupLabelProperties(autoSaveDataJL, "Save data on exit", "w 90px, h 20px", Config.TEXTFONT);
		setupCheckBox(autoSaveDataJCB, "", "wrap", Config.TEXTFONT, false, doNothingAL);
		
		setupSeperatorProperties(new JSeparator(), 0, "w 100%, h 5px, spanx 3, wrap");
		
		// Save and cancel buttons
		setupButtonProperties(saveButtonBT, "Save", "w 110px:110px:110px, h 20px, alignx center", Config.TEXTFONT, saveAL);
		setupButtonProperties(cancelButtonBT, "Close/Cancel", "w 110px:110px:110px, h 20px, alignx center, wrap", Config.TEXTFONT, cancelAL);
	}
	
	@Override
	protected void initWidgets() {
		careerJL = new JLabel();
		careerValueJT = new JTextField();
		
		userNameJL = new JLabel();
		userNameValueJT = new JTextField();
		
		browseBT = new JButton();
		
		autoLoadCareerJL = new JLabel();
		autoLoadCareerJCB = new JCheckBox();
		autoSaveDataJL = new JLabel();
		autoSaveDataJCB = new JCheckBox();
		
		saveButtonBT = new JButton();
		cancelButtonBT = new JButton();
	}
}
