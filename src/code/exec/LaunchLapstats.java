package code.exec;

/**
 * @author sdentro
 * @version $Id$
 */

import code.backend.DataController;
import code.backend.FileProcessor;
import code.backend.control.Control;
import code.backend.control.GuiControl;
import code.backend.control.TalkToGui;
import code.gui.main.MainFrame;

public class LaunchLapstats {
	
	public static void main(String args[]) {
		start();
	}
	
	private static void start() {
		DataController dc = new DataController();
		Control c = new Control(dc);
		TalkToGui ttg = new TalkToGui();
		FileProcessor fp = new FileProcessor(c, ttg);
		GuiControl gc = new GuiControl(c, dc, fp, ttg);
		c.setGuiControl(gc);
		
		fp.readSavedData();
		gc.setCarClasses(c.getCarClasses());
		
		gc.loadCareerOfAllUsersAtStartup();

		MainFrame m = new MainFrame(gc);
//		ttg.setStatusBar(m.getStatusBar());
		
	}
}
