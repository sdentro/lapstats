package code.exec;

/**
 * @author sdentro
 * @version $Id$
 */

import java.awt.Font;

public abstract class Config {

	// Used in MainFrame only
	public static final String mainFrameTitle = "Lapstats";
	
	public static final String aboutText = "Lapstats for GTR2 v1.2.5\n\n" +
									 "This software package comes as is, usage is at own\n" +
									 "risk. Lapstats is free and may never be sold or used\n" +
									 "commercially without written conscent of the author.\n\n" +
									 "Created by stevy99\n" +
									 "lapstats@gmail.com";
	
	public static final String fileChooserStandardDirectory = "desktop";
	
	public static final String ICON_LOCATION = "icon.png";
	
	public static final int MAIN_FRAME_WIDTH = 800;
	public static final int MAIN_FRAME_HEIGHT = 600;

	// From Control
	public static final Font TEXTFONT = new Font("times", Font.PLAIN, 12);
	public static final Font HEADERFONT = new Font("times", Font.BOLD, 12);
	public static final Font STATUSBARFONT = new Font("times", Font.PLAIN, 10);

}
