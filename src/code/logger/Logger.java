package code.logger;

/**
 * @author sdentro
 * @version $Id: $
 */

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class Logger extends Thread {
	
	private FileWriter fileOut;
	private BufferedWriter out;
	
	private Queue<String> messageQueue;

	
	public Logger() {
		try {
			fileOut = new FileWriter("log.txt");
			out = new BufferedWriter(fileOut);
			messageQueue = new LinkedBlockingQueue<String>();
		} 
		catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void log(String className, String message) {
			messageQueue.add(getTime()+" ["+className+"] "+message+"\n");
	}

	private String getTime() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd 'at' hh:mm:ss");

		
		return sdf.format(cal.getTime());
	}
	
	public void run() {
		while(true) {
			if(!messageQueue.isEmpty()) {
				try {
					out.append(messageQueue.poll());
					out.flush();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
