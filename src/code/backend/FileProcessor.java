package code.backend;

/**
 * @author sdentro
 * @version $Id$
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

import code.backend.control.Control;
import code.backend.control.TalkToGui;
import code.backend.data.Car;
import code.backend.data.Id;
import code.backend.data.Track;
import code.backend.data.TrackSet;
import code.backend.data.User;

public class FileProcessor {

	private String dataFile = "data.dat";
	private FileReader fis;
	private BufferedReader in;
	private Control control;
	private FileWriter fout;
	private BufferedWriter out;
	private TalkToGui talkToGui;
	
	public FileProcessor(Control control, TalkToGui talkToGui) {
		this.control = control;
		this.talkToGui = talkToGui;
	}
	
	public void setDataFile(String fileName) {
		dataFile = fileName;
	}
	
	
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Read a Career.blt File methods 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
		// TODO check of de String eigenlijk wel een career.blt file is!
	public void readCareer(String file, String user) {
		talkToGui.setStatusBarMessage("Loading career of "+user);
		try {
			fis = new FileReader(file);
			in = new BufferedReader(fis);
			String nextLine = in.readLine();
			while (!(nextLine == null)) {
				if (nextLine.equals("[PLAYERTRACKSTAT]")) {
					readLaptime(user);
				} else if (nextLine.equals("[CAREER]")) {
					readGlobalData(user); // this does nothing.. needs beter fix
				}
				nextLine = in.readLine();
			}
			in.close();
			fis.close();
		} catch (FileNotFoundException e) {
			System.out.println("No career set for user "+user);
//			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Error in ReadCareer of FileProcessor r60");
			e.printStackTrace();
		}
		talkToGui.setStatusBarMessage("Done loading");
	}

	private void readLaptime(String user) throws IOException {
		/*
		 *  Make trackId go to uppercase, no matter what. This was a big bug. It turns out that GTR2 writes the
		 *  trackname for a new laptime to partly lowercase characters. That's why the new laptime wouldn't show
		 *  up before reloading GTR2. The second time GTR2 wrote the trackname in uppercase characters. After that
		 *  everything was fine. The addition of toUpperCase() fixes the bug of Lapstats not handling the quirky 
		 *  feature of GTR2 well.
		 */
		String trackId = splitInput(in.readLine()).toUpperCase();

		// Pass up on the next two lines, we're not using them
		splitInput(in.readLine());
		splitInput(in.readLine());

		String carId = splitInput(in.readLine()).toUpperCase();
		String laps = splitInput(in.readLine());
		
		// Read the laptime and convert it to human readable.
		String nonHumanReadableLaptime = splitInput(in.readLine());
		String laptime = control.convertToHumanreadableLaptime(nonHumanReadableLaptime);

		if(Double.parseDouble(nonHumanReadableLaptime) != 0.000000 && control.containsTrack(trackId) && control.containsCar(carId)) {
			control.setLaptime(user, carId, trackId, laptime, nonHumanReadableLaptime, Integer.parseInt(laps));
		}
	}

	private void readGlobalData(String user) throws IOException {
		for (int i = 0; i < 46; i++) {
			// Do nothing really..
			in.readLine();
		}
	}

	/**
	 * The Strings that are read in consist of identifier=value pair. This
	 * method splits the identifier of the value and returns the value.
	 * 
	 * @param input
	 *            : String that needs to be split
	 * @return String, the value in this line
	 */
	public String splitInput(String input) {
		String spltInput[] = input.split("=");
		if(spltInput.length > 1) {
			return spltInput[1];
		}
		else{
			return "";
		}
	}

	
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Read saved data files (data.dat)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	/**
	 * Write out the saved laptimes and user data to the laptimes.dat file. The data of data.dat is stale and does not
	 * need to be written over and over again.
	 * @author sdentro
	 */
	public void writeData() {
		try {
			fout = new FileWriter(dataFile);
			out = new BufferedWriter(fout);
		
			writeCarClasses(control.getCarClasses());
			
			for(String u : control.getAllUserNames()) {
				writeUserData(u);
			}
			
			for(Id c : control.getAllCarIds()) {
				writeCar(control.getCar(c));
			}
			
			for(Id t : control.getAllTrackIds()) {
				writeTrack(control.getTrack(t));
			}
			out.close();
			fout.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Read the data file. Data.dat contains cars and tracks.
	 * @author sdentro
	 */
	public void readSavedData() {
		try {
			fis = new FileReader(dataFile);
			in = new BufferedReader(fis);
		
			String nextLine = in.readLine();
			
			while(!(nextLine == null)) {
				if(nextLine.equals("[Car]")) { 
					Car nwCar = readCar();
					control.addCar(nwCar);
				}
				
				if(nextLine.equals("[Track]")) { 
					Track t = readTrack();
					control.addTrack(t); 
					control.addTrackToOnlyTheseTracksets(t, t.getTracksets());
				}
				
				if(nextLine.equals("[User]")) {
					User u = readUser();
					control.addUser(u);
					if(u.isSelectedUser()) {
						control.setCurrentlyActiveUserName(u.getName());
					}
				}
				
				if(nextLine.equals("[FirstStartup]")) { control.setFirstTime(); }
				
				if(nextLine.equals("[Classes]")) { control.setCarClasses(readCarClasses()); }
				
				nextLine = in.readLine();
			}
			in.close();
			fis.close();
		} catch (FileNotFoundException e) {
	//		e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Import and export settings
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	public void exportSettings(String filePath) {
		try {
			fout = new FileWriter(filePath);
			out = new BufferedWriter(fout);
		
			// Export user settings
			for(String userName : control.getAllUserNames()) {
				writeUserData(userName);
			}
			
			for(String trackSetName : control.getAllTrackSetNames()) {
				if(trackSetNotStandard(trackSetName))
					writeTrackSet(trackSetName);
			}
			
			out.close();
			fout.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private boolean trackSetNotStandard(String trackSetName) {
		if(trackSetName.equals("Full") 
				|| trackSetName.equals("2003") 
				|| trackSetName.equals("2004") 
				|| trackSetName.equals("GTR2")) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public void importSettings(String filePath) {
		try {
			fis = new FileReader(filePath);
			in = new BufferedReader(fis);
		
			String nextLine = in.readLine();
			
			while(!(nextLine == null)) {
				if(nextLine.equals("[User]")) {
					User u = readUser();
					control.addUser(u);
				}
			
				if(nextLine.equals("[TrackSet]")) {
					TrackSet ts = readTrackSet();
					control.addTrackSet(ts.getName());
					control.saveTrackSet(ts.getName(), ts.getTrackNames());
				}
				nextLine = in.readLine();
			}
			in.close();
			fis.close();
		} catch (FileNotFoundException e) {
	//		e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
		
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Convenience methods
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	private void writeUserData(String u) throws IOException {
		User us = control.getUser(u);
		out.append("[User]\n");
		out.append("UserName="+us.getName()+"\n");
		out.append("CareerLoc="+us.getCareerLocation() + "\n");
		out.append("Selected="+us.isSelectedUser()+"\n");
		out.append("LoadData="+us.getLoadCareerOnStartup()+"\n");
		out.append("SaveCareer="+us.getSaveDataOnExit()+"\n");
		out.flush();
	}

	private User readUser() throws IOException {
		String name = splitInput(in.readLine());
		String careerLoc = splitInput(in.readLine());
		boolean isSelected = Boolean.parseBoolean(splitInput(in.readLine()));
		boolean loadCareer = Boolean.parseBoolean(splitInput(in.readLine()));
		boolean saveData = Boolean.parseBoolean(splitInput(in.readLine()));
		User u = new User(name);

		u.setCareerLocation(careerLoc);
		u.setSelectedUser(isSelected);
		u.setLoadCareerOnStartup(loadCareer);
		u.setSaveDataOnExit(saveData);
		return u;
	}

	/**
	 * Write a single track to the specified BufferedWriter
	 * @param out
	 * @param t
	 * @throws IOException
	 */
	private void writeTrack(Track t) throws IOException {
		out.append("[Track]\n");
		out.append("TrackName=" + t.getName() + "\n");
		out.append("TrackId=" + t.getId().getIdentifier() + "\n");
		out.append("Author="+t.getAuthor() + "\n");
		// Create a list of the class reference times
		String refTimes = "RefTimes=";
		Hashtable<String, String> refTimesTable = t.getReferenceTimes();
		for(String s : control.getCarClasses()) {
			if(refTimesTable.get(s) == null) {
				refTimes = refTimes + s + "#" + "NA" + ",";
			}
			else {
				refTimes = refTimes + s + "#" + refTimesTable.get(s) + ",";
			}
		}
		refTimes = refTimes + "\n";
		out.append(refTimes);
		// Append the list of sets that this track belongs to
		out.append("Sets=" + t.getSetsAsString() +"\n");
		
		out.flush();
	}
	
	/**
	 * Read in a single track from the given {@link BufferedReader}.
	 * @param in is the BufferedReader that contains the data of the track
	 * @return the created Track object.
	 * @throws IOException
	 */
	private Track readTrack() throws IOException {
		// Read track parameters
		String name = splitInput(in.readLine());
		String id = splitInput(in.readLine());

		// Create new object and return it
		Track newTrack = new Track(name, id);
		// Set author
		String author = splitInput(in.readLine());
		newTrack.setAuthor(author);
		// Read in the reference times
		String refTimes = splitInput(in.readLine());
		// Split the reference times from eachother
		String times[] = refTimes.split(",");
		for(String s : times) {
			// Now split the time from the class
			String time[] = s.split("#");
			if(time.length > 1) {
				newTrack.addReferenceTime(time[0], time[1]);
			}
		}
		// Read in the sets that this track belongs to, split it up and add them
		String setString[] = splitInput(in.readLine()).split(",");
		for(String s : setString) {
			newTrack.addToSet(s);
		}
		
		return newTrack;
	}
	
	private TrackSet readTrackSet() throws IOException  {
		String name = splitInput(in.readLine());
		String trackIdString = splitInput(in.readLine());
		String trackIds[] = trackIdString.split(",");
		Vector<Track> tracks = new Vector<Track>();
		for(String id : trackIds) {
			tracks.add(control.getTrack(new Id(id)));
		}
		
		return new TrackSet(name, tracks);
	}
	
	private void writeTrackSet(String trackSetName) throws IOException {
		TrackSet trackSet = control.getTrackSet(trackSetName);
		out.append("[TrackSet]\n");
		out.append("Name=" + trackSet.getName() + "\n");
		int i = 0;
		String tracks = "Tracks=";
		for(Track track : trackSet.getTracks()) {
			if(i == 0) {
				tracks += track.getIdentifier();
			}
			else {
				tracks += "," + track.getIdentifier();
			}
			i++;
		}
		tracks += "\n";
		out.append(tracks);
	}
	
	private void writeCar(Car c) throws IOException {
		out.append("[Car]\n");
		out.append("CarName=" + c.getName() + "\n");
		out.append("CarId=" + c.getId().getIdentifier() + "\n");
		out.append("Author=" + c.getAuthor() + "\n");
		
		String classes = "Classes=" + c.getCarClass() + "\n";
		out.append(classes);
		out.flush();
	}
	
	private Car readCar() throws IOException {
		String name = splitInput(in.readLine());
		String id = splitInput(in.readLine());
		String author = splitInput(in.readLine());
		String carClasses = splitInput(in.readLine());
		
		Car c = new Car(name, id);
		
		c.setAuthor(author);
		
		String classes[] = carClasses.split(", ");
		for(String carClass : classes) {
			if(!carClass.equals("Full")) {
				c.setToClass(carClass);
			}
		}
		return c;
	}
	
	private void writeCarClasses(String[] carClasses) throws IOException {
		out.append("[Classes]\n");
		String classes = "";
		int i = 0;
		for(String carClass : carClasses) {
			if(i == 0) {
				classes = "Classes=" + carClass;
			}
			else {
				classes = classes + "," + carClass;
			}
			i++;
		}
		out.append(classes + "\n");
	}
	
	private String[] readCarClasses() throws IOException {
		String readClasses = splitInput(in.readLine());
		return readClasses.split(",");
	}
	
//	private void printAllCarsAndTracks() {
//		Vector<String> cars = new Vector<String>();
//		Vector<String> tracks = new Vector<String>();
//		
//		for(String carName : control.getAllCarNames()) {
//			Car c = control.getCar(carName);
//			cars.add(c.getName() + " by " + c.getAuthor());
//		}
//		
//		for(String trackName : control.getAllTrackNames()) {
//			Track c = control.getTrack(trackName);
//			tracks.add(c.getName() + " by " + c.getAuthor());
//		}
//		cars = control.sort(cars);
//		tracks = control.sort(tracks);
//		
//		for(String car : cars) {
//			System.out.println(car);
//		}
//		
//		for(String track : tracks) {
//			System.out.println(track);
//		}
//	}

}
