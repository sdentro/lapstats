package code.backend.table;

/**
 * @author sdentro
 * @version $Id$
 */

public class EmptyCellObject extends CellObject {

	public String toString() {
		return "Empty cell object";
	}
}
