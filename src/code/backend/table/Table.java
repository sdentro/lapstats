package code.backend.table;

/**
 * The Table class implements a table. It consists of a {@link Vector} of {@link Vector}s that contains {@link Object}s. 
 * The Table will take care of filling in the cells for you. So after creation of a row and column, adding an {@link Object} 
 * is as simple as pointing to the right cell by giving it's row and column index.
 * @author sdentro
 * @version $Id$
 */

import java.util.Iterator;
import java.util.Vector;

public class Table<Row, Col, Cell extends CellObject> {
		
	private Vector<Vector<CellObject>> table;
	
	private int numberOfColumns;
	private int numberOfRows;
	
	private Vector<Row> rowObjects;
	private Vector<Col> columnObjects;
	
	/**
	 * Main constructor for class {@link Table}. Creates a new table.
	 * @author sdentro
	 */
	public Table() {
		table = new Vector<Vector<CellObject>>();
		numberOfColumns = 0;
		numberOfRows = 0;
		
		columnObjects = new Vector<Col>();
		rowObjects = new Vector<Row>();
	}
	
	/**
	 * Add a row to the table. {@link Table} will take care of filling in empty Objects at the cells.
	 * @param element The element associated with the row.
	 * @author sdentro
	 */
	public void addRow(Row element) {
		rowObjects.add(element);
		Vector<CellObject> newRow = new Vector<CellObject>();
		// Fill up the columns that are filled in for other rows
		for(int i=0; i<numberOfColumns; i++) {
			newRow.add(new EmptyCellObject());
		}
		table.add(newRow);
		numberOfRows++;
	}
	
	/**
	 * Remove a row from the table.
	 * @param element
	 * @author sdentro
	 */
	public void removeRow(Row element) {
		int index = rowObjects.indexOf(element);
		rowObjects.remove(element);
		table.remove(index);
	}

	/**
	 * Add a column to the {@link Table}. A Col already in the table will be added again.
	 * @param element The element associated with the column.
	 * @author sdentro
	 */
	public void addColumn(Col element) {
		columnObjects.add(element);
		for(Vector<CellObject> row : table) {
			row.add(new EmptyCellObject());
		}
		numberOfColumns++;
	}
	
	/**
	 * Removes a column and all the elements under it from the table
	 * @param element
	 * @author sdentro
	 */
	public void removeColumn(Col element) {
		int index = columnObjects.indexOf(element);
		columnObjects.remove(element);
		for(Vector<CellObject> row : table) {
			row.remove(index);
		}
		numberOfColumns--;
	}
	
	/**
	 * Set the Object in a particular cell.
	 * @param object The {@link Object} that needs to be placed inside the table. Iff this Object is null, no changes will be made.
	 * @param row The {@link Object} associated with the row it needs to go in.
	 * @param column The {@link Object} associated with column it needs to go in.
	 * @author sdentro
	 */
	public void setCell(Cell object, Row row, Col column) {
		if(object != null && rowObjects.contains(row) && columnObjects.contains(column)) {
			// 	This is straightforward. All cells should be set with an empty Object at least. Just place the new Object in the right cell.
			table.get(rowObjects.indexOf(row)).set(columnObjects.indexOf(column), object);
		}
	}
	
	/**
	 * Get the content of the cell specified by the row and col objects.
	 * @param row
	 * @param column
	 * @return
	 * @throws IndexOutOfBoundsException when either row or col object does not exist.
	 */
	public CellObject getCell(Row row, Col column) throws IndexOutOfBoundsException {
		if(rowObjects.contains(row) && columnObjects.contains(column)) {
			return table.get(rowObjects.indexOf(row)).get(columnObjects.indexOf(column));
		}
		throw new IndexOutOfBoundsException("[Table] Row or Col does not exist");
	}
	
	/**
	 * Clear the item from the cell specified by the row and column. Cannot be undone.
	 * @param row
	 * @param column
	 */
	public void clearCell(Row row, Col column) {
		if(rowObjects.contains(row) && columnObjects.contains(column)) {
			table.get(rowObjects.indexOf(row)).set(columnObjects.indexOf(column), new EmptyCellObject());
		}
	}
	
	/**
	 * This method returns a {@link Vector} with all the cells in the column of the given column Object.
	 * @param selectedItem The {@link Object} connected to the column that you want the items of.
	 * @return A {@link Vector} that contains all {@link Object}s in the selected column.
	 * 		   Iff the column Object is not a column, return null.
	 */
	public Vector<CellObject> getColumn(Col column) throws IndexOutOfBoundsException {
		if(columnObjects.contains(column)) {
			Vector<CellObject> returnVector = new Vector<CellObject>();
			int columnId = columnObjects.indexOf(column);
			for(Vector<CellObject> row : table) {
				returnVector.add(row.get(columnId));
			}
			return returnVector;
		}
		throw new IndexOutOfBoundsException("[Table] Col does not exist");
	}
	
	/**
	 * Method that returns a {@link Vector} that contains all the elements at the row of the given object at the parameter.
	 * @param row The {@link Object} that corresponds to the row that contains the wanted elements.
	 * @return A {@link Vector} that contains all the {@link Object}s at the selected row.
	 * 			Iff object is not a row, return value is null.
	 */
	public Vector<CellObject> getRow(Row row) throws IndexOutOfBoundsException {
		if(rowObjects.contains(row)) {
			return table.get(rowObjects.indexOf(row));
		}
		throw new IndexOutOfBoundsException("[Table] Row does not exist");
	}
	
	/**
	 * Check weather the item at the asked cell (row,col) is empty
	 * @param row the row of the item
	 * @param col the col of the item
	 * @return true iff item is empty
	 * @throws IndexOutOfBoundsException iff row or col do not exist
	 */
	public boolean isEmpty(Row row, Col col) throws IndexOutOfBoundsException {
		if(rowObjects.contains(row) && columnObjects.contains(col)) {
			boolean isNull = table.get(rowObjects.indexOf(row)).get(columnObjects.indexOf(col)) == null;
			boolean isCellObject = table.get(rowObjects.indexOf(row)).get(columnObjects.indexOf(col)) instanceof EmptyCellObject;
			
			return isNull || isCellObject;
		}
		throw new IndexOutOfBoundsException("[Table] Row or Col does not exist");
	}
	
	/**
	 * Method that returns the table in {@link Vector}<Vector<Object>> table.
	 * @return All elements contained in the {@link Table} are returned in Vector form.
	 * @author sdentro
	 */
	public Vector<Vector<CellObject>> getVectorTable() {
		return table;
	}
	
	public Vector<Row> getRowObjects() {
		return rowObjects;
	}
	
	public Vector<Col> getColumnObjects() {
		return columnObjects;
	}
	
	/**
	 * Print the contents of the table on screen.
	 * @author sdentro
	 */
	public void printTable() {
		for(Vector<CellObject> row : table) {
			System.out.print(table.indexOf(row) + "\t");
			Iterator<CellObject> it = row.iterator(); 
			while(it.hasNext()) {
				System.out.print(it.next() + "\t");
			}
			System.out.print("\n");
		}
	}
}
