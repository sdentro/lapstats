package code.backend.control;

/**
 * @author sdentro
 * @version $Id$
 */

import code.gui.main.StatusBarPanel;

public class TalkToGui {

	private StatusBarPanel statusBar;
	
	public TalkToGui() {
	
	}
	
	public void setStatusBar(StatusBarPanel statusBar) {
		this.statusBar = statusBar;
	}
	
	public void setStatusBarMessage(String message) {
		if(statusBarExists()) {
			statusBar.setMessage(message);
		}
	}

	private boolean statusBarExists() {
		return statusBar != null;
	}
}
