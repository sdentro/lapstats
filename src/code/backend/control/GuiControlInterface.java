package code.backend.control;

/**
 * @author sdentro
 * @version $Id$
 */

import java.io.File;
import java.util.Hashtable;
import java.util.Vector;

import code.backend.data.Car;
import code.backend.data.Track;
import code.backend.data.TrackSet;
import code.backend.data.User;

public interface GuiControlInterface {
	
	public Vector<String> sort(Vector<String> vector);
	
	public TrackSet getTrackSet(String set);
	public Car getCar(String name);
	public Track getTrack(String name);
	public User getUser(String userName);
	
	public void addUser(String userName);
	public void removeUser(User user);
	public void addTrackSet(String trackSet);
	public void removeTrackSetAndAllReferences(String trackSet);
	
	public boolean containsUser(String userName);
	public boolean containsTrackSet(String setName);
	
	public void saveTrackSet(String trackSetName, Vector<String> trackNames);
	public void saveCar(String carIdentifier, String carName, String carClass);
	public void saveModifiedTrack(String trackIdentifier, String trackName, String trackSetNames, Hashtable<String, String> newRefTimes);
	
	public Vector<String> getAllCarNames();
	public Vector<String> getAllTrackSetNames();
	public Vector<String> getAllUserNames();
	public Vector<String> getAllTrackNames();
	
	public Vector<Vector<String>> getTrackSetsTableData(String userName, String trackSetName, String carName);
	public Vector<Vector<String>> getTracksTableData(String trackName);
	public Vector<Vector<String>> getCarsTableData(String carName);
	public Vector<Vector<String>> getHandicapsTableData(String trackSet);
	
	public int getNumberOfLapsWithCar(String carName);
	public int getNumberOfLapsAtTrack(String trackName);

	public String getCurrentlyActiveUserName();
	public void setCurrentlyActiveUserName(String userName);
	public String[] getCarClasses();
	
	public void importSettings(File file);
	public void exportSettings(File file);
	public void loadCareerOfAllUsers();
	public void saveDataToFile();
	public void readCareerFromFile(File file);
	public void readCareerOfCurrentlyActiveUser();
	
	public String getTotalLaptimes(String userName);
	public String getTotalCarsDriven(String userName);
	public String getTotalLaps(String userName);
	public Vector<Vector<String>> getTrackNamesAndLaps(String userName);
	public Vector<Vector<String>> getCarNamesAndLaps(String userName); 
	
	public boolean getFirstTime();
}
