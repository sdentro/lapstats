package code.backend.control;

/**
 * @author sdentro
 * @version $Id$
 */

import java.util.Vector;

import code.backend.DataController;
import code.backend.data.Car;
import code.backend.data.Id;
import code.backend.data.Track;
import code.backend.data.TrackSet;
import code.backend.data.User;
import code.backend.data.UserCar;

public abstract class AbstractSimpleControlMethods {

	protected DataController dc;
	protected String[] carClasses; // TODO trouble here is that every subclass has it's own of these... and the creation of the guicontrol, normal control and setting the carclasses is virtually impossible to manage this way
//	private boolean lapstatsRunForfirstTime = false;
	
	public Vector<String> getAllCarNames() {
		return dc.getListOfCarNames();
	}

	public Vector<String> getAllTrackNames() {
		return dc.getListOfTrackNames();
	}

	public Vector<String> getAllTrackSetNames() {
		return dc.getListOfSetNames();
	}

	public Vector<String> getAllUserNames() {
		return dc.getListOfUserNames();
	}

	public Vector<Id> getAllCarIds() {
		return dc.getListOfCarIds();
	}

	public Vector<Id> getAllTrackIds() {
		return dc.getListOfTrackIds();
	}

	public Car getCar(Id id) {
		return dc.getCar(id);
	}

	public Id getCarId(String carName) {
		return dc.getCarId(carName);
	}

	public Track getTrack(Id id) {
		return dc.getTrack(id);
	}

	public Id getTrackId(String trackName) {
		return dc.getTrackId(trackName);
	}

	public User getUser(String userName) {
		return dc.getUser(userName);
	}

	public TrackSet getTrackSet(String set) {
		return dc.getTrackSet(set);
	}
	
	protected UserCar getUserCar(String userName, Id carId) {
		return getUser(userName).getUserCar(getCar(carId));
	}
	
	
	public boolean containsTrackSet(String setName) {
		return dc.containsTrackSet(setName);
	}
	
	public boolean containsUser(String userName) {
		return dc.containsUser(userName);
	}

	public boolean containsCar(Id carId) {
		return dc.containsCar(carId);
	}
	
	/**
	 * Add a user to the sytem.
	 * @param user the {@link User} that needs to be added to the system.
	 * @author sdentro
	 */
	public void addUser(User user) {
		if(!dc.containsUser(user)) {
			// Username is new, adding a new user
			dc.addUser(user);
		
			addCarsToUser(user);
			addTracksToUser(user);
			addTrackSetsToUser(user);
		}
		else {
			// Username exists. Fetch the object and store the new user in it
			User userObject = dc.getUser(user.getName());
			userObject.setCareerLocation(user.getCareerLocation());
			userObject.setLoadCareerOnStartup(user.getLoadCareerOnStartup());
			userObject.setSaveDataOnExit(user.getSaveDataOnExit());
			userObject.setSelectedUser(user.isSelectedUser());
		}
	}
	
	private void addTracksToUser(User user) {
		for(Id trackId : getAllTrackIds()) { 
			user.addTrack(getTrack(trackId)); 
		}
	}
	
	private void addCarsToUser(User user) {
		for(Id carId : getAllCarIds()) { 
			user.addCar(getCar(carId)); 
		}
	}
	
	private void addTrackSetsToUser(User user) {
		for(String trackSetName : getAllTrackSetNames()) { 
			user.addSet(getTrackSet(trackSetName)); 
		}
	}
	
	/**
	 * Add a trackSet to the system.
	 * @param trackSet the name of the trackSet that needs to be added.
	 * @author sdentro
	 */
	public void addTrackSet(String trackSetName) {
		dc.addTrackSet(trackSetName);
	}
	
	/**
	 * Saves a trackSet to the system. It takes care of adding a the trackset to the track and vice versa.
	 * @param trackSetName: String name of the set.
	 * @param trackNames: Vector containing id's of the tracks.
	 */
	public void saveTrackSet(String trackSetName, Vector<String> trackNames) {
		dc.saveTrackSet(trackSetName, trackNames);
	}

	protected void removeUser(String userName) {
		dc.removeUser(userName);
	}
	
	protected void saveModifiedCar(Car car) {
		dc.saveModifiedCar(car);
	}
	
	/**
	 * Method that checks whether there are more than one users. There 
	 * must always be one user, this one checks if there are more than
	 * that.
	 * @return Returns <code>true</code> iff there are more than one users.
	 */
	public boolean thereAreOtherUsers() {
		return dc.getNumberOfUsers() > 1;
	}
	
	public String[] getCarClasses() {
		return carClasses;
	}
	
//	public void setFirstTime() {
//		lapstatsRunForfirstTime = true;
//	}
//	
//	public boolean getFirstTime() {
//		return lapstatsRunForfirstTime;
//	}
	
	public void setCarClasses(String[] carClasses) {
		this.carClasses = carClasses;
	}
}
