package code.backend.control;

/**
 * @author sdentro
 * @version $Id$
 */

import java.util.HashSet;
import java.util.Hashtable;
import java.util.Vector;
import java.util.Map.Entry;

import code.backend.DataController;
import code.backend.data.Car;
import code.backend.data.Id;
import code.backend.data.Track;
import code.backend.data.TrackSet;
import code.backend.data.User;
import code.backend.data.UserCar;
import code.backend.data.UserTrack;

public class Control extends AbstractSimpleControlMethods {
	
	//[FirstStartup]
	
//	private StatusBarPanel statusBar;
	private GuiControl guiControl;
	
	private boolean lapstatsRunForfirstTime = false;
	
	public static final Vector<String> CARS_TABLE_HEADERS = new Vector<String>();
	public static final Vector<String> TRACKS_TABLE_HEADERS = new Vector<String>();
	public static final Vector<String> USERS_TABLE_HEADERS = new Vector<String>();
	public static final Vector<String> HANDICAPS_TABLE_HEADERS = new Vector<String>();
	public static final Vector<String> TRACKSETS_TABLE_HEADERS = new Vector<String>();
	public static final Vector<String> CARS_LAPS_TABLE_HEADERS = new Vector<String>();
	public static final Vector<String> TRACKS_LAPS_TABLE_HEADERS = new Vector<String>();
	
	public Control(DataController dataControl) {
		dc = dataControl;
		createStaticTableHeaders();
	}
	
	public void setCurrentlyActiveUserName(String userName) {
		guiControl.setCurrentlyActiveUserName(userName);
	}
	
	public void setGuiControl(GuiControl guiControl) {
		this.guiControl = guiControl;
	}
	
//	public void setStatusBar(StatusBarPanel statusBar) {
//		this.statusBar = statusBar;
//	}
	
	public void setFirstTime() {
		lapstatsRunForfirstTime = true;
	}
	
	public boolean getFirstTime() {
		return lapstatsRunForfirstTime;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Diverse set of utility methods
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		/**
		 * Build the static table headers for the GUI
		 * @author sdentro
		 */
		protected void createStaticTableHeaders() {
			CARS_TABLE_HEADERS.add("User");
			CARS_TABLE_HEADERS.add("Track");
			CARS_TABLE_HEADERS.add("Laptime");
			CARS_TABLE_HEADERS.add("Laps");
			CARS_TABLE_HEADERS.add("Difference");
			
			TRACKS_TABLE_HEADERS.add("User");
			TRACKS_TABLE_HEADERS.add("Car");
			TRACKS_TABLE_HEADERS.add("Laptime");
			TRACKS_TABLE_HEADERS.add("Laps");
			TRACKS_TABLE_HEADERS.add("Difference");
			
			USERS_TABLE_HEADERS.add("Car");
			USERS_TABLE_HEADERS.add("Track");
			USERS_TABLE_HEADERS.add("Laptime");
			USERS_TABLE_HEADERS.add("Laps");
			USERS_TABLE_HEADERS.add("Difference");
			
			HANDICAPS_TABLE_HEADERS.add("User");
			HANDICAPS_TABLE_HEADERS.add("Car");
			HANDICAPS_TABLE_HEADERS.add("Class");
			HANDICAPS_TABLE_HEADERS.add("Handicap");
			
			TRACKSETS_TABLE_HEADERS.add("Track");
			TRACKSETS_TABLE_HEADERS.add("Laptime");
			TRACKSETS_TABLE_HEADERS.add("Difference");
			
			CARS_LAPS_TABLE_HEADERS.add("Car");
			CARS_LAPS_TABLE_HEADERS.add("Laps");
			
			TRACKS_LAPS_TABLE_HEADERS.add("Track");
			TRACKS_LAPS_TABLE_HEADERS.add("Laps");
		}
		
		/**
		 * Method that converts a read laptime from file to a human readable one.
		 * Example: non humanreadable = 86.569 => humanreadable = 1:26.569
		 * @param nonHumanreadableLaptime The laptime read from file in non humanreadable format.
		 * @return A human readable laptime.
		 */
		public String convertToHumanreadableLaptime(String nonHumanreadableLaptime) {
			
			double laptime = Double.parseDouble(nonHumanreadableLaptime);
			int nonHumanreadableSeconds = (int) laptime;
			
			String humanreadableLaptime = "";
			if(nonHumanreadableSeconds > 0) {
				int humanreadableMinutes = convertMinutes(nonHumanreadableSeconds);
				String humanreadableSeconds = convertSeconds(nonHumanreadableSeconds, humanreadableMinutes, laptime);
				humanreadableLaptime = humanreadableMinutes + ":" + humanreadableSeconds;
			}
			return buildLaptimeFormat(humanreadableLaptime, nonHumanreadableLaptime);
		}
		
		private String buildLaptimeFormat(String humanreadableLaptime, String nonHumanreadableLaptime) {
			char[] charArray = humanreadableLaptime.toCharArray();
			if(charArray.length >= 8) {
				return stripOffExcessDigits(charArray);
			}
			if(charArray.length < 8) {
				return addExtraDigits(new String(charArray));
			}
			else {
				return nonHumanreadableLaptime;
			}
		}

		private int convertMinutes(int nonHumanreadableSeconds) {
			final int SECONDS_IN_MINUTE = 60;
			int humanreadableMinutes = 0;
			while(nonHumanreadableSeconds >= SECONDS_IN_MINUTE) {
				// While nonHumanreadableMinutes is larger or equal to 60, there's another minute in it
				nonHumanreadableSeconds = nonHumanreadableSeconds - SECONDS_IN_MINUTE;
				humanreadableMinutes++;
			}
			return humanreadableMinutes;
		}
		
		private String convertSeconds(int nonHumanreadableSeconds, int humanreadableMinuts, double laptime) {
			nonHumanreadableSeconds = nonHumanreadableSeconds - 60*humanreadableMinuts;
			final int TENTHS_IN_A_SECOND = 10;
			String humanreadableSeconds = "";
			if(nonHumanreadableSeconds < TENTHS_IN_A_SECOND) {
				humanreadableSeconds = 0 + "" + (nonHumanreadableSeconds + (laptime%((int) laptime)));
			}
			else {
				humanreadableSeconds = "" + (nonHumanreadableSeconds + (laptime%((int) laptime)));
			}
			return humanreadableSeconds;
		}
		
		/**
		 * Function that takes a char[] representation of a laptime and returns a string of the first 8 chars.
		 * @param humanreadableLaptime
		 * @return
		 */
		private String stripOffExcessDigits(char[] humanreadableLaptime) {
			String returnValue = "";
			// For this use only the first 8 characters are wanted
			for(int i=0; i<8; i++) { 
				returnValue = returnValue + humanreadableLaptime[i]; 
			}
			return returnValue;
		}
		
		/**
		 * A human readable laptime should consist of 8 digits. This function appends a zero
		 * 	for every missing digit.
		 * @param humanreadableLaptime A String containing a laptime with not enough digits.
		 * @return A fully populated laptime.
		 */
		private String addExtraDigits(String humanreadableLaptime) {
			int numberOfMissingDigits = 8 - humanreadableLaptime.length();
			for(int i=0; i<numberOfMissingDigits; i++) {
				humanreadableLaptime = humanreadableLaptime + "0";
			}
			return humanreadableLaptime;
		}
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Simple Setters	
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			public void setLaptime(String userName, String carIdentifier, String trackIdentifier, String time, String nonHumanReadableTime, int numberOfLaps) {
				Id carId = new Id(carIdentifier);
				Id trackId = new Id(trackIdentifier);
				
				if(containsUser(userName)) {
					Car car = getCar(carId);
					Track track = getTrack(trackId);
					User user = getUser(userName);
					user.addLaptime(car, track, time, nonHumanReadableTime, numberOfLaps);
				}
			}
		
		
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Add and remove methods
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
		/**
		 * Add a car to the system
		 * @param car the {@link Car} that needs to be added.
		 * @author sdentro
		 */
		public void addCar(Car car) {
			if(!containsCar(car.getId())) {
				dc.addCar(car);
				
				// Add it to every user's tables
				for(String userName : getAllUserNames()) {
					User user = getUser(userName);
					user.addCar(car);
				}
			}
		}	
		
		/**
		 * Add a track to the system.
		 * @param track the {@link Track} that needs to be added.
		 * @author sdentro
		 */
		public void addTrack(Track track) {
			if(!containsTrack(track)) {
				// Add all car classes to the Track
				track.addCarClasses(getCarClasses());
				
				dc.addTrack(track);
				
				for(String user : getAllUserNames()) {
					User curr = getUser(user);
					curr.addTrack(track);
				}
			}
		}
		


		/**
		 * Add a track to a set. This method will take care of everything.
		 * @param track is the specific track.
		 * @param sets is the set of set names that the track needs to be added to.
		 * @author sdentro
		 */
		public void addTrackToOnlyTheseTracksets(Track track, HashSet<String> sets) {
			for(String setName : sets) {
				if(!containsTrackSet(setName)) {
					addTrackSet(setName);
				}
				getTrackSet(setName).addTrack(track);
			}
			removeTrackFromAllOtherSets(track, sets);
			track.addToSets(sets);
		}
		
		/**
		 * Remove the given track from all tracksets not specified at the param. This means that the sets this track needs
		 * to be included in should be specified in the HashSet.
		 * @param track Track object.
		 * @param setsTrackShouldBeIn HashSet that contains the names of the TrackSets this track should be in
		 */
		private void removeTrackFromAllOtherSets(Track track, HashSet<String> setsTrackShouldBeIn) {
			HashSet<String> emptySets = new HashSet<String>();
			// Now walk through all sets and remove the track from where it shouldn't be
			for (String trackSetName : getAllTrackSetNames()) {
				TrackSet currentTrackSet = getTrackSet(trackSetName);
				// If the current set we're looking at contains the track, but the track should not be in the current set, remove it!
				if (currentTrackSet.contains(track) && !setsTrackShouldBeIn.contains(trackSetName)) {
					currentTrackSet.removeTrack(track);
					// If the trackSet is empty, remove it all together
					if (currentTrackSet.isEmpty()) {
						emptySets.add(trackSetName);
					}
				}
			}
			// Scan for empty sets that can be removed
			for (String s : emptySets) {
				removeTrackSetAndAllReferences(s);
			}
		}

		/**
		 * Remove a trackSet from the system.
		 * @param trackSet the name of the trackSet that needs to be removed.
		 * @author sdentro
		 */
		public void removeTrackSetAndAllReferences(String trackSet) {
			removeTrackSetFromAllUsers(trackSet);
			removeTrackSetFromAllTracks(trackSet);
			dc.removeTrackSet(trackSet);
		}
		
		/**
		 * Remove the trackSet from all users in the system
		 * @param trackSetName
		 */
		private void removeTrackSetFromAllUsers(String trackSetName) {
			for (String u : getAllUserNames()) {
				User user = getUser(u);
				user.removeSet(getTrackSet(trackSetName));
			}
		}
		
		/**
		 * Remove the trackSet from all Tracks in the system
		 * @param trackSetName
		 */
		private void removeTrackSetFromAllTracks(String trackSetName) {
			for (Id trackId : getAllTrackIds()) {
				Track track = getTrack(trackId); 
				if (track.isPartOfTrackSet(trackSetName)) {
					track.removeFromSet(trackSetName);
				}
			}
		}
		
		protected void removeTrackSet(String trackSet) {
			dc.removeTrackSet(trackSet);
		}
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Simple Getters	
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			public UserCar getUserCar(String userName, String carName) {
				if(checkForParamsNotNull(userName, carName)) {
					Id carId = getCarId(carName);
					return getUserCar(userName, carId);
				}
				//TODO catch this exception
				throw new NullPointerException("[AbstractControl] userName or carName null or no such user");
			}
			
			public UserTrack getUserTrack(String userName, String trackName) {
				if(checkForParamsNotNull(userName, trackName)) {
					Id trackId = getTrackId(trackName);
					return getUserTrack(userName, trackId);
				}
				//TODO catch this exception
				throw new NullPointerException("[AbstractControl] userName or trackName null or no such user");
			}
			
			protected UserTrack getUserTrack(String userName, Id trackId) {
				return getUser(userName).getUserTrack(getTrack(trackId));
			}

			
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// contains methods
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			public boolean containsCar(String carIdentifier) {
				return containsCar(new Id(carIdentifier));
			}
			
			public boolean containsTrack(Track track) {
				return dc.containsTrack(track);
			}
			
			public boolean containsTrack(Id trackId) {
				return dc.containsTrack(trackId);
			}
			
			public boolean containsTrack(String trackIdentifier) {
				return containsTrack(new Id(trackIdentifier));
			}
			
			protected boolean checkForParamsNotNull(Object...objects) {
				for(Object o : objects) {
					if(o == null) {
						return false;
					}
				}
				return true;
			}
		
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Modify data
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/**
		 * Save a modified track. This will also update the specific track for every user
		 * @param trackIdentifier
		 * @param trackName
		 * @param lodLocation
		 * @param trackSets
		 * @param newRefTimes
		 */
		public void saveModifiedTrack(String trackIdentifier, String trackName, HashSet<String> trackSets, 
				Hashtable<String, String> newRefTimes) {
			Id trackId = new Id(trackIdentifier);
			if (containsTrack(trackId)) {
				Track track = getTrack(trackId);
				track.setName(trackName);
				addTrackToOnlyTheseTracksets(track, trackSets);
				updateReferenceTimes(track, newRefTimes);
				recalculateDifferencesForAllUsersAtTrack(track);
				saveModifiedTrack(track);
			}
		}
		
		private void updateReferenceTimes(Track track, Hashtable<String, String> newRefTimes) {
			for (Entry<String, String> carClass : newRefTimes.entrySet()) {
				track.addReferenceTime(carClass.getKey(), carClass.getValue());
			}
		}

		private void recalculateDifferencesForAllUsersAtTrack(Track track) {
			for (String userName : getAllUserNames()) {
				User user = getUser(userName); 
				user.recalculateDifferences(track);
			}
		}
		
		/**
		 * Let's tell the DataController about the changed track.
		 * @param track
		 */
		private void saveModifiedTrack(Track track) {
			dc.saveModifiedTrack(track);
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Talk to GUI methods
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//		public void setStatusBarMessage(String message) {
//			if(statusBarExists()) {
//				statusBar.setMessage(message);
//			}
//		}
//
//		private boolean statusBarExists() {
//			return statusBar != null;
//		}
	}