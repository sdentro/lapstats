package code.backend.control;

/**
 * @author sdentro
 * @version $Id$
 */

import java.io.File;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Vector;

import code.backend.DataController;
import code.backend.FileProcessor;
import code.backend.data.Car;
import code.backend.data.Handicap;
import code.backend.data.Id;
import code.backend.data.Laptime;
import code.backend.data.Track;
import code.backend.data.TrackSet;
import code.backend.data.User;
import code.backend.data.UserCar;
import code.backend.data.UserTrack;
import code.backend.table.CellObject;

public class GuiControl extends AbstractSimpleControlMethods implements GuiControlInterface {

	private Control control;
	private String currentlyActiveUserName;
	private FileProcessor fileProcessor;
	private TalkToGui talkToGui;
	
	public GuiControl(Control control, DataController dc, FileProcessor fileProcessor, TalkToGui talkToGui) {
		this.control = control;
		this.dc = dc;
		this.fileProcessor = fileProcessor;
		this.talkToGui = talkToGui;
	}

	/**
	 * Add a user to the system.
	 * @param userName the name of the user that needs to be added.
	 * @author sdentro
	 */
	public void addUser(String userName) {
		addUser(new User(userName));
	}

	@Override
	public Car getCar(String name) {
		return getCar(getCarId(name));
	}

	@Override
	public String getCurrentlyActiveUserName() {
		return currentlyActiveUserName;
	}
	
	@Override
	public void setCurrentlyActiveUserName(String userName) {
		if (aUserWasActiveBefore()) {
			User oldUser = control.getUser(currentlyActiveUserName);
			oldUser.setSelectedUser(false);
		}
		currentlyActiveUserName = userName;
		User newUser = control.getUser(userName);
		newUser.setSelectedUser(true);
	}
	
	private boolean aUserWasActiveBefore() {
		return currentlyActiveUserName != null && !currentlyActiveUserName.equals("");
	}

	@Override
	public int getNumberOfLapsAtTrack(String trackName) {
		Track track = getTrack(trackName);
		Id trackId = track.getId();
		int returnValue = 0;
		for(String userName : getAllUserNames()) {
			returnValue = returnValue + control.getUserTrack(userName, trackId).getNumberOfLaps();
		}
		return returnValue;
	}

	@Override
	public int getNumberOfLapsWithCar(String carName) {
		Id carId = getCarId(carName);
		int returnValue = 0;
		for(String userName : getAllUserNames()) {
			returnValue = returnValue + getUserCar(userName, carId).getNumberOfLaps();
		}
		return returnValue;
	}

	@Override
	public Track getTrack(String name) {
		return getTrack(getTrackId(name));
	}

	@Override
	public void loadCareerOfAllUsers() {
		for(String user : getAllUserNames()) {
			if(!getUser(user).getCareerLocation().equals("")) {
				fileProcessor.readCareer(getUser(user).getCareerLocation(), user);
			}
		}
	}
	
	public void loadCareerOfAllUsersAtStartup() {
		for(String user : getAllUserNames()) {
			if(getUser(user).getLoadCareerOnStartup()) {
				fileProcessor.readCareer(getUser(user).getCareerLocation(), user);
			}
		}
	}
	
	@Override
	public void importSettings(File file) {
		try {
			fileProcessor.importSettings(file.getAbsolutePath());
		}
		catch(NullPointerException e) {
			System.out.println("[Control] nullpointer exception in importSettings");
		}
	}
	
	@Override
	public void exportSettings(File file) {
		try {
			fileProcessor.exportSettings(file.getAbsolutePath());
		}
		catch(NullPointerException e) {
			System.out.println("[Control] nullpointer exception in exportSettings");
		}
	}

	/**
	 * Opens the file given at the param and will tell the file reader to read all the info inside
	 * @param file The {@link File} that needs to be opened 
	 */
	@Override
	public void readCareerFromFile(File file) {
		// throws a nullpointer exception when cancel is pressed in the fileselector
		try {
			fileProcessor.readCareer(file.getAbsolutePath(), currentlyActiveUserName);
		}
		catch(NullPointerException e) {
			System.out.println("[Control] nullpointer exception in readCareerFromFile");
		}
	}

	/**
	 * Read a career from the specified location. The location needs to be a full path to the career.blt.
	 * @param careerLocation
	 */
	@Override
	public void readCareerOfCurrentlyActiveUser() {
		String userName = getCurrentlyActiveUserName();
		fileProcessor.readCareer(getUser(userName).getCareerLocation(), userName);
	}

	@Override
	public void removeTrackSetAndAllReferences(String trackSet) {
		control.removeTrackSetAndAllReferences(trackSet);
	}

	/**
	 * Remove the specified user from the system. User will not be removed if it is the 
	 * last user in the system. The number of laps of the User will be subtracted from
	 * the Cars and Tracks.
	 * @param user the user that needs to be removed
	 * @author sdentro
	 */
	@Override
	public void removeUser(User user) {
		if(thereAreOtherUsers()) {
			subtractNumberOfLapsFromCars(user);
			subtractNumberOfLapsFromTrack(user);

			removeUser(user.getName());
		}
	}
	
	private void subtractNumberOfLapsFromCars(User user) {
		for(Id carId : getAllCarIds()) {
			Car c = getCar(carId);
			UserCar uCar = user.getUserCar(c);
			c.subtractNumberOfLaps(uCar.getNumberOfLaps());
		}
	}
	
	private void subtractNumberOfLapsFromTrack(User user) {
		for(Id trackId : getAllTrackIds()) {
			Track t = getTrack(trackId);
			UserTrack uTrack = user.getUserTrack(t);
			t.subtractNumberOfLaps(uTrack.getNumberOfLaps());
		}
	}

	
	/**
	 * Save a car in the backend. Call this method when a Car has changed
	 * @param carName the name of the Car
	 * @param carClass the class of the Car
	 * @author sdentro
	 */
	@Override
	public void saveCar(String carIdentifier, String carName, String carClass) {
		Id carId = new Id(carIdentifier);
		if (containsCar(carId)) {

			Car car = getCar(carId);
			car.setName(carName);
			car.setToClass(carClass);
			
			recalculateDifferencesForAllUsersForCar(car);
			saveModifiedCar(car);
		}
	}
	
	private void recalculateDifferencesForAllUsersForCar(Car car) {
		for(String userName : getAllUserNames()) {
			User user = getUser(userName);
			user.recalculateDifferences(car);
		}
	}

	@Override
	public void saveDataToFile() {
		fileProcessor.writeData();
	}

	@Override
	public void saveModifiedTrack(String trackIdentifier, String trackName,
			String trackSetNames, Hashtable<String, String> newRefTimes) {
		control.saveModifiedTrack(trackIdentifier, trackName, convertSetnamesToHashSet(trackSetNames), newRefTimes);
	}
	
	/**
	 * Convert a String containing a list of set names into a HashSet containing all the set names in String form.
	 * @param trackSets contains the names of the sets, all separated by ", " (comma space).
	 * @return A set containing all set names included in the original String given at the param.
	 */
	private HashSet<String> convertSetnamesToHashSet(String trackSetNames) {
		HashSet<String> returnSet = new HashSet<String>();
		String split[] = trackSetNames.split(",");
		
		for (String s : split) {
			returnSet.add(s);
		}
		return returnSet;
	}

	/**
	 * Bubble sort a {@link Vector} of {@link String}s. It uses the compareTo function of the Strings to distinguish. Elements a and b get swapped if a.compareTo(b) > 0.
	 * @param vector The Vector containing Strings that needs to be sorted.
	 * @return A sorted vector.
	 * @author sdentro
	 */
	@Override
	public Vector<String> sort(Vector<String> vector) {
		if(vector.size() > 1) {
			boolean swapped = true;
			while(swapped) {
				swapped = false;
			
				for(int i=0; i<vector.size()-1;i++) {
					if(vector.elementAt(i).compareTo(vector.elementAt(i+1)) > 0) {
						String temp = vector.elementAt(i);
					
						vector.set(i, vector.elementAt(i+1));
						vector.set(i+1, temp);
					
						swapped = true;
					}
				}
			}
		}
		return vector;
	}

	
	/**
	 * Get the data for the TrackSets tab.
	 * @param userName The name of the specific user
	 * @param trackSetName Name of the TrackSet
	 * @param carName Name of the Car
	 * @return A Vector<Vector<String>> table containing for each track element of the specific
	 * TrackSet for the specific car: the track name, the laptime and the difference.
	 */
	@Override
	public Vector<Vector<String>> getTrackSetsTableData(String userName, String trackSetName, String carName) {
		Vector<Vector<String>> returnTable = new Vector<Vector<String>>();
		if(checkForParamsNotNull(userName, trackSetName, carName)) {
			TrackSet ts = getTrackSet(trackSetName);
			Car c = getCar(carName);
		
			Vector<String> trackSetTracks = ts.getTrackNames();
			User user = getUser(userName);
			Vector<CellObject> times = user.getCarLaptimes(c);
			for(CellObject time : times) {
				if(time instanceof Laptime) {
					Laptime lptm = (Laptime) time;
					
					if(trackSetTracks.contains(lptm.getTrackName())) {
						Vector<String> newLaptime = new Vector<String>();
						
						newLaptime.add(lptm.getTrackName());
						newLaptime.add(lptm.getTime());
						newLaptime.add(""+lptm.getDifference());
						
						returnTable.add(newLaptime);
						trackSetTracks.remove(lptm.getTrackName());
					}
				}
			}
			
			for(String remainingTrackName : trackSetTracks) {
				Vector<String> newLaptime = new Vector<String>();
				
				newLaptime.add(remainingTrackName);
				Track remainingTrack = getTrack(remainingTrackName);
				Car car = getCar(carName);
				newLaptime.add(remainingTrack.getReferenceTime(car.getCarClass()));
				newLaptime.add("");
				
				returnTable.add(newLaptime);
			}
		}
		
		return returnTable;			
	}
	
	protected boolean checkForParamsNotNull(Object...objects) {
		for(Object o : objects) {
			if(o == null) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Method that fetches all laptimes driven at the track name given at the param.
	 * @param trackName Name of the track that you want the laptimes of.
	 * @return A {@link Vector} that contains tuples of <User, Car and Laptime>.
	 */
	@Override
	public Vector<Vector<String>> getTracksTableData(String trackName) {
		Vector<Vector<String>> returnTable = new Vector<Vector<String>>();
		
		if(checkForParamsNotNull(trackName)) {
			for(String userName : getAllUserNames()) {
			
				Track selectedTrack = getTrack(trackName);
			
				User user = getUser(userName);
				Vector<CellObject> times = user.getTrackLaptimes(selectedTrack);
				for(CellObject lt : times) {
					if(lt instanceof Laptime) {
						Vector<String> newLaptime = new Vector<String>();
						Laptime lptm = (Laptime) lt;
						
						newLaptime.add(userName);
						newLaptime.add(lptm.getCarName());
						newLaptime.add(lptm.getTime());
						newLaptime.add(""+lptm.getNumberOfLaps());
						newLaptime.add(""+lptm.getDifference());
						
						returnTable.add(newLaptime);
					}
				}
			}
		}
		return returnTable;
	}

	/**
	 * Method that fetches all laptimes driven by the car name given at the param.
	 * @param carName Name of the car that you want the laptimes of.
	 * @return A {@link Vector} that contains tuples of <User, Track and Laptime>.
	 */
	@Override
	public Vector<Vector<String>> getCarsTableData(String carName) {
		Vector<Vector<String>> returnTable = new Vector<Vector<String>>();
		
		if(checkForParamsNotNull(carName)) {
			for(String userName : getAllUserNames()) {
				Car selectedCar = getCar(carName);
				
				User user = getUser(userName);
				Vector<CellObject> times = user.getCarLaptimes(selectedCar);
				for(CellObject time : times) {
					if(time instanceof Laptime) {
						Vector<String> newLaptime = new Vector<String>();
						Laptime lptm = (Laptime) time;
						
						newLaptime.add(userName);
						newLaptime.add(lptm.getTrackName());
						newLaptime.add(lptm.getTime());
						newLaptime.add(""+lptm.getNumberOfLaps());
						newLaptime.add(""+lptm.getDifference());
						
						returnTable.add(newLaptime);
					}
				}
			}
		}
		return returnTable;
	}
	
	/**
	 * Fetch all handicaps for the selected trackSet.
	 * @param trackSet
	 * @return a table containing all the handicaps driven for this set
	 * @author sdentro
	 */
	@Override
	public Vector<Vector<String>> getHandicapsTableData(String trackSet) {
		Vector<Vector<String>> returnValue = new Vector<Vector<String>>();
		
		if (checkForParamsNotNull(trackSet)) {
			// Fetch handicaps from every user
			for (String userName : getAllUserNames()) {
				User user = getUser(userName);
				TrackSet set = getTrackSet(trackSet);
				
				// Fetch handicaps for every car for this trackset
				for (Id carId : getAllCarIds()) {
					Vector<String> newHandicap = new Vector<String>();
					Car car = getCar(carId);
					Handicap handicap = user.getHandicap(car, set);
						
					// getHandicap returns null iff there is no Handicap set for this car in this trackSet
					if (handicap != null) {
						newHandicap.add(userName);
						newHandicap.add(car.getName());
						newHandicap.add(car.getCarClass());
						newHandicap.add(handicap.getHandicap());
							
						returnValue.add(newHandicap);
					}
				}
			}
		}
		return returnValue;
	}
	
	@Override
	public boolean getFirstTime() {
		return control.getFirstTime();
	}

	/**
	 * Returns the total number of laptimes driven by the user
	 * specified at the param.
	 * @param a valid username
	 */
	@Override
	public String getTotalLaptimes(String userName) {
		return ""+dc.getTotalLaptimes(userName);
	}
	
	/**
	 * Returns the total number of cars driven by the user
	 * specified at the param.
	 * @param a valid username
	 */
	@Override
	public String getTotalCarsDriven(String userName) {
		return ""+dc.getTotalCarsDriven(userName);
	}
	
	/**
	 * Returns the total number of laps driven by the user
	 * specified at the param.
	 * @param a valid username
	 */
	@Override
	public String getTotalLaps(String userName) {
		return ""+dc.getTotalLaps(userName);
	}

	@Override
	public Vector<Vector<String>> getCarNamesAndLaps(String userName) {
		return dc.getCarNamesAndLaps(userName);
	}

	@Override
	public Vector<Vector<String>> getTrackNamesAndLaps(String userName) {
		return dc.getTrackNamesAndLaps(userName);
	}
}
