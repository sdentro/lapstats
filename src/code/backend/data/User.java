package code.backend.data;

/**
 * @author sdentro
 * @version $Id$
 */

import java.util.HashSet;
import java.util.Hashtable;
import java.util.Vector;

import code.backend.table.CellObject;
import code.backend.table.Table;

public class User extends LapstatsObject {

	/*
	 * Een gebruiker heeft:
	 *  1) Per car een UserCar
	 *  2) per track een UserTrack
	 *  3) per usercar/usertrack een mogelijke laptime
	 *  4) per trackset-usercar een mogelijke handicap
	 *  5) per trackset-carclass een mogelijke handicap
	 */
	private boolean isSelectedUser;
	private String careerLocation;
	private boolean loadCareerOnStartup;
	private boolean saveDataOnExit;
	private int numberOfLapsDriven;
	private int numberOfCarsDriven;
	private int numberOfTracksDriven;
	private Table<UserCar, UserTrack, Laptime> laptimeTable;
	private Table<UserCar, TrackSet, Handicap> carHandicapTable;
	private Table<String, TrackSet, Handicap> classHandicapTable;
	private Hashtable<Car, UserCar> userCarTable;
	private Hashtable<Track, UserTrack> userTrackTable;
	
	public User(String name) {
		super(name);
		careerLocation = "";
		laptimeTable = new Table<UserCar, UserTrack, Laptime>();
		carHandicapTable = new Table<UserCar, TrackSet, Handicap>();
		classHandicapTable = new Table<String, TrackSet, Handicap>();
		userCarTable = new Hashtable<Car, UserCar>();
		userTrackTable = new Hashtable<Track, UserTrack>();
		numberOfLapsDriven = 0;
		numberOfCarsDriven = 0;
		numberOfTracksDriven = 0;
	}
	
	public void addLaptime(Car car, Track track, String laptime, String nonHumanReadableLaptime, int numberOfLaps) {
		if(userCarTable.keySet().contains(car) && userTrackTable.keySet().contains(track)) {
			UserCar uCar = userCarTable.get(car);
			UserTrack uTrack = userTrackTable.get(track);
			updateNumberOfLaps(uCar, uTrack, numberOfLaps);
			updateNumberOfTracksAndCarsDriven(uCar, uTrack);
			laptimeTable.setCell(new Laptime(uCar, uTrack, laptime, nonHumanReadableLaptime, numberOfLaps), uCar, uTrack);
			if(!car.getCarClass().equals("")) {
				calculateCarHandicaps(car, uTrack.getTrack().getTracksets());
			}
			
		}
		else {
			throw new IndexOutOfBoundsException("Track "+track.getName()+" or Car "+car.getName()+" not found for user "+getName());
		}
	}
	
	/**
	 * Set this laptime in the laptime table for this user. The laptime contains all info needed for storage
	 * @param laptime
	 */
	public void setLaptime(Laptime laptime) {
		if(userCarTable.keySet().contains(laptime.getUserCar().getCar()) && 
				userTrackTable.keySet().contains(laptime.getUserTrack().getTrack())) {
			updateNumberOfLaps(laptime.getUserCar(), laptime.getUserTrack(), laptime.getNumberOfLaps());
			laptimeTable.setCell(laptime, laptime.getUserCar(), laptime.getUserTrack());
			recalculateHandicaps();
		}
	}
	
	private void updateNumberOfLaps(UserCar uCar, UserTrack uTrack, int newNumberOfLaps) {
		CellObject cell = laptimeTable.getCell(uCar, uTrack);
		if(previousLaptimeExists(cell)) {
			Laptime lptm = (Laptime) cell;
			
			int difference = newNumberOfLaps - lptm.getNumberOfLaps();
			
			uCar.addNumberOfLaps(difference);
			uCar.getCar().addNumberOfLaps(difference);
			
			uTrack.addNumberOfLaps(difference);
			uTrack.getTrack().addNumberOfLaps(difference);
			
			numberOfLapsDriven += difference;
		}
		else {
			uCar.addNumberOfLaps(newNumberOfLaps);
			uCar.getCar().addNumberOfLaps(newNumberOfLaps);
			
			uTrack.addNumberOfLaps(newNumberOfLaps);
			uTrack.getTrack().addNumberOfLaps(newNumberOfLaps);
			
			numberOfLapsDriven += newNumberOfLaps;
		}
	}
	
	private void updateNumberOfTracksAndCarsDriven(UserCar uCar, UserTrack uTrack) {
		if(!hasLaptimeAtThisTrack(uTrack)) {
			numberOfTracksDriven++;
		}
		if(!hasLaptimeWithThisCar(uCar)) {
			numberOfCarsDriven++;
		}
	}
	
	private boolean hasLaptimeAtThisTrack(UserTrack uTrack) {
		Vector<CellObject> col = laptimeTable.getColumn(uTrack);
		for(CellObject c : col) {
			if(c instanceof Laptime) {
				return true;
			}
		}
		return false;
	}
	
	private boolean hasLaptimeWithThisCar(UserCar uCar) {
		Vector<CellObject> row = laptimeTable.getRow(uCar);
		for(CellObject c : row) {
			if(c instanceof Laptime) {
				return true;
			}
		}
		return false;
	}
	
	private boolean previousLaptimeExists(CellObject cell) {
		return cell instanceof Laptime;
	}

	private String calculateCarHandicap(UserCar uCar, TrackSet trackSet) {
		double returnValue = 0.0;

		for(Track track : trackSet.getTracks()) {
			if(laptimeTable.getCell(uCar, userTrackTable.get(track)) instanceof Laptime) {
				Laptime uCaruTrackLaptime = (Laptime) laptimeTable.getCell(uCar, userTrackTable.get(track));
				returnValue += Double.parseDouble(uCaruTrackLaptime.getDifference());
			}
			else {
				returnValue = 0.0;
				break;
			}
		}
		return stripOffExcessDigits(returnValue);
	}
	
	/**
	 * Recalculate all handicaps. Call this method when a Car or Track has changed
	 */
	private void recalculateHandicaps() {
		HashSet<String> trackSets = new HashSet<String>();
		trackSets.addAll(getAllTrackSetNames());
		for(Car c : userCarTable.keySet()) {
			if(!c.getCarClass().equals("")) {
				calculateCarHandicaps(c, trackSets);
			}
		}
	}
	
	private HashSet<String> getAllTrackSetNames() {
		HashSet<String> trackSets = new HashSet<String>();
		for(TrackSet tSet : carHandicapTable.getColumnObjects()) {
			trackSets.add(tSet.getName());
		}
		return trackSets;
	}
	
	/**
	 * Method that returns a vector containing all user specific UserTrack objects.
	 * @return A Vector containing UserTrack objects
	 */
	public Vector<UserTrack> getAllUserTracks() {
		return new Vector<UserTrack>(userTrackTable.values());
	}
	
	/**
	 * Method that returns a vector containing all user specific UserCar objects.
	 * @return A Vector containing UserCar objects
	 */
	public Vector<UserCar> getAllUserCars() {
		return new Vector<UserCar>(userCarTable.values());
	}
	
	/**
	 * Method that checks if a handicap can be calculated at this point for the given sets at the param. 
	 * If it is possible the handicap will be calculated and stored in the carHandicapTable.
	 * @param sets is a set containing the names of the sets.
	 */
	private void calculateCarHandicaps(Car car, HashSet<String> sets) {
		UserCar uCar = userCarTable.get(car);
		for (String trackSetName : sets) {
			TrackSet ts = getTrackSet(trackSetName);
			String handicap = calculateCarHandicap(uCar, ts);
			if (allTracksHaveLaptimes(handicap)) {
				carHandicapTable.setCell(new Handicap("" + handicap), uCar, ts);
			} 
			else {
				setEmptyHandicap(uCar, ts);
			}
		}
	}
	
	public TrackSet getTrackSet(String trackSetName) {
		for(TrackSet tSet : carHandicapTable.getColumnObjects()) {
			if(tSet.getName().equals(trackSetName)) {
				return tSet;
			}
		}
		throw new NullPointerException("[User] trackSet with name " + trackSetName + " not found in user " + getName());
	}

	private boolean allTracksHaveLaptimes(String handicap) {
		return !handicap.equals("0.000");
	}

	private void setEmptyHandicap(UserCar uCar, TrackSet ts) {
		carHandicapTable.clearCell(uCar, ts);
	}

	/**
	 * Add a car to this user's tables. Should be invoked when a new Car is added to the system
	 * @param car
	 */
	public void addCar(Car car) {
		UserCar uCar = new UserCar(this, car);
		userCarTable.put(car, uCar);
		laptimeTable.addRow(uCar);
		carHandicapTable.addRow(uCar);
	}
	
	/**
	 * Add a track to this user's tables. Should be invoked when a new Track is added to the system
	 * @param track
	 */
	public void addTrack(Track track) {
		UserTrack uTrack = new UserTrack(this, track);
		userTrackTable.put(track, uTrack);
		laptimeTable.addColumn(uTrack);
	}
	
	/**
	 * Add a set to this user's tables. Should be invoked when a new set is added to the system
	 * @param set
	 */
	public void addSet(TrackSet set) {
		carHandicapTable.addColumn(set);
		classHandicapTable.addColumn(set);
		recalculateHandicaps();
	}
	
	/**
	 * Remove a set from the user's tables. Should be invoked when a set is removed from the system
	 * @param set
	 */
	public void removeSet(TrackSet set) {
		carHandicapTable.removeColumn(set);
		classHandicapTable.removeColumn(set);
		recalculateHandicaps();
	}
	
	public String getCareerLocation() {
		return careerLocation;
	}
	
	public void setCareerLocation(String location) {
		careerLocation = location;
	}
	
	public Laptime getLaptime(Car car, Track track) {
		if(containsCarAndTrack(car, track)) {
			CellObject potentialLaptime = laptimeTable.getCell(userCarTable.get(car), userTrackTable.get(track));

			if(potentialLaptime instanceof Laptime) {
				return (Laptime) potentialLaptime;
			}
		}
		throw new NullPointerException("[User] no such Car or Track available or no laptime set for this pair");
	}
	
	private boolean containsCarAndTrack(Car car, Track track) {
		return userCarTable.containsKey(car) && userTrackTable.containsKey(track);
	}
	
	public Vector<CellObject> getTrackLaptimes(Track track) {
		return laptimeTable.getColumn(userTrackTable.get(track));
	}
	
	public Vector<CellObject> getCarLaptimes(Car car) {
		return laptimeTable.getRow(userCarTable.get(car)); 
	}
	
	public Handicap getHandicap(Car car, TrackSet set) {
		if(carHandicapTable.getCell(userCarTable.get(car), set) instanceof Handicap) 
			return (Handicap) carHandicapTable.getCell(userCarTable.get(car), set);
		else {
			return null;
		} 
//		throw new NullPointerException("[User] there is no handicap for the car at this trackset");
	}
	
	public UserCar getUserCar(Car car) {
		return userCarTable.get(car);
	}
	
	public UserTrack getUserTrack(Track track) {
		return userTrackTable.get(track);
	}
	
	
	public int getNumberOfTracksDriven() {
		return numberOfTracksDriven;
	}
	
	public int getNumberOfCarsDriven() {
		return numberOfCarsDriven;
	}
	
	public int getTotalNumberOfLapsDriven() {
		return numberOfLapsDriven;
	}

	/**
	 * Recalculate the differences when a Car is updated. Call this method when a Car is changed
	 * @param c
	 * @author sdentro
	 */
	public void recalculateDifferences(Car c) {
		Vector<CellObject> times = laptimeTable.getRow(userCarTable.get(c));
		recalculateDifferences(times);
		recalculateHandicaps();
	}

	/**
	 * Recalculate the differences when a Track is updated. Call this method when a Track is changed
	 * @param t
	 * @author sdentro
	 */
	public void recalculateDifferences(Track t) {
		Vector<CellObject> times = laptimeTable.getColumn(userTrackTable.get(t));
		recalculateDifferences(times);
		recalculateHandicaps();
	}
	
	/**
	 * Recalculate the differences for all Laptimes in the Vector given at the param.
	 * @param times a {@link Vector} containing CellObjects. The method will only update Laptime instances.
	 */
	private void recalculateDifferences(Vector<CellObject> times) {
		for(CellObject co : times) {
			if(co instanceof Laptime) {
				((Laptime) co).calculateDifference();
			}
		}
	}
	
	public void setSaveDataOnExit(boolean value) {
		saveDataOnExit = value;
	}
	
	public boolean getSaveDataOnExit() {
		return saveDataOnExit;
	}
	
	public void setLoadCareerOnStartup(boolean value) {
		loadCareerOnStartup = value;
	}
	
	public boolean getLoadCareerOnStartup() {
		return loadCareerOnStartup;
	}
	
	/**
	 * This method strips off the extra digits that sometimes get added when calculating with doubles.
	 * After running this method the String will consist of this format: (-) 
	 * @param value
	 * @return
	 */
	public String stripOffExcessDigits(double value) {
		char valueArray[] = new Double(value).toString().toCharArray();
		/*
		 *  First devide the param into two categories: the seconds (plus the "-" sign) and the
		 *  digits behind the decimal point. The main character of the double is of course the
		 *  point in between both the seconds and digits. That's how this for loop works.
		 */
		boolean pointReached = false;
		String originalSeconds = "";
		String originalDigits = "";
		for(char element : valueArray) {
			if(element == '.') 
				pointReached = true;
			else if(!pointReached) 
				originalSeconds += element;
			else 
				originalDigits += element;
		}
		return originalSeconds+"."+processDigits(originalDigits);
	}
	
	/**
	 * Strip off extra digits, this method will return a string containing three digits.
	 * @param originalDigits
	 * @return
	 */
	private String processDigits(String originalDigits) {
		/*
		 * Now let's get the digits ready. We want 3 digits, so there are 3 possibilities:
		 *  1) one digit, followed by nothing (add two zero's)
		 *  2) two digits, flowed by nothing (add one zero)
		 *  3) three or more digits (just select the first three digits)
		 */
		char[] digitsArray = (""+originalDigits).toCharArray();
		String newDigits = "";
		if(digitsArray.length == 1)
			newDigits = digitsArray[0]+"00";
		else if(digitsArray.length == 2)
			newDigits = digitsArray[0]+""+digitsArray[1]+"0";
		else
			newDigits = digitsArray[0]+""+digitsArray[1]+""+digitsArray[2];
		return newDigits;
	}
	
	public boolean isSelectedUser() {
		return isSelectedUser;
	}

	public void setSelectedUser(boolean selected) {
		isSelectedUser = selected;
	}

}
