package code.backend.data;

/**
 * @author sdentro
 * @version $Id$
 */

import java.util.HashSet;
import java.util.Hashtable;

public class Track extends CarTrackObject {

	private Hashtable<String, String> referenceTimes;
	private HashSet<String> sets;

	public Track(String name, String id) {
		super(name, id);
		sets = new HashSet<String>();
		referenceTimes = new Hashtable<String, String>();
	}
	
	public void addReferenceTime(String carClass, String time) {
		referenceTimes.put(carClass, time);
	}
	
	public Hashtable<String, String> getReferenceTimes() {
		return referenceTimes;
	}
	
	public String getReferenceTime(String carClass) {
		if (referenceTimes.containsKey(carClass))
			return referenceTimes.get(carClass);
		else
			return "NA";
	}
	
	public void addToSets(HashSet<String> sets) {
		this.sets = sets;
	}
	
	public String getSetsAsString() {
		String returnString = "";
		if (sets.size() == 1) {
			for (String s : sets) {
				returnString = s;
			}
		}
		else {
			for (String s : sets) {
				returnString = returnString + s + ",";
			}
		}
		return returnString;
	}
	
	public void addToSet(String set) {
		sets.add(set);
	}
	
	public void removeFromSet(String set) {
		sets.remove(set);
	}
	
	public HashSet<String> getTracksets() {
		return sets;
	}

	public void addCarClasses(String[] carClasses) {
		for (String carClass : carClasses) {
			if (!referenceTimes.containsKey(carClass))
				referenceTimes.put(carClass, "NA");
		}
	}
	
	public boolean isPartOfTrackSet(String trackSetName) {
		return sets.contains(trackSetName);
	}
}
