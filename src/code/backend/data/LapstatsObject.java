package code.backend.data;

/**
 * @author sdentro
 * @version $Id$
 */

public abstract class LapstatsObject {

	protected String name;
	
	public LapstatsObject(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	
	public boolean equals(Object that) {
		if (that instanceof LapstatsObject) {
			return getName().equals(((LapstatsObject) that).getName());
		}
		return false;
	}
	
	public int hashCode() {
		int hash = 7;
		// Calculate our own hashCode of identifier is empty (hashcode = 0). 
		// Otherwise use the one from the id
		hash = 31 * hash + (null == name ? 0 : name.hashCode());
		return hash;
	}
}
