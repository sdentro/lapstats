package code.backend.data;

/**
 * @author sdentro
 * @version $Id$
 */

import java.util.HashSet;
import java.util.Vector;

public class TrackSet extends LapstatsObject {

	private HashSet<Track> tracks;
	
	public TrackSet(String name, Track...tracks) {
		super(name);
		this.tracks = new HashSet<Track>();
		for(Track t : tracks) {
			this.tracks.add(t);
		}
	}
	
	public TrackSet(String name, Vector<Track> tracks) {
		super(name);
		this.tracks = new HashSet<Track>();
		this.tracks.addAll(tracks);
	}
	
	public HashSet<Track> getTracks() {
		return tracks;
	}
	
	public Vector<String> getTrackNames() {
		Vector<String> returnVector = new Vector<String>();
		for (Track track : tracks) {
			returnVector.add(track.getName());
		}
		return returnVector;
	}
	
	public void addTrack(Track track) {
		tracks.add(track);
	}
	
	public void removeTrack(Track track) {
		tracks.remove(track);
	}
	
	public boolean contains(Track t) {
		return tracks.contains(t);
	}
	
	public boolean isEmpty() {
		return tracks.isEmpty();
	}
}
