package code.backend.data;

/**
 * @author sdentro
 * @version $Id$
 */

public class UserCar {

	private final User user;
	private final Car car;
	private int numberOfLaps;
	
	public UserCar(User user, Car car) {
		this.user = user;
		this.car = car;
		numberOfLaps = 0;
	}
	public void addNumberOfLaps(int numberOfLaps) {
		this.numberOfLaps = this.numberOfLaps + numberOfLaps;
	}	
	
	public int getNumberOfLaps() {
		return numberOfLaps;
	}

	public User getUser() {
		return user;
	}

	public Car getCar() {
		return car;
	}
	
	public String getCarName() {
		return car.getName();
	}
	
	public String getUserName() {
		return user.getName();
	}
}
