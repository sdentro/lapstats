package code.backend.data;

/**
 * @author sdentro
 * @version $Id$
 */

import code.backend.table.CellObject;

public class Handicap extends CellObject {
	private String handicap;
	
	public Handicap(String value) {
		handicap = value;
	}
	
	public String getHandicap() {
		return handicap;
	}
}
