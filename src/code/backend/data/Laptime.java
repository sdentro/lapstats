package code.backend.data;

/**
 * @author sdentro
 * @version $Id$
 */

import code.backend.table.CellObject;

public class Laptime extends CellObject {

	private User user;
	private UserTrack track;
	private UserCar car;
	private String time;
	private String nonHumanReadableTime;
	private int numberOfLaps;
	private String difference;

	public Laptime(UserCar car, UserTrack track, String time, String nonHumanReadableTime, int numberOfLaps) {
		this.user = car.getUser();
		this.track = track;
		this.car = car;
		this.time = time;
		this.nonHumanReadableTime = nonHumanReadableTime;
		this.numberOfLaps = numberOfLaps;

		calcDiff(time, this.track.getTrack().getReferenceTime(this.car.getCar().getCarClass()));
	}

	public UserTrack getUserTrack() {
		return track;
	}
	
	public String getTrackName() {
		Track trk = track.getTrack();
		return trk.getName();
	}
	
	public UserCar getUserCar() {
		return car;
	}
	
	public String getCarName() {
		Car cr = car.getCar();
		return cr.getName();
	}

	public String getTime() {
		return time;
	}
	
	public int getNumberOfLaps() {
		return numberOfLaps;
	}
	
	public String getDifference() {
		return difference;
	}
	
	/**
	 * Calculate the difference between the laptime and the reference time. The calculated difference will be stored in 
	 * the diference field.
	 * @author sdentro
	 */
	public void calculateDifference() {
		calcDiff(time, this.track.getTrack().getReferenceTime(this.car.getCar().getCarClass()));
	}
	

	private void calcDiff(String laptime, String refTime) {
		/*
		 * Laptime: x:xx.xxx is 8 characters. 
		 * 			-x:xx.xxx is 9 characters.
		 * 
		 * First split off the minutes. Those can be calculated differently. Were doing laptime - reftime,
		 * so if the number of minutes in reftime are bigger than in laptime we'll get a negative number.
		 * 
		 * The seconds are a little more complicated. In both cases (end result both positive and negative)
		 * the number of seconds can be lower or higher than 10. In case of lower an extra zero needs to be
		 * added. Unfortunately I cannot think of a good way to do this, so some redundant code here. Also,
		 * when 
		 * 
		 *  Finally, since we're working with doubles, a long range of extra numbers are added behind the
		 *  decimal point. That's where the 8 or 9 comes into play. That number is used to strip of the 
		 *  excess numbers
		 */
		String returnValue = "";
		if(!laptime.equals("NA") && !refTime.equals("NA")) {
			String lt[] = laptime.split(":");
			String rt[] = refTime.split(":");

			int lt_mins = Integer.parseInt(lt[0]);
			int rt_mins = Integer.parseInt(rt[0]);
		
			double lt_secs = Double.parseDouble(lt[1]);
			double rt_secs = Double.parseDouble(rt[1]);

			int minutes = (lt_mins-rt_mins);
			
			returnValue = user.stripOffExcessDigits(minutes*60 + (lt_secs-rt_secs));
		}
		difference = returnValue;
	}
}
