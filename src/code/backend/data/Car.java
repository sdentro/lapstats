package code.backend.data;

/**
 * @author sdentro
 * @version $Id$
 */

import java.util.HashMap;

public class Car extends CarTrackObject {
	
	private String carClass;
	private HashMap<String, String> handicaps;
	public Car(String name, String id) {
		super(name, id);
		carClass = "";
		handicaps = new HashMap<String, String>();
	}

	public void setToClass(String carClass) {
		this.carClass = carClass;
	}
	
	public String getCarClass() {
		return carClass;
	}
}
