package code.backend.data;

/**
 * This Class describes an identifier. At first only a String was used
 * but that is too easy to interpred wrong as a name. So the more telling
 * Id Object was born. The identifier value cannot be changed.
 * @author sdentro
 * @version $Id$
 */
public class Id {
	private final String identifier;
	
	public Id(String identifier) {
		this.identifier = identifier;
	}
	
	public String getIdentifier() {
		return identifier;
	}
	
	public boolean equals(Object that) {
		return that instanceof Id ? this.getIdentifier().equals(((Id) that).getIdentifier()) : false;
	}
	
	public int hashCode() {
		int hash = 7;
		// Calculate our own hashCode of identifier is empty (hashcode = 0). Otherwise use the one from the string
		hash = 31 * hash + (null == identifier ? 0 : identifier.hashCode());
		return hash;
	}
	
	public String toString() {
		return "[Id] " + getIdentifier();
	}
}
