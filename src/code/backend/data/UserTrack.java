package code.backend.data;

/**
 * @author sdentro
 * @version $Id$
 */

public class UserTrack {

	private final User user;
	private final Track track;
	private int numberOfLaps;
	
	public UserTrack(User user, Track track) {
		this.user = user;
		this.track = track;
		numberOfLaps = 0;
	}
	
	public void addNumberOfLaps(int numberOfLaps) {
		this.numberOfLaps = this.numberOfLaps + numberOfLaps;
	}
	
	public int getNumberOfLaps() {
		return numberOfLaps;
	}

	public Track getTrack() {
		return track;
	}
	
	public String getTrackName() {
		return track.getName();
	}
	
	public String getUserName() {
		return user.getName();
	}
}
