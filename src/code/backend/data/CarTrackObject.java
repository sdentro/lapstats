package code.backend.data;

/**
 * @author sdentro
 * @version $Id$
 */

public abstract class CarTrackObject extends LapstatsObject {
	
	private Id id;
	private int numberOfLaps;
	private String author;

	public CarTrackObject(String name, String id) {
		super(name);
		this.id = new Id(id);
	}
	
	public void addNumberOfLaps(int numberOfLaps) {
		this.numberOfLaps = this.numberOfLaps + numberOfLaps;
	}
	
	public void subtractNumberOfLaps(int numberOfLaps) {
		this.numberOfLaps = this.numberOfLaps - numberOfLaps;
	}
	
	public void setName(String newName) {
		name = newName;
	}
	
	public Id getId() {
		return id;
	}
	
	/**
	 * Method to fetch the identifier itself. It is shorthand code
	 * for getId().getIdentifier().
	 * @return A String containing the identifier of the object. 
	 */
	public String getIdentifier() {
		return id.getIdentifier();
	}
	
	public String getAuthor() {
		return author;
	}
	
	public void setAuthor(String author) {
		this.author = author;
	}
	
	public boolean equals(Object that) {
		return that instanceof CarTrackObject ? this.getId().equals(((CarTrackObject) that).getId()) : false;
	}
	
	public int hashCode() {
		int hash = 7;
		// Calculate our own hashCode of identifier is empty (hashcode = 0). Otherwise use the one from the id
		hash = 31 * hash + (null == id ? 0 : id.hashCode());
		return hash;
	}
	
	public String toString() { 
		return "["+getName()+"]";
	}
		
}
