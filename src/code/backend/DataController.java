package code.backend;

/**
 * @author sdentro
 * @version $Id$
 */

import java.util.Hashtable;
import java.util.Vector;

import code.backend.data.Car;
import code.backend.data.Id;
import code.backend.data.Track;
import code.backend.data.TrackSet;
import code.backend.data.User;
import code.backend.data.UserCar;
import code.backend.data.UserTrack;


public class DataController {

	// Hashtable that connects a username to a user object
	private final Hashtable<String, User> userTable = new Hashtable<String, User>();
	
	// Hashtable that connects a tracksetname to a trackset
	private final Hashtable<String, TrackSet> trackSets = new Hashtable<String, TrackSet>();
	
	// Hashtable that connects a trackname to a track
	private final Hashtable<Id, Track> trackTable = new Hashtable<Id, Track>();
	private final Hashtable<String, Id> trackNameIdTable = new Hashtable<String, Id>();
	
	// Hashtable that connects a carname to a car
	private final Hashtable<Id, Car> carTable = new Hashtable<Id, Car>();
	private final Hashtable<String, Id> carNameIdTable = new Hashtable<String, Id>();
	
	public DataController() {

	}
	
	////////////////////////////////////////////////////////////////////////////////////////
	// Add and remove elements from the data store
	////////////////////////////////////////////////////////////////////////////////////////
	public void addCar(Car car) {
		if(!carTable.containsKey(car.getId())) {
			carTable.put(car.getId(), car);
			carNameIdTable.put(car.getName(), car.getId());
		}
	}
	
	public void addTrack(Track track) {
		if(!trackTable.containsKey(track.getId())) {
			trackTable.put(track.getId(), track);
			trackNameIdTable.put(track.getName(), track.getId());
		}
	}
	
	public void addUser(User user) {
		if(!userTable.containsKey(user.getName()))
			userTable.put(user.getName(), user);
	}
	
	public void removeUser(String userName) {
		if(userTable.containsKey(userName))
			userTable.remove(userName);
	}
	
	public void addTrackSet(String trackSetName) {
		addTrackSet(new TrackSet(trackSetName));
		for(User u : userTable.values()) {
			u.addSet(getTrackSet(trackSetName));
		}
	}
	
	public void addTrackSet(TrackSet trackSet) {
		if(!trackSets.containsKey(trackSet.getName()))
			trackSets.put(trackSet.getName(), trackSet);
	}
	
	public void removeTrackSet(String setName) {
		if(trackSets.containsKey(setName))
			trackSets.remove(setName);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////
	// Simple getters
	////////////////////////////////////////////////////////////////////////////////////////
	
	public Car getCar(Id carId) {
		if(carId != null && carTable.containsKey(carId))
			 return carTable.get(carId);
		throw new NullPointerException("[DC] no such carId");
	}
	
	public Car getCarByName(String name) {
		if(name != null && carNameIdTable.containsKey(name))
			 return carTable.get(carNameIdTable.get(name));
		throw new NullPointerException("[DC] no such car name");
	}
	
	public Vector<String> getListOfCarNames() {
		return new Vector<String>(carNameIdTable.keySet());
	}
	
	public Vector<Id> getListOfCarIds() {
		return new Vector<Id>(carTable.keySet());
	}
	
	public Id getCarId(String carName) {
		return carNameIdTable.get(carName);
	}
	
	public Track getTrack(Id trackId) {
		if(trackId != null && trackTable.containsKey(trackId))
			return trackTable.get(trackId);
		throw new NullPointerException("[DC] no such trackId");
	}
	
	public Track getTrackByName(String name) {
		if(name != null && trackNameIdTable.containsKey(name))
			return trackTable.get(trackNameIdTable.get(name));
		throw new NullPointerException("[DC] no such track name");
	}
	
	public Vector<String> getListOfTrackNames() {
		return new Vector<String>(trackNameIdTable.keySet());
	}
	
	public Vector<Id> getListOfTrackIds() {
		return new Vector<Id>(trackTable.keySet());
	}
	
	public Id getTrackId(String trackName) {
		return trackNameIdTable.get(trackName);
	}
	
	public User getUser(String name) {
		if(name != null && userTable.containsKey(name))
			return userTable.get(name);
		throw new NullPointerException("[DC] no such user name");
	}
	
	public Vector<String> getListOfUserNames() {
		return new Vector<String>(userTable.keySet());
	}
	
	public TrackSet getTrackSet(String name) {
		if(name != null && trackSets.containsKey(name))
			return trackSets.get(name);
		throw new NullPointerException("[DC] no such trackSet name");
	}
	
	public Vector<String> getListOfSetNames() {
		return new Vector<String>(trackSets.keySet());
	}
	
	////////////////////////////////////////////////////////////////////////////////////////
	// Contains 
	////////////////////////////////////////////////////////////////////////////////////////
	
	public boolean containsUser(String userName) {
		return userTable.containsKey(userName);
	}
	
	public boolean containsUser(User user) {
		return containsUser(user.getName());
	}
	
	public boolean containsCar(Car car) {
		return containsCar(car.getId());
	}
	
	public boolean containsCar(Id carId) {
		return carTable.containsKey(carId);
	}
	
	public boolean containsTrack(Track track) {
		return containsTrack(track.getId());
	}
	
	public boolean containsTrack(Id trackId) {
		return trackTable.containsKey(trackId);
	}
	
	public boolean containsTrackSet(String trackSetName) {
		return trackSets.containsKey(trackSetName);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////
	// Other methods
	////////////////////////////////////////////////////////////////////////////////////////
	
	public int getNumberOfUsers() {
		return userTable.keySet().size();
	}
	
	public void saveModifiedTrack(Track track) {
		String oldName = "";
		// delete the old track name-id pair from  the list and add the new one. 
		for(String trackName : trackNameIdTable.keySet()) {
			if(trackNameIdTable.get(trackName).equals(track.getId())) {
				oldName = trackName;
			}
		}
		trackNameIdTable.remove(oldName);
		trackNameIdTable.put(track.getName(), track.getId());
	}
	
	public void saveModifiedCar(Car car) {
		String oldName = "";
		// delete the old car name-id pair from  the list and add the new one. 
		for(String carName : carNameIdTable.keySet()) {
			if(carNameIdTable.get(carName).equals(car.getId())) {
				oldName = carName;
			}
		}
		carNameIdTable.remove(oldName);
		carNameIdTable.put(car.getName(), car.getId());
	}
	
	public void saveTrackSet(String trackSetName, Vector<String> trackNames) {
		if(!trackTable.containsKey(trackSetName)) {
			addTrackSet(trackSetName);
		}
		TrackSet set = getTrackSet(trackSetName);
		
		Vector<String> newTracksInSet = new Vector<String>();
		newTracksInSet.addAll(trackNames);
		newTracksInSet.removeAll(set.getTrackNames());
		
		Vector<String> removedTracksFromSet = new Vector<String>();
		removedTracksFromSet.addAll(set.getTrackNames());
		removedTracksFromSet.removeAll(trackNames);
		
		// Add all tracks to the set and add set to the track
		for(String trackName : newTracksInSet) {
			Id trackId = getTrackId(trackName);
			Track track = getTrack(trackId);
			set.addTrack(track);
			track.addToSet(trackSetName);
		}
		
		// Remove other tracks from set
		for(String trackName : removedTracksFromSet) {
			Id trackId = getTrackId(trackName);
			Track track = getTrack(trackId);
			set.removeTrack(track);
			track.removeFromSet(trackSetName);
		}
	}
	
	/**
	 * Returns the total number of laptimes driven by the user
	 * specified at the param.
	 * @param a valid username
	 */
	public int getTotalLaptimes(String userName) {
		return ((User) userTable.get(userName)).getNumberOfTracksDriven();
	}
	
	/**
	 * Returns the total number of cars driven by the user
	 * specified at the param.
	 * @param a valid username
	 */
	public int getTotalCarsDriven(String userName) {
		return ((User) userTable.get(userName)).getNumberOfCarsDriven();
	}
	
	/**
	 * Returns the total number of laps driven by the user
	 * specified at the param.
	 * @param a valid username
	 */
	public int getTotalLaps(String userName) {
		return ((User) userTable.get(userName)).getTotalNumberOfLapsDriven();
	}
	
	/**
	 * Method that returns a vector of vectors containing all names and number of
	 * laptimes for all cars where the user has at least one laptime.
	 * @param userName
	 * @return
	 */
	public Vector<Vector<String>> getCarNamesAndLaps(String userName) {
		Vector<UserCar> userCars = userTable.get(userName).getAllUserCars();
		Vector<Vector<String>> returnVector = new Vector<Vector<String>>();
		for(UserCar uCar : userCars) {
			if(uCar.getNumberOfLaps() > 0) {
				Vector<String> row = new Vector<String>();
				row.add(uCar.getCarName());
				row.add(""+uCar.getNumberOfLaps());
				returnVector.add(row);
			}
		}
		
		return returnVector;
	}

	/**
	 * Method that returns a vector of vectors containing all names and number of
	 * laptimes for all tracks where the user has at least one laptime.
	 * @param userName
	 * @return
	 */
	public Vector<Vector<String>> getTrackNamesAndLaps(String userName) {
		Vector<UserTrack> userTracks = userTable.get(userName).getAllUserTracks();
		Vector<Vector<String>> returnVector = new Vector<Vector<String>>();
		for(UserTrack uTrack : userTracks) {
			if(uTrack.getNumberOfLaps() > 0) {
				Vector<String> row = new Vector<String>();
				row.add(uTrack.getTrackName());
				row.add(""+uTrack.getNumberOfLaps());
				returnVector.add(row);
			}
		}
		
		return returnVector;
	}
}
