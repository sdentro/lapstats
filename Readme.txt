================================================================================================================================================
Readme Lapstats v1.2.5
================================================================================================================================================
Thank you for downloading Lapstats for GTR2. This small software package is created so that anyone can go out and try 
to push themselves to improve laptimes. Why Lapstats when we have GTR2 Rank you may ask. Well, GTR2 Rank is a wonderful
website that is very useful. But it contains only the stock GTR2 cars and a small selection of tracks. With the abundance
of addons that are available for GTR2 I felt there should be a software package that is versatile and adaptable (on the data
end at least). 

I have taken the liberty of combining a large amount of cars into one big GT system. Car classes range from GT1 to GT6. It
has taken me many hours to get the basics of this framework down. The plan was to create a measuring stick on how fast 
a car is, when compared to others. Now you can compare cars that aren't in the original GTR2 game. The structure of the
data.dat file is in plain text, so you can add/change whatever you want.

Usage is simple. Start Lapstats, go through the wizard (will only show the first time at startup) and start using. It is possible
to import data from v1.1/v1.1.1, mainly the user data and created tracksets. An existing user or trackset will not be added,
so no changes you made will be overwritten! 

This release is merely a data release with some small bug fixes. 

Although I have not tested this, it is possible that Lapstats will work (more or less) with other Simbin titles. But
there is no data for the tracks and cars. Although one might never know what happens in the future.

I hope you will enjoy Lapstats. If you have any ideas or would like to help out, drop me a line at lapstats@gmail.com.

================================================================================================================================================
# Installation
================================================================================================================================================
1) You will need a Java 1.6 compatible JRE, if not already on your PC => http://www.java.com/en/download/manual.jsp
2) Extract the .7z file to a place where you have write access (the data.dat file needs write access).
3) Upon first usage a wizard will show up asking you for all information needed to start using Lapstats.
4) You can import data from v1.1 and v1.1.1 by using the menu option and selecting the data.dat file from your previous installation. 

================================================================================================================================================
# Known issues
================================================================================================================================================
- The quirkyness of the save and close buttons is designed this way. It's a little awkward when doing just one change, but when doing more than 2 changes it actually saves a lot of clicks;
- FIA 2006 mod contains the same Viper GTS-R (with the same name), but with altered engine. Lapstats cannot see the difference;
- FIA GT3 mod contains a Gallardo with the same name as the standalone Gallardo mod, but with a different engine. Lapstats cannot see the difference;
- the graphical interface doesn't know what to do with a big amount of car classes. Adding a couple more should be OK, but after that the interface might not show all classes when it runs out of space to show info;
- when importing data a user will not be added when it's already in the system. You will see no warning;
- there is no checking of input whatsoever. That means, selecting the wrong file to import or setting a reference time that is not compatible will probably crash Lapstats.
- for the moment track and car names cannot be changed. In version 1.1 I used the names as unique identifiers, but in 1.2 that changed to trackId and carId. But I did not change it properly. Hope to fix this in the future.

================================================================================================================================================
# Changelog
================================================================================================================================================
# Updates in 1.2.5
- Adding 55 cars, including the new BPR mod;
- adding 40 tracks;
- adding a statistics tab that lists the number of laps driven at tracks/with cars;
- export function to export user settings;
- small bugfix in filereader when driving the exact benchmark laptime.

# Updates in 1.2.4
- Adding 244 tracks to a total of 542;
- fixed sorting on every tab. Either laps or classes are sorted instead of the username, as it was before;
- lots of refactoring behind the scenes.

# Updates in 1.2.3
- Fixed the extreme slowness of the filereader that caused so many problems in v1.2.2;
- removed support for 1.1.x versions;
- changed author RS4 v8;
- changed id Astra;
- changed name of one of the Aida tracks.

# Updates in v1.2.2
- Fixed reading cars bug (GTR2 sometimes writes car name, sometimes id to Career.blt, for some reason);
- disabled editing of car and track name since that's not possible in the GUI (see known bugs);
- synchronized startup of graphics with backend to make startup look more uniform;
- added multiple selection to Edit Track Sets window to ease up adding a lot of tracks to a set;
- added some extra info in the names of the F430's and 997 porsches. Even I couldn't tell one from another;
- changed name Lemans 2004 to Le Mans 2004;
- added 118 tracks, 299 in total;
- added 31 cars, extended to a total of 127.

# Updates in v1.2.1
- Fixed bug of no counted laps in Track Tab;
- fixed problem with no times showing in Track Sets Tab (due to user selection);
- added user selector to Track Sets Tab so you can change the view more easily;
- added Ahvenisto track (two versions);
- added Vallelunga extra tracks (three);
- Hungary FIA and Hungaroring 2006 have same reference times now;
- changed reference times for Most (first had the Most Chicane times);
- changed name Boavista track to Porto;
- cars and tracks are savable again in the edit menu's;
- made some space for three extra car classes;
- last but not least, added seven cars. Six of them in a new GT6 class. 

# Updates in V1.2
- Total rewrite of the previously existing graphics;
- added a new view, you can now see at which tracks you need to drive with a certain car to complete your handicap;
- you can decide how many car classes there need to be by editing the data.dat file;
- fixed a couple of bugs in the code, most notably the Career loading bug of v1.1.1;
- trashed the .exe that is just not needed;
- adding a total of 34 cars and 111 tracks!

# Updates in v1.1.1
Major bugfix in the filewriter. I discovered that things went terribly wrong when writing multiple users to data.dat. That bug is fixed in this
version. I also added some extra notes to this readme.

# Updates in v1.1
After some beta testing I've decided it's time to release version 1.1. It contains better support for multiple users, handicaps, calculated over tracksets, improved tracksets and optional auto-loading and auto-saving. This means that I've included the functionality that I initially planned for Lapstats. Also included are more cars and more tracks, so more fun! But there are still lots improvements to be done. Next version will probably have a GUI redesign which is needed... 

================================================================================================================================================
================================================================================================================================================
